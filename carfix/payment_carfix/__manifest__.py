{

    'name': 'Payment Voucher',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'AR Process',
    'website': 'http://visi.co.id/',
    'summary': 'AR Process',
    'description': '''
    ''',
    'depends': [
        'vs_account_role',
        'account',
        'analytic',
        'web',
        'api_account_invoice',
        'payment_advance',
        'payment_voucher',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',

        'data/sequence.xml',
        'views/payment_carfix.xml',
        'views/payment_type.xml',
        'views/account_invoice.xml',
        'views/daftar_bank.xml',

        'wizard/confirm_payment_multi_wizard.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
