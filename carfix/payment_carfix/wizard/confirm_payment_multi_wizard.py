from datetime import datetime, date
from odoo import fields, models, api, _

class ConfirmPaymentMultiWizard(models.TransientModel):

    _name = 'confirm.payment.multi.wizard'

    name = fields.Char('Name')

    def confirm_payment(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        doc_ids.confirm_payment()
