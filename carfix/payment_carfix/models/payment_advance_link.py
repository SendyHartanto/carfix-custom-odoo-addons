from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class PaymentCarfix(models.Model):
    _inherit = "payment.carfix"

    advance_ids = fields.One2many('payment.advance', 'settlement_id', string='Advance', readonly=True, states={'draft': [('readonly', False)]})

class PaymentAdvance(models.Model):
    _inherit = "payment.advance"

    settlement_id = fields.Many2one('payment.carfix', string="Related Settlement")
