from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class DaftarBank(models.Model):
    _name = "daftar.bank"

    name = fields.Char(string='Name')
    is_edc = fields.Boolean(string='EDC')
    is_kartu = fields.Boolean(string='Penerbit Kartu')
    kode_bank = fields.Char(string='Kode Bank')



class AccountPayment(models.Model):
    _inherit = "account.payment"

    bank_edc_id = fields.Many2one('daftar.bank', string='Bank EDC', ondelete='restrict')
    bank_penerbit_id = fields.Many2one('daftar.bank', string='Bank Penerbit Kartu', ondelete='restrict')
