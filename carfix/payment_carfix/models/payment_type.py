from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class PaymentType(models.Model):
    _name = "payment.type"

    name = fields.Char(string='Name')
    saci_code = fields.Char(string='Saci Code')
    multiple_payment = fields.Selection([
        ('single', 'Single Payment'),
        ('multi', 'Multi Payment'),
        ], default='draft', copy=False, string="Tipe Payment")
