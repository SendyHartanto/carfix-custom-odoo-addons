from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    tipe_customer = fields.Many2one('payment.type', 'Tipe')
    jenis_customer = fields.Selection([
        ('non-aff', 'Non Affiliate'),
        ('affiliate', 'Affiliate'),
        ], default='', string="Jenis Customer")

    def _assign_tipe_customer(self, tipe_customer_invoice):
        super(AccountInvoice, self)._assign_tipe_customer(tipe_customer_invoice)
        search_query = [('saci_code', '=', tipe_customer_invoice)]
        payment_type_obj = self.sudo().env['payment.type']
        payment_type_id = payment_type_obj.search(search_query, limit=1)
        if not payment_type_id:
            raise UserError("Tidak ada konfigurasi '%s' pada Tipe Customer. Konfigurasi dilakukan via Invoicing > Configuration > Payment Type" % tipe_customer_invoice)
        return payment_type_id.id



    # @api.model
    # def name_search(self, name, args=None, operator='ilike', limit=100):

    #     args = args or []
    #     recs = self.browse()
    #     if name:
    #         recs = self.search([('number', '=', name)] + args, limit=limit)
    #     if not recs:
    #         recs = self.search([('name', operator, name)] + args, limit=limit)
    #     return recs.name_get()


    # @api.multi
    # def name_get(self):
    #     if self._context.get('invoice_to_pay_search'):
    #         res = []
    #         for invoice in self:
    #             name = "invoice.ref or False"
    #             if name:
    #                 res.append((invoice.id, name))
    #         return res
    #     else:
    #         res = super(AccountInvoice, self).name_get()
    #         return res

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        if self._context.get('invoice_to_pay_search'):
            recs = self.browse()
            if name:
                recs = self.search([('number', 'ilike', name)] + args, limit=limit)
            if not recs:
                recs = self.search([] + args, limit=limit)
            return recs.name_get()
        else:
            result = super(AccountInvoice, self).name_search(name=name, args=args, operator=operator, limit=limit)
            return result
