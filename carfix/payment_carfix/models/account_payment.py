from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

#TAMBAHAN AKUN UNTUK DEBIT, CREDIT CARD, E MONEY

class AccountPayment(models.Model):
    _inherit = "account.payment"

    journal_id_type = fields.Selection(related="journal_id.type", string="Type")
    bank_edc = fields.Char("Bank EDC", required=False, )
    bank_penerbit_kartu = fields.Char("Bank Penerbit Kartu", required=False)
    no_kartu = fields.Char("Nomor Kartu", required=False)
    app_code = fields.Char("APP Code", required=False)


    #Kalau jadi Cash, detail Bank dll di delete
    @api.onchange('journal_id')
    def _onchange_journal(self):
        result = super(AccountPayment, self)._onchange_journal()
        force_invoice_ids = self._context.get('force_invoice_ids', False)
        if force_invoice_ids and self.journal_id:
            if self.journal_id.type == 'cash':
                self.bank_edc = False
                self.bank_penerbit_kartu = False
                self.no_kartu = False
                self.app_code = False
        return result
