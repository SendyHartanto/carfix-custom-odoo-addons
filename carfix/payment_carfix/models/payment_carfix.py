from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class AccountPayment(models.Model):
    _inherit = "account.payment"

    payment_carfix_id = fields.Many2one('payment.carfix', 'Payment Carfix')
    payment_method_id = fields.Many2one(required=False)

    @api.model
    def default_get(self, fields):
        rec = super(AccountPayment, self).default_get(fields)
        force_invoice_ids = self._context.get('force_invoice_ids', False)
        if force_invoice_ids:
            rec.update({
                'invoice_ids': [(6, 0, [force_invoice_ids])],
            })
        return rec

    def _onchange_amount(self):
        result = super(AccountPayment, self)._onchange_amount()
        force_invoice_ids = self._context.get('force_invoice_ids', False)
        if force_invoice_ids:
            self.journal_id = False
        return result

    def _compute_journal_domain_and_types(self):
        result = super(AccountPayment, self)._compute_journal_domain_and_types()
        force_invoice_ids = self._context.get('force_invoice_ids', False)
        if force_invoice_ids:
            result.update({
                'journal_types': set(['bank', 'cash']),
            })
        return result

    def _create_payment_entry(self, amount):
        move = super()._create_payment_entry(amount)
        if self._context.get('force_move_ref', False):
            move.write({
                'ref' : self._context.get('force_move_ref')
            })
        return move

class PaymentCarfix(models.Model):
    _name = "payment.carfix"

    _order = "payment_date desc, name desc"

    name = fields.Char(string='Name')
    invoice_id = fields.Many2one('account.invoice', 'No Invoice', readonly=True, states={'draft': [('readonly', False)]})

    company_id = fields.Many2one('res.company', string='Company', change_default=True,
        readonly=True, states={'draft': [('readonly', False)]},
        default=lambda self: self.env.user.company_id.id)

    invoice_partner_id_ref = fields.Char(related='invoice_id.partner_id.ref', string='Kode Customer', store=True)
    invoice_partner_id = fields.Many2one('res.partner', related='invoice_id.partner_id', string='Nama')
    invoice_alamat = fields.Char(related='invoice_id.alamat', string='Alamat')
    invoice_state = fields.Selection(related='invoice_id.state', string='Invoice Status')
    currency_id = fields.Many2one('res.currency', related='invoice_id.currency_id', string='Currency')
    invoice_date_invoice = fields.Date(related='invoice_id.date_invoice', string='Tanggal Invoice')
    invoice_residual = fields.Monetary(related='invoice_id.residual', string='Amount Due')
    invoice_amount_untaxed = fields.Monetary(related='invoice_id.amount_untaxed', string='DPP')
    invoice_amount_tax = fields.Monetary(related='invoice_id.amount_tax', string='Tax')
    invoice_amount_total = fields.Monetary(related='invoice_id.amount_total', string='Amount Total')
    invoice_no_polisi = fields.Char(related='invoice_id.no_polisi', string='No. Polisi')
    invoice_mobil = fields.Char(related='invoice_id.mobil', string='Mobil')
    invoice_tipe_customer = fields.Many2one(related='invoice_id.tipe_customer', string='Tipe Customer')
    invoice_jenis_customer = fields.Selection(related='invoice_id.jenis_customer', string='Jenis Customer')
    invoice_prefix = fields.Char(string='Prefix Provinsi / Cabang (5 digit)')

    voucher_ids = fields.One2many('payment.voucher', 'payment_carfix_id', string='Voucher', readonly=True, states={'draft': [('readonly', False)]})
    payment_ids = fields.One2many('account.payment', 'payment_carfix_id', string='Payment', readonly=True, states={'draft': [('readonly', False)]})


    residual = fields.Monetary(string='Total Biaya')
    after_payment_total = fields.Monetary(string='Sisa yang harus dibayarkan', readonly=True)
    biaya_materai = fields.Monetary(string='Sisa yang harus dibayarkan', readonly=True, compute='update_total_bayar', store=True)
    total_bayar_dan_materai = fields.Monetary(string='Sisa yang harus dibayarkan', compute='update_total_bayar', store=True)

    tagihan_per_tanggal_bayar = fields.Monetary(string='Tagihan per tanggal Bayar', readonly=True)
    after_payment_residual = fields.Monetary(string='Sisa per tanggal Bayar', readonly=True)

    payment_date = fields.Date(help="Biasanya Tanggal Hari Ini", string='Tanggal Bayar', readonly=True, states={'draft': [('readonly', False)]}, default=fields.Date.context_today, required=True, copy=False)
    state = fields.Selection([
                                ('draft', 'Draft'),
                                ('posted', 'Posted'),
                                ('sent', 'Sent'),
                                ('reconciled', 'Reconciled'),
                                ('cancelled', 'Cancelled')], default='draft', copy=False, string="Status")


    @api.onchange('invoice_id')
    def _onchange_invoice_id(self):
        for this_invoice in self:
            inv_id = this_invoice.invoice_id.id or False #HARUS ID untuk dimasukkan ke Tuple
            if this_invoice.invoice_id.number:
                this_invoice.invoice_prefix = this_invoice.invoice_id.number[:5]
            else:
                this_invoice.invoice_prefix = False
            this_invoice.residual = this_invoice.invoice_id.residual or 0
            this_invoice.tagihan_per_tanggal_bayar = this_invoice.invoice_id.residual or 0

            for payment_id in this_invoice.payment_ids:
                if payment_id.state == 'draft':
                    payment_id.invoice_ids = [(6, 0, [inv_id])] if inv_id else False
                    payment_id.partner_id = this_invoice.invoice_id.partner_id or False

    @api.depends('amount_total', 'biaya_materai', 'invoice_id')
    def update_total_bayar(self):
        for this_invoice in self:
            # Display Materai sementara dihilangkan
            # if this_invoice.invoice_id.amount_total > 1000000:
            #     this_invoice.biaya_materai = 6000
            # elif this_invoice.invoice_id.amount_total > 250000:
            #     this_invoice.biaya_materai = 3000
            # else:
            #     this_invoice.biaya_materai = 0
            this_invoice.biaya_materai = 0
            this_invoice.total_bayar_dan_materai = this_invoice.amount_total + this_invoice.biaya_materai
            this_invoice.after_payment_residual = this_invoice.tagihan_per_tanggal_bayar - this_invoice.amount_total

    @api.multi
    def process_residual_value(self):
        for rec in self:
            if rec.invoice_tipe_customer.multiple_payment == "single":
                if rec.invoice_residual > rec.amount_total:
                    raise UserError(_("Payment for Invoice : %s failed. Customers with type  %s are needed to complete Invoice directly within one payment") % (rec.invoice_id.display_name, self.invoice_tipe_customer.name))
            if rec.invoice_residual < rec.amount_total:
                raise UserError(_("Payment for Invoice : %s failed. Amount Total is to much. Maximum payment is %s") % (rec.invoice_id.display_name, '{:,.2f}'.format(rec.invoice_residual)))
            rec.residual = rec.invoice_residual


    def get_payment_carfix_name(self):
        sequence_code = 'payment.carfix'
        name_next_number = self.env['ir.sequence'].with_context(ir_sequence_date=self.payment_date).next_by_code(sequence_code)
        if not name_next_number:
            raise UserError(_("Payment for Invoice : %s failed. You need to define a sequence with code :  %s in your company.") % (self.invoice_id.display_name, sequence_code))
        invoice_prefix = self.invoice_id.number[:5]
        payment_doc_name = invoice_prefix + name_next_number
        return payment_doc_name

    @api.multi
    def confirm_payment(self):
        for payment_doc in self:
            if not payment_doc.invoice_id:
                raise UserError(_("Link an Invoice first !"))
            payment_doc.process_residual_value()
            payment_doc.process_residual_value()
            payment_doc_name = payment_doc.get_payment_carfix_name()
            payment_doc.with_context(force_move_ref=payment_doc_name).link_invoice()
            payment_doc.write({
                'state': 'posted',
                'name': payment_doc_name,
            })


    @api.multi
    def set_to_draft(self):
        self.write({'state': 'draft'})


    @api.multi
    def cancel(self):
        for rec in self:
            for voucher_id in rec.voucher_ids:
                voucher_id.cancel()
            for payment_id in rec.payment_ids:
                payment_id.cancel()
            for advance_id in rec.advance_ids:
                advance_id.cancel()

        self.write({'state': 'cancelled'})

    def _check_zero_amount(self):
        if self.amount_total == 0:
            raise ValidationError(_(
            'This payment total amount must be greater than 0. Check field : Amount Total !'))
        for voucher_id in self.voucher_ids:
            if voucher_id.amount == 0:
                raise ValidationError(_(
                'Amount Voucher must be greater than 0. Check Voucher Code : %s !') % (voucher_id.name))

        for payment_id in self.payment_ids:
            if payment_id.amount == 0:
                raise ValidationError(_(
                'Amount Payment must be greater than 0. Check %s Payment!') % (payment_id.journal_id.name))


    def _check_payment_method_invoice_ids(self, payment_id):
        if not payment_id.invoice_ids:
            payment_id.write({
                'invoice_ids': [(6, 0, [self.invoice_id.id])],
            })

    @api.multi
    def link_invoice(self):
        self._check_zero_amount()
        for rec in self:
            for voucher_id in rec.voucher_ids:
                voucher_id.post()

            for payment_id in rec.payment_ids:
                payment_id.write({
                    'payment_date': self.payment_date
                })
                #Pastikan setiap Payment, sudah ada Invoice ID nya. Ini Untuk Importable
                self._check_payment_method_invoice_ids(payment_id)
                try:
                    payment_id.post()
                except Exception as E:
                    if E.__class__.__name__ == "UserError":
                        orig_message = E.name
                        new_message = """
                            Payment Process gagal.
                            Pembayaran diperuntukkan untuk Invoice %s

                            %s
                        """ % (self.invoice_id.number, orig_message)
                        raise E.__class__(new_message)
                    else:
                        raise

            advance_row = 1
            for advance_id in rec.advance_ids:
                if advance_id.partner_id != self.invoice_partner_id:
                    raise UserError(_("Payment for Invoice : %s failed. Advance Payment row no : %s is for partner %s which is different to %s. Choose Payment for %s !") % (rec.invoice_id.display_name, advance_row, advance_id.partner_id.name, rec.invoice_partner_id.name, rec.invoice_partner_id.name))

                advance_id.write({
                    'invoice_id' : self.invoice_id.id,
                })
                advance_id.link_invoice()
                advance_row += 1

            # rec.after_payment_residual = rec.invoice_residual # :TODO Harus di 0 lagi ketika Unreconciled..


    amount_total = fields.Monetary(compute='_compute_payments_amount', string='Amount Total')

    @api.multi
    @api.depends('payment_ids.amount', 'voucher_ids.amount', 'advance_ids.amount')
    def _compute_payments_amount(self):
        for rec in self:
            payment_ids_amount = sum(rec.payment_ids.mapped('amount'))
            voucher_ids_amount = sum(rec.voucher_ids.mapped('amount'))
            advance_ids_amount = sum(rec.advance_ids.mapped('amount'))
            rec.amount_total = payment_ids_amount + voucher_ids_amount + advance_ids_amount


    @api.multi
    def unlink(self):
        if any(bool(rec.state != 'draft') for rec in self):
            raise UserError(_("You can not delete a payment that is not draft"))
        return super(PaymentCarfix, self).unlink()

class PaymentVoucher(models.Model):
    _inherit = "payment.voucher"

    payment_carfix_id = fields.Many2one('payment.carfix', 'Payment Carfix')
    partner_id = fields.Many2one('res.partner', related='payment_carfix_id.invoice_partner_id', string='Customer')
    invoice_id = fields.Many2one('account.invoice', related='payment_carfix_id.invoice_id', string='Invoice')

    @api.model
    def default_get(self, fields):
        rec = super(PaymentVoucher, self).default_get(fields)
        force_invoice_ids = self._context.get('force_invoice_ids', False)
        if force_invoice_ids:
            rec.update({
                'invoice_ids': [(6, 0, [force_invoice_ids])]
            })
        return rec



