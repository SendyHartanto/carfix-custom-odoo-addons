{
    'name': 'VISI - E Faktur Pajak',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http://visi.co.id/',
    'summary': 'Custom-built Odoo',
    'description': '''

    Perlu install vs_account_discount_value.
    1. Odoo hanya support Diskon Persentase
    2. Efaktur butuh Diskon nominal

    ''',
    'depends': [
        'account',
        'vs_account_discount_value',
        # 'api_account_invoice',
    ],
    'data': [
        'views/res_partner.xml',
        'security/user_role.xml',
        'security/ir.model.access.csv',
        'views/account_invoice_faktur.xml',
        'views/account_invoice_custom.xml',
        'wizard/recompute_faktur.xml',
        'wizard/multi_create_faktur_pajak.xml',
        'views/web_extension.xml',
        'views/res_company.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
