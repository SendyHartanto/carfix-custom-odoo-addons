# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    faktur_pajak_id = fields.Many2one('faktur.pajak', string='Nomor Faktur', copy=False)
    faktur_required = fields.Boolean(string='Faktur Required', default=False, copy=False)

    @api.one
    def write(self, vals):
        #self._cr.commit()
        faktur_id = vals.get('faktur_pajak_id', None)
        if faktur_id:
            self.env['faktur.pajak'].browse(faktur_id).write({'invoice_id': self.id})
            if self.faktur_pajak_id.id != faktur_id:
                self.env['faktur.pajak'].browse(self.faktur_pajak_id.id).clear_data()
        if not faktur_id and faktur_id is not None:
            if self.faktur_pajak_id.exists():
                self.env['faktur.pajak'].browse(self.faktur_pajak_id.id).clear_data()
        #
        # self._check_account_ids(vals)
        # res = super(IrHttp, self).session_info()
        # user = request.env.user
        # show_exp_notif = False
        # if (user.has_group('sales_team.group_sale_salesman') or
        #     user.has_group('sales_team.group_sale_salesman_all_leads') or
        #     user.has_group('sales_team.group_sale_manager')
        #     ):
        #     show_exp_notif = True
        # res['user_context'].update({'show_exp_notif' : show_exp_notif})
        res = super(AccountInvoice, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        invoice = super(AccountInvoice, self.sudo()).create(vals)
        faktur_id = vals.get('faktur_pajak_id', False)
        if faktur_id:
            self.env['faktur.pajak'].browse(faktur_id).write({'invoice_id': self.id})
        return invoice



    @api.multi
    def cetak_faktur_pajak(self):
        a =  4
        b = 2

