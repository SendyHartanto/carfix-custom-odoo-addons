from odoo import api, fields, models, _

class ResPartner(models.Model):
    _inherit = 'res.partner'

    #Kepentingan Efaktur
    #######################################
    nama_pkp = fields.Char(string="Nama PKP", help="Nama Pengusaha Kena Pajak")
    npwp = fields.Char(string="NPWP", help="Nomor Pokok Wajib Pajak adalah sarana dalam administrasi perpajakan yang dipergunakan sebagai identitas Wajib Pajak")
    pajak_street = fields.Char(
            help="Alamat Pajak ini akan di cetak pada dokumen Invoice dan dokumen Surat Jalan")
    pajak_street2 = fields.Char()
    pajak_zip = fields.Char(change_default=True)
    pajak_city = fields.Char()
    pajak_state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict')
