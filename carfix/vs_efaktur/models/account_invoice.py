from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.one
    def write(self, vals):
        #self._cr.commit()
        faktur_id = vals.get('faktur_pajak_id', None)
        if faktur_id:
            self.env['faktur.pajak'].browse(faktur_id).write({'invoice_id': self.id})
            if self.faktur_pajak_id.id != faktur_id:
                self.env['faktur.pajak'].browse(self.faktur_pajak_id.id).clear_data()
        if not faktur_id and faktur_id is not None:
            if self.faktur_pajak_id.exists():
                self.env['faktur.pajak'].browse(self.faktur_pajak_id.id).clear_data()
        #
        # self._check_account_ids(vals)
        # res = super(IrHttp, self).session_info()
        # user = request.env.user
        # show_exp_notif = False
        # if (user.has_group('sales_team.group_sale_salesman') or
        #     user.has_group('sales_team.group_sale_salesman_all_leads') or
        #     user.has_group('sales_team.group_sale_manager')
        #     ):
        #     show_exp_notif = True
        # res['user_context'].update({'show_exp_notif' : show_exp_notif})
        res = super(AccountInvoice, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        invoice = super(AccountInvoice, self.sudo()).create(vals)
        faktur_id = vals.get('faktur_pajak_id', False)
        if faktur_id:
            self.env['faktur.pajak'].browse(faktur_id).write({'invoice_id': self.id})
        return invoice

    @api.multi
    def cetak_faktur_pajak(self):
        for invoice_doc in self:
            no_seri_pajak = invoice_doc.no_seri_pajak
            faktur_pajak_obj = invoice_doc.env['faktur.pajak']
            data_faktur = {
                'name' : no_seri_pajak,
                'invoice_id' : invoice_doc.id,
            }
            # search if already available
            search_query = [('name', '=', no_seri_pajak)]
            faktur_pajak_doc = faktur_pajak_obj.search(search_query, limit=1)

            if faktur_pajak_doc:
                no_invoice = faktur_pajak_doc.invoice_id.no_invoice
                message = '''No Seri Pajak %s sudah ada pada sistem untuk Invoice %s.''' % (no_seri_pajak, no_invoice)
                raise UserError(message)
            else:
                faktur_pajak_doc = faktur_pajak_obj.create(data_faktur)
                faktur_pajak_doc.write(data_faktur)
