# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import json

class PajakWizard(models.TransientModel):
    _name = "pajak.wizard"
    
    @api.multi
    def download_csv(self):
        data = {
            'doc_ids' : self._context.get('active_ids', []),
        }
        action = {
            'name': 'FEC',
            'type': 'ir.actions.act_url',
            'url': '/web/export_csv?data='+json.dumps(data),
            'target': 'button_download',
            }
        return action