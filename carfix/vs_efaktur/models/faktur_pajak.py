# -*- coding: utf-8 -*-
#TODO FLOAT GIMANA untuk total, amount dll ?
#TODO Amount Tax belum disimpen di database
#TODO Unlink Invoice Line, perlu di delete jg..

import json
import re

from odoo import api
from odoo import models, fields
from datetime import datetime
from odoo.tools import float_compare, float_round, float_repr
from odoo.exceptions import UserError


def_val = ''

def remove_tandabaca(tulisan=False):
    if not tulisan:
        return ''
    tulisan = re.sub('-', '', tulisan)
    tulisan = tulisan.replace(" ", "")
    return tulisan

def process_faktur(tulisan=False):
    if not tulisan:
        return ''
    tulisan = re.sub('-', '', tulisan)
    tulisan = tulisan.replace(".", "")
    tulisan = tulisan.replace(" ", "")
    kode_pengganti = tulisan[:2] or ''
    fg_pengganti = tulisan[2:3] or ''
    nomor_faktur = tulisan[3:] or ''
    return kode_pengganti, fg_pengganti, nomor_faktur

def get_tahun(tanggal=False):
    if not tanggal:
        return ''
    tanggal = tanggal[:4]
    return tanggal

def get_tanggal(tanggal=False):
    if not tanggal:
        return ''
    tanggal_dalam_datetime = datetime.strptime(tanggal, "%Y-%m-%d")
    tanggal_string = tanggal_dalam_datetime.strftime("%d/%m/%Y")
    return tanggal_string

def get_bulan(tanggal=False):
    if not tanggal:
        return ''
    tanggal_dalam_datetime = datetime.strptime(tanggal, "%Y-%m-%d")
    tanggal_string = tanggal_dalam_datetime.strftime("%m")
    return tanggal_string

class ResPartner(models.Model):
    _inherit = "res.partner"

    npwp = fields.Char('NPWP', copy=False)
    blok = fields.Char('blok', copy=False)
    nomor = fields.Char('nomor', copy=False)
    rt = fields.Char('rt', copy=False)
    rw = fields.Char('rw', copy=False)
    kecamatan = fields.Char('kecamatan', copy=False)
    kelurahan = fields.Char('kelurahan', copy=False)
    kabupaten = fields.Char('kabupaten', copy=False)

class FakturPajakLine(models.Model):
    _name = "faktur.pajak.line"

    of_kode_objek = fields.Char()
    of_nama = fields.Char()
    of_harga_satuan = fields.Char()
    of_jumlah_barang = fields.Char()
    of_harga_total = fields.Char()
    of_diskon = fields.Char()
    of_dpp = fields.Char()
    of_ppn = fields.Char()
    of_tarif_ppnbm = fields.Char()
    of_ppnbm = fields.Char()

    faktur_id = fields.Many2one('faktur.pajak', string='Faktur')

class FakturPajak(models.Model):
    _name = "faktur.pajak"

    company_id = fields.Many2one('res.company', related='invoice_id.company_id', string='Company', store=True, copy=False)
    no = fields.Char('Nomor', copy=False)
    name = fields.Char('Nomor Faktur', copy=False)
    total = fields.Float('Total Invoice', copy=False)
    invoice_id = fields.Many2one('account.invoice', string='Invoice')
    line_ids = fields.One2many('faktur.pajak.line', 'faktur_id' , string='Line OF')

    lt_npwp = fields.Char()
    lt_nama = fields.Char()
    lt_jalan = fields.Char()
    lt_blok = fields.Char()
    lt_nomor = fields.Char()
    lt_rt = fields.Char()
    lt_rw = fields.Char()
    lt_kecamatan = fields.Char()
    lt_kelurahan = fields.Char()
    lt_kabupaten = fields.Char()
    lt_propinsi = fields.Char()
    lt_kode_pos = fields.Char()
    lt_nomor_telepon = fields.Char()

    fk_kd_jenis_transaksi = fields.Char()
    fk_fg_pengganti = fields.Char()
    fk_nomor_faktur = fields.Char()
    fk_masa_pajak = fields.Char()
    fk_tahun_pajak = fields.Char()
    fk_tanggal_faktur = fields.Char()
    fk_npwp = fields.Char()
    fk_nama = fields.Char()
    fk_alamat_lengkap = fields.Char()
    fk_jumlah_dpp = fields.Char()
    fk_jumlah_ppn = fields.Char()
    fk_jumlah_ppnbm = fields.Char()
    fk_id_keterangan_tambahan = fields.Char()
    fk_fg_uang_muka = fields.Char()
    fk_uang_muka_dpp = fields.Char()
    fk_uang_muka_ppn = fields.Char()
    fk_uang_muka_ppnbm = fields.Char()
    fk_referensi = fields.Char()

    of_kode_objek = fields.Char()
    of_nama = fields.Char()
    of_harga_satuan = fields.Char()
    of_jumlah_barang = fields.Char()
    of_harga_total = fields.Char()
    of_diskon = fields.Char()
    of_dpp = fields.Char()
    of_ppn = fields.Char()
    of_tarif_ppnbm = fields.Char()
    of_ppnbm = fields.Char()

    @api.multi
    def download_csv(self):
        data = {
            'doc_ids' : self.ids,
        }
        action = {
            'name': 'FEC',
            'type': 'ir.actions.act_url',
            'url': '/web/export_csv?data='+json.dumps(data),
            'target': 'button_download',
            }
        return action

    @api.multi
    def clear_data(self):
        vals = {}
        vals.update({
            'invoice_id': False,
            'lt_npwp' : '',
            'lt_nama' : '',
            'lt_jalan' : '',
            'lt_blok' : '',
            'lt_nomor' : '',
            'lt_rt' : '',
            'lt_rw' : '',
            'lt_kecamatan' : '',
            'lt_kelurahan' : '',
            'lt_kabupaten' : '',
            'lt_propinsi' : '',
            'lt_kode_pos' : '',
            'lt_nomor_telepon' : '',

            'fk_kd_jenis_transaksi' : '',
            'fk_fg_pengganti' : '',
            'fk_nomor_faktur' : '',
            'fk_masa_pajak' : '',
            'fk_tahun_pajak' : '',
            'fk_tanggal_faktur' : '',
            'fk_npwp' : '',
            'fk_nama' : '',
            'fk_alamat_lengkap' : '',
            'fk_jumlah_dpp' : '',
            'fk_jumlah_ppn' : '',
            'fk_jumlah_ppnbm' : '',
            'fk_id_keterangan_tambahan' : '',
            'fk_fg_uang_muka' : '',
            'fk_uang_muka_dpp' : '',
            'fk_uang_muka_ppn' : '',
            'fk_uang_muka_ppnbm' : '',
            'fk_referensi' : '',

            'of_kode_objek' : '',
            'of_nama' : '',
            'of_harga_satuan' : '',
            'of_jumlah_barang' : '',
            'of_harga_total' : '',
            'of_diskon' : '',
            'of_dpp' : '',
            'of_ppn' : '',
            'of_tarif_ppnbm' : '',
            'of_ppnbm' : '',
        })
        self.write(vals)
        return

    @api.multi
    def write(self, vals):
        normal_write = self._context.get('normal_write', False)
        if normal_write:
            return super(FakturPajak, self).write(vals)
        invoice_id = self.invoice_id.id
        if invoice_id:
            vals.update(self.get_partner_info(invoice_id))
            vals.update(self.get_invoice_info(invoice_id))
            vals.update(self.get_invoice_line_info(invoice_id))
        if not invoice_id and invoice_id is not None:
            vals.update({
                'invoice_id': False,
                'lt_npwp' : '',
                'lt_nama' : '',
                'lt_jalan' : '',
                'lt_blok' : '',
                'lt_nomor' : '',
                'lt_rt' : '',
                'lt_rw' : '',
                'lt_kecamatan' : '',
                'lt_kelurahan' : '',
                'lt_kabupaten' : '',
                'lt_propinsi' : '',
                'lt_kode_pos' : '',
                'lt_nomor_telepon' : '',

                'fk_kd_jenis_transaksi' : '',
                'fk_fg_pengganti' : '',
                'fk_nomor_faktur' : '',
                'fk_masa_pajak' : '',
                'fk_tahun_pajak' : '',
                'fk_tanggal_faktur' : '',
                'fk_npwp' : '',
                'fk_nama' : '',
                'fk_alamat_lengkap' : '',
                'fk_jumlah_dpp' : '',
                'fk_jumlah_ppn' : '',
                'fk_jumlah_ppnbm' : '',
                'fk_id_keterangan_tambahan' : '',
                'fk_fg_uang_muka' : '',
                'fk_uang_muka_dpp' : '',
                'fk_uang_muka_ppn' : '',
                'fk_uang_muka_ppnbm' : '',
                'fk_referensi' : '',

                'of_kode_objek' : '',
                'of_nama' : '',
                'of_harga_satuan' : '',
                'of_jumlah_barang' : '',
                'of_harga_total' : '',
                'of_diskon' : '',
                'of_dpp' : '',
                'of_ppn' : '',
                'of_tarif_ppnbm' : '',
                'of_ppnbm' : '',
            })
        return super(FakturPajak, self).write(vals)

    def get_partner_info(self, invoice_id):
        # lt
        # npwp
        # nama
        # jalan
        # blok
        # nomor
        # rt
        # rw
        # kecamatan
        # kelurahan
        # kabupaten
        # propinsi
        # kode_pos
        # nomor_telepon

        value = {}
        inv_doc = self.env['account.invoice'].browse(invoice_id)

        # Partner ID dari Company Objek pajak
        # Bukan Customer
        partner_id = inv_doc.company_id.partner_id
        # partner_id = inv_doc.partner_id
        if partner_id.exists():
            value = {
                'lt_npwp' : partner_id.npwp or def_val,
                'lt_nama' : partner_id.nama_pkp or def_val,
                'lt_jalan' : str(partner_id.pajak_street or def_val) + ' ' + str(partner_id.pajak_street2 or def_val),
                'lt_blok' : partner_id.blok or def_val,
                'lt_nomor' : partner_id.nomor or def_val,
                'lt_rt' : partner_id.rt or def_val,
                'lt_rw' : partner_id.rw or def_val,
                'lt_kecamatan' : partner_id.kecamatan or def_val,
                'lt_kelurahan' : partner_id.kelurahan or def_val,
                'lt_kabupaten' : partner_id.kabupaten or def_val,
                'lt_propinsi' : partner_id.pajak_state_id.name if partner_id.pajak_state_id.exists() else def_val,
                'lt_kode_pos' : partner_id.pajak_zip or def_val,
                'lt_nomor_telepon' : partner_id.phone or def_val,
            }

        return value

    def get_invoice_info(self, invoice_id):
        # fk
        # kd_jenis_transaksi
        # fg_pengganti
        # nomor_faktur
        # masa_pajak
        # tahun_pajak
        # tanggal_faktur
        # npwp
        # nama
        # alamat_lengkap
        # jumlah_dpp
        # jumlah_ppn
        # jumlah_ppnbm
        # id_keterangan_tambahan
        # fg_uang_muka
        # uang_muka_dpp
        # uang_muka_ppn
        # uang_muka_ppnbm
        # referensi
        # REF https://www.online-pajak.com/cara-penggunaan-kode-transaksi-faktur-pajak
        value = {}
        inv_doc = self.env['account.invoice'].browse(invoice_id)
        if inv_doc.exists():
            this_company = self.env.user.company_id.partner_id
            if not self.name:
                raise UserError("Tidak ada Nomor Faktur Pajak untuk Invoice %s" % inv_doc.number)

            kd_jenis, fg_pengganti, nomor_faktur = process_faktur(self.name)
            value = {
                'fk_kd_jenis_transaksi' : kd_jenis,
                'fk_fg_pengganti' : fg_pengganti,
                'fk_nomor_faktur' : nomor_faktur,
                'fk_masa_pajak' : get_bulan(inv_doc.date_invoice) or '',
                'fk_tahun_pajak' : get_tahun(inv_doc.date_invoice) or '',
                'fk_tanggal_faktur' : get_tanggal(inv_doc.date_invoice) or '',
                'fk_npwp' : inv_doc.npwp or '',
                'fk_nama' : inv_doc.partner_id.name or '',
                'fk_alamat_lengkap' : inv_doc.alamat or '',
                # 'fk_alamat_lengkap' : str(this_company.street or def_val) + ' ' + str(this_company.street2 or def_val),
                'fk_jumlah_dpp' : float_repr(float_round(inv_doc.amount_untaxed, 0), 0),
                'fk_jumlah_ppn' : float_repr(float_round(inv_doc.amount_tax, 0, rounding_method='DOWN'), 0),
                'fk_jumlah_ppnbm' : 0,
                'fk_id_keterangan_tambahan' : '',
                'fk_fg_uang_muka' : 0,
                'fk_uang_muka_dpp' : 0,
                'fk_uang_muka_ppn' : 0,
                'fk_uang_muka_ppnbm' : 0,
                'fk_referensi' : inv_doc.number or '',
            }
        return value

    def get_invoice_line_info(self, invoice_id):
        # of
        # kode_objek
        # nama
        # harga_satuan
        # jumlah_barang
        # harga_total
        # diskon
        # dpp
        # ppn
        # tarif_ppnbm
        # ppnbm
        o2m_command = [(5,0)]
        inv_doc = self.env['account.invoice'].browse(invoice_id)
        if inv_doc.exists():
            temp_inv_lines = []
            for inv_line in inv_doc.invoice_line_ids:
                transaction = {
                    'of_kode_objek': inv_line.product_id.default_code or '',
                    'of_nama': inv_line.product_id.name or '',
                    'of_harga_satuan': float_repr(float_round(inv_line.price_unit , 0), 0),
                    'of_jumlah_barang':float_repr(float_round(inv_line.quantity , 0), 0),
                    'of_harga_total': float_repr(float_round(inv_line.quantity * inv_line.price_unit , 0), 0),
                    'of_diskon': float_repr(float_round(inv_line.quantity * inv_line.price_reduce , 0), 0),
                    'of_dpp': float_repr(float_round(inv_line.price_subtotal, 0), 0),
                    'of_ppn': float_repr(float_round(inv_line.price_total - inv_line.price_subtotal, 0, rounding_method='DOWN'), 0),
                    'of_tarif_ppnbm': 0,
                    'of_ppnbm': 0,
                }
                temp_inv_lines.append(transaction)



            inv_tax_total = int(float_repr(float_round(inv_doc.amount_tax, 0, rounding_method='DOWN'), 0))
            temp_total_ppn = 0
            for temp_inv_line in temp_inv_lines:
                # Get this line PPN
                this_line_ppn = int(temp_inv_line.get('of_ppn', '0'))
                temp_total_ppn += this_line_ppn


            selisih_ppn_efaktur = inv_tax_total - temp_total_ppn
            if selisih_ppn_efaktur > 0:
                # Lakukan Adjustment
                for temp_inv_line in temp_inv_lines:
                    # Get this line PPN
                    if selisih_ppn_efaktur == 0:
                        break
                    this_line_ppn = int(temp_inv_line.get('of_ppn', '0'))
                    if this_line_ppn == 0:
                        continue
                    else:
                        this_line_ppn += 1
                        selisih_ppn_efaktur -=1
                        temp_inv_line['of_ppn'] = this_line_ppn

            # Penyesuaian Nilai PPN agar bisa masuk efaktur

            # Assignment to One2many
            for temp_inv_line in temp_inv_lines:
                new_line = (0, 0, temp_inv_line)
                o2m_command.append(new_line)


        value = {'line_ids': o2m_command}
        return value

