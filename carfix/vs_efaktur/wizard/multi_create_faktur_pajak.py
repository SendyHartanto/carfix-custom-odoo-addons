from datetime import datetime, date
from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError

class MultiCreateFakturPajakWizard(models.TransientModel):

    _name = 'multi.create.faktur.pajak.wizard'

    def process_wizard(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        doc_id_ok = []
        doc_id_ok_log = []
        doc_id_error = []
        doc_id_error_log = []
        for doc_id in doc_ids:
            # doc_ids.cetak_faktur_pajak()
            self.env.cr.execute('SAVEPOINT sebelum_create_faktur_pajak')
            try:
                doc_id.cetak_faktur_pajak()
                doc_id_ok.append(doc_id.id)
                doc_id_ok_log.append('- ' + doc_id.no_invoice)
            except Exception as E:
                if hasattr(E, 'name'):
                    payload_log = E.name
                else:
                    payload_log = str(E)
                self.env.cr.execute('ROLLBACK TO SAVEPOINT sebelum_create_faktur_pajak')
                doc_id_error.append(doc_id.id)
                doc_id_error_log.append('- ' + payload_log)

        self.env.cr.commit()
        # raise RedirectWarning(msg, action.id, _('Go to the configuration panel'))
        message = '''Process Summary
            Successfully Created Faktur = %s Invoice(s) 

            %s

            Error Created Faktur = %s Invoice(s)
        
            %s
            
        ''' % (
            len(doc_id_ok),
            '\n'.join(doc_id_ok_log),
            len(doc_id_error),
            '\n'.join(doc_id_error_log),
        )
        raise UserError(message)
