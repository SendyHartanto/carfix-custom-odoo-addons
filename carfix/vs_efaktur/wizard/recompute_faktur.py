from datetime import datetime, date
from odoo import fields, models, api, _

class RecomputeFakturWizard(models.TransientModel):

    _name = 'recompute.faktur.wizard'

    name = fields.Char('Name')

    def process_wizard(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        doc_ids.baca_ulang()

class FakturPajak(models.Model):
    _inherit = 'faktur.pajak'

    @api.multi
    def baca_ulang(self):
        for faktur_doc in self:
            faktur_invoice_id = faktur_doc.invoice_id.id
            vals = {}
            if faktur_invoice_id:
                vals.update(faktur_doc.get_partner_info(faktur_invoice_id))
                vals.update(faktur_doc.get_invoice_info(faktur_invoice_id))
                vals.update(faktur_doc.get_invoice_line_info(faktur_invoice_id))
            faktur_doc.with_context(normal_write=True).write(vals)
        return
    