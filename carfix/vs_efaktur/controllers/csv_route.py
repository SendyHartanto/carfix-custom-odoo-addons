# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json

from odoo import http
from odoo.http import request
import csv
from io import StringIO
from datetime import datetime

class CsvEXporter(http.Controller):

    @http.route('/web/export_csv', type='http', auth="user")
    def export_csv(self, data=None, *args, **kwargs): #Args Kwargs diperlukan siapa tahu ada param lain di URL. sehingga tetep masuk data nya.
        jdata = json.loads(data)
        if jdata.get('doc_ids', False):
            doc_ids = jdata.get('doc_ids', False)
            documents = request.env['faktur.pajak'].browse(doc_ids)

        rows_to_print = []
        rows_to_print.append(['FK','KD_JENIS_TRANSAKSI','FG_PENGGANTI','NOMOR_FAKTUR','MASA_PAJAK','TAHUN_PAJAK','TANGGAL_FAKTUR','NPWP','NAMA','ALAMAT_LENGKAP','JUMLAH_DPP','JUMLAH_PPN','JUMLAH_PPNBM','ID_KETERANGAN_TAMBAHAN','FG_UANG_MUKA','UANG_MUKA_DPP','UANG_MUKA_PPN','UANG_MUKA_PPNBM','REFERENSI'])
        rows_to_print.append(['LT', 'NPWP', 'NAMA', 'JALAN', 'BLOK', 'NOMOR', 'RT', 'RW', 'KECAMATAN', 'KELURAHAN', 'KABUPATEN', 'PROPINSI', 'KODE_POS', 'NOMOR_TELEPON'])
        rows_to_print.append(['OF','KODE_OBJEK','NAMA','HARGA_SATUAN','JUMLAH_BARANG','HARGA_TOTAL','DISKON','DPP','PPN','TARIF_PPNBM','PPNBM'])
        for document in documents:
            rows_to_print.append(self.get_fk_rows(document))
            rows_to_print.append(self.get_lt_rows(document))
            rows_to_print.extend(self.get_of_rows(document))

        def generate():
            data = StringIO()
            w = csv.writer(data)

            # write header
            # w.writerow(('action', 'timestamp'))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)

            # write each log item
            for item in rows_to_print:
                w.writerow(item)
                yield data.getvalue()
                data.seek(0)
                data.truncate(0)

        # add a filename
        response2 = request.make_response(generate(),
            headers=[('Content-Type', 'text/csv'),
                    ('Content-Disposition', 'attachment; filename=Upload Efaktur.csv;')],
            cookies={'fileToken': request.session.session_token})

        # stream the response as the data is generated
        return response2

    def get_fk_rows(self, doc):
        value = (
            'FK',
            doc.fk_kd_jenis_transaksi,
            doc.fk_fg_pengganti,
            doc.fk_nomor_faktur,
            doc.fk_masa_pajak,
            doc.fk_tahun_pajak,
            doc.fk_tanggal_faktur,
            doc.fk_npwp,
            doc.fk_nama,
            doc.fk_alamat_lengkap,
            doc.fk_jumlah_dpp,
            doc.fk_jumlah_ppn,
            doc.fk_jumlah_ppnbm,
            doc.fk_id_keterangan_tambahan,
            doc.fk_fg_uang_muka,
            doc.fk_uang_muka_dpp,
            doc.fk_uang_muka_ppn,
            doc.fk_uang_muka_ppnbm,
            doc.fk_referensi,
        )
        return value

    def get_lt_rows(self, doc):
        value = (
            'FAPR',
            # doc.lt_npwp,
            doc.lt_nama,
            doc.lt_jalan,
            doc.lt_blok,
            doc.lt_nomor,
            doc.lt_rt,
            doc.lt_rw,
            doc.lt_kecamatan,
            doc.lt_kelurahan,
            doc.lt_kabupaten,
            doc.lt_propinsi,
            doc.lt_kode_pos,
            doc.lt_nomor_telepon,
        )
        return value

    def get_of_rows(self, doc):
        value=[]
        for line in doc.line_ids:
            line_data = (
                'OF',
                line.of_kode_objek,
                line.of_nama,
                line.of_harga_satuan,
                line.of_jumlah_barang,
                line.of_harga_total,
                line.of_diskon,
                line.of_dpp,
                line.of_ppn,
                line.of_tarif_ppnbm,
                line.of_ppnbm,
            )
            value.append(line_data)
        return value
