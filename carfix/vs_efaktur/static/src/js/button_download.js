odoo.define('button.download', function (require) {
'use strict';

var ActionManager = require('web.ActionManager');
var core = require('web.core');
var crash_manager = require('web.crash_manager');
var framework = require('web.framework');


var session = require('web.session');


var _t = core._t;
var _lt = core._lt;


var wkhtmltopdf_state;
ActionManager.include({
  
    ir_actions_act_url: function (action, options) {
        var url = action.url;
        if (action.target === 'button_download') {
            framework.redirect(url);
            options.on_close();
            return $.when();
        } else {
			// this._super();
			return $.when(this._super(action, options));
        }
    },
    
});

});
