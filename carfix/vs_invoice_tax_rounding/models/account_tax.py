from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.float_utils import float_round as round

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def get_taxes_values(self):
        tax_grouped = super(AccountInvoice, self).get_taxes_values()
        if tax_grouped:
            for key_tax in tax_grouped:
                tax_grouped[key_tax]['amount'] = int(tax_grouped[key_tax]['amount'])
        return tax_grouped

    def _compute_amount(self):
        res = super(AccountInvoice, self)._compute_amount()
        for invoice_doc in self:
            invoice_doc.amount_tax = int(invoice_doc.amount_tax)
            invoice_doc.amount_total = int(invoice_doc.amount_total)
        return res
