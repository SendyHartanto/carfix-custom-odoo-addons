{
    'name': 'VS Invoice - Tax Rounding',
    'version': '11.1.0',
    'author': 'PT VISI',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http: //www.visi.co.id/',
    'summary': 'Custom-built Odoo',
    'description': '''
    ''',
    'depends': [
        'account', # python
    ],
    'data': [
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
