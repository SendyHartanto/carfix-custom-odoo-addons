{
    'name': 'Payment Request',
    'version': '11.1.0',
    'author': 'PT VISI',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http: //www.visi.co..id/',
    'summary': 'Custom-built Odoo',
    'description': '''
    ''',
    'depends': [
        'account', # python
    ],
    'data': [
        # 'views/_menu_item.xml',
        'views/ir_sequence.xml',
        'views/payment_request.xml',
        'views/account_invoice.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}