from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class AccountRegisterPayments(models.TransientModel):
    _inherit = "account.register.payments"


    @api.multi
    def create_payments(self):
        res = super(AccountRegisterPayments, self).create_payments()
        payment_request_id = self._context.get('payment_request_id', False)
        if payment_request_id:
            self.env['payment.request'].browse(payment_request_id).after_payment_callback()
        return res

    @api.model
    def default_get(self, fields):
        rec = super(AccountRegisterPayments, self).default_get(fields)
        payment_request_id = self._context.get('payment_request_id', False)
        if payment_request_id:
            rec.update({
                'communication': self._context.get('default_communication', 'Payment Request')
            })
        return rec

    communication = fields.Char(help="""
        Ketika user melakukan pelunasan AP melalui Payment Request, maka kolom Memo akan terisi
        dengan nomor Payment Request yang akan berpengaruh dengan nomor Referensi pada
        Journal Entries
    """)
