from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    payment_request_id = fields.Many2one('payment.request', 'Payment Request')
