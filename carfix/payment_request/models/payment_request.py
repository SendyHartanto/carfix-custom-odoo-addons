from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class PaymentRequest(models.Model):
    _name = 'payment.request'
    _inherit = ['mail.thread']

    READONLY_STATES = {
        'draft': [('readonly', False)],
    }

    name = fields.Char('Name', default='/')
    date_request = fields.Date(string='Date Requested', readonly=True, states=READONLY_STATES, default=fields.Date.today(), )
    date_planned = fields.Date(string='Date Planned', readonly=True, states={'waiting':[('readonly', False)]}, copy=False)
    user_id = fields.Many2one('res.users', string='User', readonly=True, states=READONLY_STATES, default=lambda self: self.env.user, copy=False)
    user_approve_id = fields.Many2one('res.users', 'Approved By', readonly=True, copy=False)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, copy=False)
    partner_id = fields.Many2one('res.partner', 'Supplier', readonly=True, states=READONLY_STATES)

    state = fields.Selection([
            ('draft', 'Draft'),
            ('waiting', 'Waiting Approval'),
            ('approve', 'Approved'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], string='Status', default='draft',
        help="""
            Draft - ketika Staf Accounting membuat dokumen pertama kali
            Waiting - ketika Staf Accounting menunggu approval dari Manager Finance
            Approved - ketika Manager Finance sudah melakukan approval
            Paid - ketika Staf Accounting sudah melakukan transaksi pembayaran
        """,
        track_visibility='onchange', copy=False)

    invoice_ids = fields.Many2many(
        comodel_name='account.invoice',
        relation='payment_request_invoice_rel',
        column1='payment_request_id',
        column2='invoice_id',
        string='Invoice',
        copy=False,
        readonly=True,
        states=READONLY_STATES,
        )

    @api.multi
    def action_cancel(self):
        for doc in self:
            doc.state = 'cancel'

            # Lepas Flag Semua
            for inv_id in doc.invoice_ids:
                inv_id.payment_request_id = False

    @api.multi
    def action_to_waiting(self):
        self.ensure_one()
        for doc in self:
            doc.name = self.env['ir.sequence'].next_by_code('payment.request')
            doc.state = 'waiting'
            # Cek dulu ada yang udah nyangkut
            inv_bermasalah = doc.invoice_ids.filtered(lambda x: x.payment_request_id)
            if inv_bermasalah:
                invoice_and_request = ""
                for inv_doc in inv_bermasalah:
                    invoice_and_request += "Vendor Bill : %s - Payment Request : %s (%s) \n" % (inv_doc.number, inv_doc.payment_request_id.name, inv_doc.payment_request_id.state)
                message = '''%s Vendor Bill(s) sudah memiliki Payment Request
                    %s
                ''' % (len(inv_bermasalah), invoice_and_request)
                raise UserError(message)

            # Kasih Flag Semua
            for inv_id in doc.invoice_ids:
                inv_id.payment_request_id = self.id

    @api.multi
    def action_approve(self):
        for doc in self:
            if not doc.date_planned:
                raise UserError("Date Planned harus terisi")
            if doc.date_request > doc.date_planned:
                raise UserError("Tanggal pada kolom Date Planned harus lebih dibanding kolom tanggal pada Date Requst")
            doc.user_approve_id = self.env.user
            doc.state = 'approve'

    @api.multi
    def action_pay(self):
        for doc in self:
            form_view_id = self.env.ref('account.view_account_payment_from_invoices')
            invoice_ids_amount = sum(self.invoice_ids.mapped('residual_signed'))
            invoice_ids_to_pay = [invoice_id.id for invoice_id in self.invoice_ids]
            return {
                'name': 'Pembayaran AP - (Multiple)',
                'view_type': 'form',
                'view_mode': 'form',
                'views': [[form_view_id.id, 'form']],
                'context': {
                    'default_payment_type': 'outbound',
                    'active_ids': invoice_ids_to_pay,
                    'payment_request_id': self.id,
                    'default_communication': self.name,
                },
                'res_model': 'account.register.payments',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }


    @api.multi
    def after_payment_callback(self):
        for doc in self:
            doc.state = 'paid'


    def payment_request_get_journal(self):
        domain = ['|', ('name','ilike', self.name), ('ref','ilike', self.name)]
        return {
            'name': 'Related Journal Entry',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain' : domain,
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
        }
