from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    def partner_get_outstanding_ar(self):
        moveline_obj = self.env['account.move.line']
        movelines = moveline_obj.search([
            ('partner_id', '=', self.id),
            #  ('account_id.user_type_id.name', 'in', ['Receivable', 'Payable']),
            ('account_id.user_type_id.name', '=', 'Receivable'),
            ('full_reconcile_id', '=', False)
        ])
        views = [(self.env.ref('partner_unpaid_ar.account_move_line_outstanding_ar_tree').id, 'tree'),
                (self.env.ref('account.view_move_line_form').id, 'form')]

        return {
            'type': 'ir.actions.act_window',
            'name': 'Outstanding AR',
            'res_model': 'account.move.line',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': views,
            'domain': [('id', 'in', movelines.ids)],
            # 'search_view_id': search_id,
            'context': {
                'search_default_date_maturity_invoice': True,
                # 'search_default_invoice_id': True,
                # 'group_by_no_leaf':1,
                # 'group_by':[]
            },
            # 'target': 'new',
        }
