{
    'name': 'Partner Unpaid AR',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Accounting',
    'website': 'http://visi.co.id/',
    'summary': 'Summary AR yang outstanding per Today',
    'depends': [
        'account',
    ],
    'data': [
        'views/partner_view.xml',
        'views/account_move_line.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
