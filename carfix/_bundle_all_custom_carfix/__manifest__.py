{
    'name': 'Bundle - Custom - Carfix',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http://visi.co.id/',
    'sequence': 0,
    'summary': 'One-Click Installation',
    'description': '''
        One-Click Multi Module Installations !

        cukup sekali klik untuk install untuk Customized-Module yang diperlukan
    ''',
    'depends': [
    # 8872 DEPENDENCY TREE
    #Hide Fields,
        'accounting_basic',
    #Core API,
        'rest_api',
    #API Account Receivable Related,
        'api_product_product',
        'api_res_partner',
        'api_account_invoice',
        'vs_account_discount_value',
    ],
    'data': [
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
