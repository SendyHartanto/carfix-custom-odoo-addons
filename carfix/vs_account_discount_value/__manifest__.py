{

    'name': 'VS - Account Discount Value',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Backend',
    'website': 'http://visi.co.id/',
    'summary': 'Discount Value',
    'description': '''


    Referensi : http://ortax.org/ortax/?mod=forum&page=show&idtopik=45207

    Purchase Discount

    https://smallbusiness.chron.com/recording-discount-accounts-payable-33706.html


    https://www.accountingcoach.com/blog/purchase-trade-discounts
    https://www.accountingcoach.com/blog/what-is-a-purchase-discount

    https://accountingexplained.com/financial/inventories/avco-method

    http://thomyhands.blogspot.com/2017/05/perhitungan-stock-persediaan-gudang.html

    Requirement :

    1. Diskon pada Invoice bukan persen, tapi nominal


    ''',
    'depends': [
        'account',
    ],
    'data': [
            'views/account_view.xml',
            'views/konfigurasi_diskon.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
