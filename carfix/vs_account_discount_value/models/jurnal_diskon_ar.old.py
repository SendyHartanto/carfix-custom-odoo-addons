from odoo import models, fields, api, _

class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        self.ensure_one()
        sum_of_disc_amount = 0
        for line in self.invoice_line_ids:
            disc_amount = 0
            if line.discount_nominal > 0:
                disc_amount = line.discount_nominal
            if line.discount > 0:
                disc_amount = line.price_unit * ((line.discount or 0.0) / 100.0)
            if disc_amount > 0:
                sum_of_disc_amount += disc_amount

        # Jika ada Diskon Amoount
        if sum_of_disc_amount > 0:
            #1. Move untuk Beban Diskon / Marketing
            account_ar = self.partner_id.property_account_receivable_id
            debit_moves = (0, 0, {
                'name': 'Beban Diskon',
                'debit': sum_of_disc_amount,
                'credit': 0,
                'partner_id': self.partner_id.id,
                'account_id': account_ar.id,
            })
            #2. Masukkan ke Move Lines yg sudah ada
            move_lines.insert(0, debit_moves)

            #3. Cari nilai AR, lalu kurangin dengan sum of diskon
            for line in move_lines:
                if line[2].get('invoice_id') and line[2].get('account_id') == self.account_id.id:
                    line[2]['debit'] -= sum_of_disc_amount

        return super(AccountInvoice, self).finalize_invoice_move_lines(move_lines)
