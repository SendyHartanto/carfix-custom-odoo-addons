from odoo import models, fields, api, _
from odoo.exceptions import UserError

class ProductProduct(models.Model):
    _inherit = "product.product"

    @api.model
    def _convert_prepared_anglosaxon_line(self, line, partner):
        result = super(ProductProduct, self)._convert_prepared_anglosaxon_line(line, partner)
        result.update({
            'invl_id' : line.get('invl_id', False),
        })
        return result


class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        self.ensure_one()
        company_id = self.company_id
        sum_of_disc_amount = 0
        product_id_vs_disc_amount = {}
        for line in self.invoice_line_ids:
            disc_amount = 0
            # bisa pakai line.price_reduce
            if line.discount_nominal > 0:
                disc_amount = line.discount_nominal
            elif line.discount > 0:
                disc_amount = line.price_unit * ((line.discount or 0.0) / 100.0)  * line.quantity
            if disc_amount > 0:
                product_id_vs_disc_amount[line.id] = disc_amount
                sum_of_disc_amount += disc_amount
        # Jika ada Diskon Amoount
        if sum_of_disc_amount > 0:
            #1. Move untuk SEMUA Beban Diskon / Marketing
            if self.type in ['out_invoice', 'out_refund']:
                account_beban = self.with_context(company_id=company_id.id, force_company=company_id.id).partner_id.property_account_disc_expense_partner_id
                if not account_beban:
                    raise UserError("Tidak ada konfigurasi 'Account Discount Expense' pada Customer %s. Konfigurasi dilakukan via Invoicing > Sales > Master Data > Customer" % self.partner_id.name)

            elif self.type in ['in_invoice', 'in_refund']:
                account_beban = self.with_context(company_id=company_id.id, force_company=company_id.id).partner_id.property_account_disc_income_partner_id
                if not account_beban:
                    raise UserError("Tidak ada konfigurasi 'Account Discount Income' pada Supplier %s. Konfigurasi dilakukan via Invoicing > Purchase > Master Data > Vendor" % self.partner_id.name)

            if self.type in ['in_invoice', 'out_refund']:
                diskon_moves = (0, 0, {
                    'name': 'Diskon',
                    'debit': 0,
                    'credit': sum_of_disc_amount,
                    'partner_id': self.partner_id.id,
                    'account_id': account_beban.id,
                })
                #2. Masukkan ke Move Lines yg sudah ada
                move_lines.insert(0, diskon_moves)

                #3. Cari Product, lalu ditambahkan nilai Diskon2nya.
                for move_line in move_lines:
                    move_line_content = move_line[2]
                    if move_line_content.get('product_id', False):
                        #Tambahkan Profitnya
                        invl_id = move_line_content.get('invl_id', False)
                        value_diskon_dari_produk = product_id_vs_disc_amount.get(invl_id, 0)
                        move_line_content['debit'] += value_diskon_dari_produk
            else:
                debit_moves = (0, 0, {
                    'name': 'Beban Diskon',
                    'debit': sum_of_disc_amount,
                    'credit': 0,
                    'partner_id': self.partner_id.id,
                    'account_id': account_beban.id,
                })
                #2. Masukkan ke Move Lines yg sudah ada
                move_lines.insert(0, debit_moves)

                #3. Cari Product, lalu ditambahkan nilai Diskon2nya.
                for move_line in move_lines:
                    move_line_content = move_line[2]
                    if move_line_content.get('product_id', False):
                        #Tambahkan Profitnya
                        invl_id = move_line_content.get('invl_id', False)
                        value_diskon_dari_produk = product_id_vs_disc_amount.get(invl_id, 0)
                        move_line_content['credit'] += value_diskon_dari_produk

            # #3. Cari nilai AR, lalu kurangin dengan sum of diskon
            # for line in move_lines:
            #     if line[2].get('invoice_id') and line[2].get('account_id') == self.account_id.id:
            #         line[2]['debit'] += sum_of_disc_amount
        return super(AccountInvoice, self).finalize_invoice_move_lines(move_lines)
