from odoo import addons #Untuk monkey Patch

from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp

def _compute_price_new(self):
    currency = self.invoice_id and self.invoice_id.currency_id or None
    price = self.price_unit - self.price_reduce # MONKEYPATCH # price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
    taxes = False
    if self.invoice_line_tax_ids:
        taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
    self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price
    self.price_total = taxes['total_included'] if taxes else self.price_subtotal
    if self.invoice_id.currency_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
        price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id._get_currency_rate_date()).compute(price_subtotal_signed, self.invoice_id.company_id.currency_id)
    sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
    self.price_subtotal_signed = price_subtotal_signed * sign


addons.account.models.account_invoice.AccountInvoiceLine._compute_price = _compute_price_new




def get_taxes_values_new(self):
    tax_grouped = {}
    for line in self.invoice_line_ids:
        price_unit = line.price_unit - line.price_reduce # MONKEYPATCH # price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
        taxes = line.invoice_line_tax_ids.compute_all(price_unit, self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
        for tax in taxes:
            val = self._prepare_tax_line_vals(line, tax)
            key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)
            if key not in tax_grouped:
                tax_grouped[key] = val
            else:
                tax_grouped[key]['amount'] += val['amount']
                tax_grouped[key]['base'] += val['base']
    return tax_grouped

addons.account.models.account_invoice.AccountInvoice.get_taxes_values = get_taxes_values_new


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    # @api.depends('price_unit', 'price_reduce', 'invoice_line_tax_ids', 'quantity',
    #     'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
    #     'invoice_id.date_invoice', 'invoice_id.date')
    @api.one
    @api.depends('price_reduce')
    def _compute_price(self):
        super(AccountInvoiceLine, self)._compute_price()

    price_reduce = fields.Float(string="Price Reduce", compute="_compute_price_reduce", digits=dp.get_precision('Product Price'), store=False)


    price_base_subtotal = fields.Float(string="Price Base Subtotal", compute="_compute_base_subtotal", store=True)

    @api.one
    @api.depends('product_id', 'price_unit', 'quantity')
    def _compute_base_subtotal(self):
        self.price_base_subtotal = self.price_unit * self.quantity
    # #Diganti Api One, biar ketika di override, tidak dua kali atau tiga kali looping.
    # @api.one
    # @api.depends('price_unit', 'discount')
    # def _compute_price_reduce(self):
    #     self.price_reduce = 0
