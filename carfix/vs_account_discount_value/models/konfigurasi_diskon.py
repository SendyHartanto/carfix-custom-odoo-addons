from odoo import models, fields, api, _

# class ProductCategory(models.Model):
#     _inherit = "product.category"

#     # property_account_disc_income_categ_id = fields.Many2one('account.account', company_dependent=True,
#     #     string="Disc Income Account", oldname="property_account_disc_income_categ",
#     #     domain=[('deprecated', '=', False)],
#     #     help="This account will be used when validating a customer invoice.")
#     property_account_disc_expense_categ_id = fields.Many2one('account.account', company_dependent=True,
#         string="Disc Expense Account", oldname="property_account_disc_expense_categ",
#         domain=[('deprecated', '=', False)],
#         help="The Discount expense is accounted for when a customer bill is validated")


class ResPartner(models.Model):
    _inherit = "res.partner"

    property_account_disc_expense_partner_id = fields.Many2one('account.account',
        company_dependent=True,
        string="Discount Expense",
        domain=[('deprecated', '=', False)],
        help="The Discount Expense is accounted for when a Customer Invoices is validated"
    )
    property_account_disc_income_partner_id = fields.Many2one('account.account',
        company_dependent=True,
        string="Discount Income",
        domain=[('deprecated', '=', False)],
        help="The Discount Income is accounted for when a Supplier Invoices is validated"
    )
    property_account_disc_freight_partner_id = fields.Many2one('account.account',
        company_dependent=True,
        string="Freight Income",
        domain=[('deprecated', '=', False)],
        help="The Freight Income is accounted for when a Supplier Invoices is validated"
    )
    property_account_disc_income_total_partner_id = fields.Many2one('account.account',
        company_dependent=True,
        string="Discount Total Income",
        domain=[('deprecated', '=', False)],
        help="The Discount Total Income is accounted for when a Supplier Invoices is validated"
    )
