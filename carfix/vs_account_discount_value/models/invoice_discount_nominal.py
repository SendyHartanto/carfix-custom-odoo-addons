from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.addons import decimal_precision as dp

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    discount_total = fields.Float(string='Add. Discount (%)')
    discount_nominal_total = fields.Monetary(string='Add. Discount', digits=dp.get_precision('Product Price'))

    summary_netto = fields.Monetary(compute='_summary_netto', string='Netto')

    def _summary_netto(self):
        for inv_doc in self:
            summary_netto = sum(inv_doc.invoice_line_ids.mapped('netto'))
            inv_doc.summary_netto = summary_netto

    @api.onchange('discount_total')
    def _compute_discount_nominal_total(self):
        if self.summary_netto > 0 and self.discount_total > 0:
            self.discount_nominal_total = (self.summary_netto * self.discount_total) / 100

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    discount_total = fields.Float(string='Discount Total', digits=dp.get_precision('Product Price'))
    discount_nominal = fields.Float(string='Discount', digits=dp.get_precision('Product Price'))
    freight = fields.Float(string='Freight')

    netto = fields.Float('Price Subtotal Before Discount Total', compute='_compute_netto')

    @api.one
    @api.depends('price_subtotal', 'discount_total')
    def _compute_netto(self):
        self.netto = self.price_subtotal + self.discount_total

    @api.onchange('discount_nominal')
    def _hilangkan_discount_persentase(self):
        if self.discount_nominal > 0:
            self.discount = 0

    @api.onchange('discount')
    def _hilangkan_discount_nominal(self):
        if self.discount > 0:
            self.discount_nominal = 0

    @api.one
    @api.depends('discount', 'discount_nominal', 'freight', 'discount_total')
    def _compute_price_reduce(self):
        discount_percentage_total = self.discount + self.freight
        discount_nominal = self.discount_nominal

        # import ipdb; ipdb.set_trace()
        # if self.quantity == 0:
        #     message = '''Pembuatan Invoice %s Gagal.
        #         Ditemukan Baris Invoice dengan Quantity 0 pada produk %s dengan Harga %s
        #         Sehingga Odoo tidak bisa mengkalkulasi terkait perhitungan Discount
        #     ''' % (self.invoice_id.no_invoice, self.name, self.price_unit)
        #     raise UserError(message)

        if self.quantity == 0:
            discount_total = 0
        else:
            discount_total = self.discount_total / self.quantity

        if discount_nominal > 0:
            if self.quantity == 0:
                price_reduce = 0.0
            else:
                price_reduce = discount_nominal / self.quantity
            self.price_reduce = price_reduce + discount_total
            return price_reduce

        elif discount_percentage_total > 0:
            price_reduce = self.price_unit * ((discount_percentage_total or 0.0) / 100.0)
            self.price_reduce = price_reduce + discount_total
            return price_reduce

        else:
            self.price_reduce = discount_total
            return discount_total
