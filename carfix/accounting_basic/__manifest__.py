{

    'name': 'Accounting Basic',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Backend',
    'website': 'http://visi.co.id/',
    'summary': 'Restful Api Service',
    'description': '''

    Requirement :

    1. Hide Payment Term
    2. Due Date diisi sendiri
    3.


    ''',
    'depends': [
        'account',
    ],
    'data': [
            'views/account_view.xml'
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
