from odoo import models, fields, api, _

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def _onchange_payment_term_date_invoice(self):
        return False

    def _onchange_partner_id(self):
        super(AccountInvoice, self)._onchange_partner_id()
        self.payment_term_id = False
