import json

from odoo import http, _
from odoo.http import request, route


class ApiTest(http.Controller):

    @route('/api/test', methods=['GET'], type='json', auth='public', csrf=False)
    def get_partner(self, debug=False, **k):
        method = request.httprequest.method
        arguments = request.httprequest.args
        result = {}
        result['count'] = 4
        return result
