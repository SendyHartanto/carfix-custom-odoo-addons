{

    'name': 'Rest Api',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Backend',
    'website': 'http://visi.co.id/',
    'summary': 'Restful Api Service',
    'description': '''

    Requirement :

    1. pip install PyJWT


    ''',
    # 'external_dependencies': {
    #     'python': ['pyjwt'],
    # },
    'depends': [
                # 'product',
                # 'stock',
                # 'point_of_sale',
                # 'hr',
                # 'administrative_area_level_1',
                # 'administrative_area_level_2',
                # 'administrative_area_level_3',
                # 'administrative_area_level_4',
                # 'pos_promotion',
                # 'pb_informasi_toko',
                # 'pb_reporting_xlsx', #Store Main Region ada disini
    ],
    'data': [
            # 'pb_barcode_seq.xml',
            # 'views/refresh_token.xml'
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
