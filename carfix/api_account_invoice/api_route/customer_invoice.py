import json

from odoo import http, _
from odoo.http import request, route
from odoo.exceptions import UserError


class CustomerInvoice(http.Controller):

    @route('/api/invoice/customer', methods=['POST'], type='json', auth='public', csrf=False)
    # @token_required()
    def post_customer_invoice(self, **kwargs):
        body = request.jsonrequest
        try:
            # with request.env.cr.savepoint():
            result = request.env['account.invoice'].new_customer_invoice(body, **kwargs)
        except Exception as E:
            if isinstance(E, UserError):
                return {'error' : {'code': 501, 'message': E.name}}
            else:
                raise E
        return {'result' : result, 'code': 201 }

        # try:
        #     with self._cr.savepoint():
        #         counterpart = self.process_reconciliation(counterpart_aml_dicts=counterpart_aml_dicts, payment_aml_rec=payment_aml_rec)
        #     return counterpart
        # except UserError:
        #     # A configuration / business logic error that makes it impossible to auto-reconcile should not be raised
        #     # since automatic reconciliation is just an amenity and the user will get the same exception when manually
        #     # reconciling. Other types of exception are (hopefully) programmation errors and should cause a stacktrace.
        #     self.invalidate_cache()
        #     self.env['account.move'].invalidate_cache()
        #     self.env['account.move.line'].invalidate_cache()
        #     return False

    @route('/api/invoice/customer', methods=['GET'], type='json', auth='public', csrf=False)
    def get_customer_invoice(self, debug=False, **k):
        method = request.httprequest.method
        arguments = request.httprequest.args
        # limit=arguments.get('limit', False)
        # offset=arguments.get('offset', False)
        # order=arguments.get('order', 'write_date asc')
        # write_date_from=arguments.get('write_date_from', False)
        # write_date_to=arguments.get('write_date_to', False)
        # if limit:
        #     limit = int(limit)
        # if offset:
        #     offset = int(offset)
        # employee, count = request.env['hr.employee'].sudo().get_employee(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        # result['result'] = employee
        result['count'] = 4
        return result
