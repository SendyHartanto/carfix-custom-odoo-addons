import json

from odoo import http, _
from odoo.http import request, route
from odoo.exceptions import UserError


class BarangMasukEndpoint(http.Controller):

    @route('/api/journal/stock/masuk', methods=['POST'], type='json', auth='public', csrf=False)
    # @token_required()
    def post_stock_masuk(self, **kwargs):
        body = request.jsonrequest
        try:
            result = request.env['barang.masuk'].sudo().new_barang_masuk(body, **kwargs)
        except Exception as E:
            if hasattr(E, 'name'):
                payload_log = E.name
            else:
                payload_log = str(E)
            return {'error' : {'code': 501, 'message': payload_log}}
        return {
            'result' : result,
            'code': 201,
        }
