import json

from odoo import http, _
from odoo.http import request, route
from odoo.exceptions import UserError


class SupplierInvoice(http.Controller):

    @route('/api/invoice/supplier', methods=['POST'], type='json', auth='public', csrf=False)
    # @token_required()
    def post_supplier_invoice(self, **kwargs):
        body = request.jsonrequest
        try:
            result = request.env['account.invoice'].new_supplier_invoice(body, **kwargs)
        except Exception as E:
            if isinstance(E, UserError):
                return {'error' : {'code': 501, 'message': E.name}}
            else:
                raise E
        return {'result' : result, 'code': 201 }

    @route('/api/invoice/supplier', methods=['GET'], type='json', auth='public', csrf=False)
    def get_supplier_invoice(self, debug=False, **k):
        method = request.httprequest.method
        arguments = request.httprequest.args
        # limit=arguments.get('limit', False)
        # offset=arguments.get('offset', False)
        # order=arguments.get('order', 'write_date asc')
        # write_date_from=arguments.get('write_date_from', False)
        # write_date_to=arguments.get('write_date_to', False)
        # if limit:
        #     limit = int(limit)
        # if offset:
        #     offset = int(offset)
        # employee, count = request.env['hr.employee'].sudo().get_employee(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        # result['result'] = employee
        result['count'] = 4
        return result
