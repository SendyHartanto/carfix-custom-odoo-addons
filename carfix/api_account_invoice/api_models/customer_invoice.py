import json
from odoo import models, fields, api, _
from odoo.addons.rest_api.http import DetailException
from odoo.exceptions import UserError, ValidationError

'''
    Remove duplicate elements from list
'''
def removeDuplicates(listofElements):

    # Create an empty list to store unique elements
    uniqueList = []

    # Iterate over the original list and for each element
    # add it to uniqueList, if its not already there.
    for elem in listofElements:
        if elem not in uniqueList:
            uniqueList.append(elem)

    # Return the list of unique elements
    return uniqueList

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    # invoice_line_ids = fields.One2many('account.invoice.line', 'invoice_id', string='Invoice Lines2', domain=[("tipe_produk", "=", "Job")])
    invoice_line_job_ids = fields.One2many('account.invoice.line', compute='_filter_invoice_line', string='Job')
    invoice_line_part_ids = fields.One2many('account.invoice.line', compute='_filter_invoice_line', string='Part')
    invoice_line_sublet_ids = fields.One2many('account.invoice.line', compute='_filter_invoice_line', string='Sublet')

    amount_tax_ppnbm = fields.Monetary(string='PPn-BM (0%)', help="Perhitungan pajak PPn BM. Nilai selalu 0", default=0)
    amount_tax_saci = fields.Monetary(string='Tax', help="Sebagai pembanding perhitungan Tax dari kedua sistem SACI dan Odoo. Dikhawatirkan ada masalah miskonfigurasi")

    def _assign_tipe_customer(self, payload_tipe_customer):
        return False

    @api.depends('invoice_line_ids')
    def _filter_invoice_line(self):
        for inv in self:
            for document in inv:
                document.invoice_line_job_ids = inv.invoice_line_ids.filtered(lambda x: x.tipe_produk == "Job")
                document.invoice_line_part_ids = inv.invoice_line_ids.filtered(lambda x: x.tipe_produk == "Part")
                document.invoice_line_sublet_ids = inv.invoice_line_ids.filtered(lambda x: x.tipe_produk == "Sublet")

    def create_new_partner(self, payload, is_supplier=False):
        customer_id = payload.get('customer_id', False)
        supplier_id = payload.get('supplier_id', False)
        supplier_name = payload.get('nama', 'New Supplier from API')
        php_id = customer_id or supplier_id
        partner_value = {
            'name': supplier_name if is_supplier else payload['nama'],
            'php_id': php_id,
            'customer': not is_supplier,
            'supplier': is_supplier,
        }

        res_partner_obj = self.sudo().env['res.partner']

        customer_id = res_partner_obj.create(partner_value)
        return customer_id

    def process_customer(self, payload):
        customer_id = payload.get('customer_id', False)
        if not customer_id:
            raise DetailException(400, "Key customer_id tidak ada pada struktur JSON")
        search_query = [('php_id', '=', customer_id)]
        res_partner_obj = self.sudo().env['res.partner']
        partner_id = res_partner_obj.search(search_query, limit=1)
        if not partner_id:
            partner_id = self.create_new_partner(payload)
        #Process New Data
        partner_id.write(
            {
                "street" : payload.get('alamat', ''),
                "ref" : payload.get('kode_customer', ''),
                "npwp" : payload.get('npwp', ''),
                "credit_limit" : payload.get('credit_limit', 0),
            }
        )
        return partner_id

    def create_new_product(self, payload):
        product_dict = {
            'name': payload['name'],
            'php_id': payload['product_id'],
            'company_id': False,
        }

        product_product_obj = self.sudo().env['product.product']
        product_id = product_product_obj.create(product_dict)
        return product_id

    def process_product(self, payload):
        search_query = [('php_id', '=', payload['product_id'])]
        product_product_obj = self.sudo().env['product.product']
        product_id = product_product_obj.search(search_query, limit=1)
        if not product_id:
            payload.update({
                'default_code' : payload.get('product_code', False),
            })
            return self.create_new_product(payload)
        return product_id

    def process_company(self, payload):
        company_code = payload.get('company_code')
        search_query = [('ref', '=', company_code)]
        res_company_obj = self.sudo().env['res.company']
        company_id = res_company_obj.search(search_query, limit=1)
        if not company_id:
            search_query = [('ref', '=', False)]
            res_company_obj = self.sudo().env['res.company']
            company_id = res_company_obj.search(search_query, limit=1)
            return company_id
        return company_id

    def _mergeDefaultAndProductTaxes(self, product_id, supplier_invoice=False):
        IrDefault = self.env['ir.default']
        force_company = self._context.get('force_company')
        if not force_company:
            force_company = self.env.user.company_id.id
        if not supplier_invoice:
            default_tax_id = IrDefault.get('product.template', "taxes_id", company_id=force_company)
        else:
            default_tax_id = IrDefault.get('product.template', "supplier_taxes_id", company_id=force_company)
        if not default_tax_id:
            res_company_obj = self.sudo().env['res.company']
            raise DetailException(400, "Default Sales/Purchase Tax untuk Company %s belum ada ! (Invoicing > Configuration > Settings > Taxes)" % (res_company_obj.browse(force_company).name, ))
        # product_taxes = product_id.taxes_id.filtered(lambda x: x.company_id.id == force_company).mapped('id') or []
        product_taxes = [] #Sementara hanya dipakai Tax yang ada di Setting
        merged_taxes = removeDuplicates(default_tax_id + product_taxes)
        return merged_taxes


    def change_journal_and_invoice_name(self, invoice_doc, move_name):
        invoice_doc.move_id.write(
            {
                'name' :    move_name
            }
        )
    def new_customer_invoice(self, body, **kwargs):

        payload_api = body.get('data', False)

        company_id = self.process_company(payload_api)
        self = self.with_context(company_id=company_id.id, force_company=company_id.id)

        #Minimum Payload adalah account_id dan partner_id
        # dict_customer_invoice={'account_id': 7, 'partner_id': 1, 'company_id' : 1}
        # products = self.with_context(force_company=company and company.id or self._context.get('force_company', self.env.user.company_id.id)).sudo()

        partner_id = self.with_context(company_id=company_id.id, force_company=company_id.id).process_customer(payload_api)
        account_ar = partner_id.property_account_receivable_id.filtered(lambda x: x.company_id.id == company_id.id)
        if not account_ar:
            raise DetailException(400, "Akun AR belum ada. Segera Konfigurasi Akun AR untuk customer %s untuk Company %s !." % (partner_id.name, company_id.name, ))
        dict_customer_invoice = {
            'payload': json.dumps(payload_api),
            'create_date_api' : fields.Datetime.now(),
            'account_id': account_ar.id,
            'partner_id': partner_id.id,
            'company_id': company_id.id,
        }
        dict_customer_invoice.update(payload_api)
        dict_customer_invoice.update({
            'date_invoice' : payload_api['tanggal_invoice'],
            'date_due' : payload_api['tanggal_pembayaran'],
        })
        #Minimum Payload untuk Invoice Line
        #{"product_id" : 34, "name": "Barang A", "price_unit": 120000, 'account_id' : 17,}

        #Cek Tipe Customer
        payload_tipe_customer = payload_api.get('tipe')
        if payload_tipe_customer:
            tipe_customer = self._assign_tipe_customer(payload_tipe_customer)

            dict_customer_invoice.update({
                'tipe_customer' : tipe_customer
            })
            # except Exception as E:
        else:
            raise DetailException(400, "Invoice tidak memiliki Tipe Invoice. (misal : retail)")


        new_invoice_lines = []

        # _get_product_accounts
        # account = self.product_id.product_tmpl_id._get_product_accounts()['expense']

        #A. Untuk Pekerjaan
        if 'job' in payload_api.keys():
            for line in payload_api['job']:
                product_id = self.process_product(line)
                # taxes = self.product_id.taxes_id or self.account_id.tax_ids
                account_income_id = product_id.property_account_income_id or product_id.categ_id.property_account_income_categ_id
                if not account_income_id:
                    raise DetailException(400, "Akun Income belum ada. Cek Konfigurasi Account Income : Produk '%s' atau Kategori Produk '%s' untuk cabang %s" % (product_id.name, product_id.categ_id.name, company_id.name))
                merged_taxes = self._mergeDefaultAndProductTaxes(product_id)
                line.update({
                    'product_id' : product_id.id,
                    'account_id' : account_income_id.id,
                    'invoice_line_tax_ids' : [(4, tax) for tax in merged_taxes],
                    'tipe_produk' : "Job",
                })
                line.pop("discount", None)
                new_invoice_lines.append((0, False, line))

        #B. Untuk Part
        if 'part' in payload_api.keys():
            for line in payload_api['part']:
                product_id = self.process_product(line)
                account_income_id = product_id.property_account_income_id or product_id.categ_id.property_account_income_categ_id
                if not account_income_id:
                    raise DetailException(400, "Akun Income belum ada. Cek Konfigurasi Account Income : Produk '%s' atau Kategori Produk '%s' untuk cabang %s" % (product_id.name, product_id.categ_id.name, company_id.name))
                merged_taxes = self._mergeDefaultAndProductTaxes(product_id)
                line.update({
                    'product_id' : product_id.id,
                    'account_id' : account_income_id.id,
                    # 'invoice_line_tax_ids' : [(6,, tax) for tax in merged_taxes],
                    'invoice_line_tax_ids': [(6, 0, [tax for tax in merged_taxes])],

                    'tipe_produk' : "Part",
                })
                line.pop("discount", None)
                new_invoice_lines.append((0, False, line))

        #C. Untuk Sublet
        if 'sublet' in payload_api.keys():
            for line in payload_api['sublet']:
                product_id = self.process_product(line)
                account_income_id = product_id.property_account_income_id or product_id.categ_id.property_account_income_categ_id
                if not account_income_id:
                    raise DetailException(400, "Akun Income belum ada. Cek Konfigurasi Account Income : Produk '%s' atau Kategori Produk '%s' untuk cabang %s" % (product_id.name, product_id.categ_id.name, company_id.name))
                merged_taxes = self._mergeDefaultAndProductTaxes(product_id)
                line.update({
                    'product_id' : product_id.id,
                    'account_id' : account_income_id.id,
                    'invoice_line_tax_ids' : [(4, tax) for tax in merged_taxes],
                    'tipe_produk' : "Sublet",
                })
                line.pop("discount", None)
                new_invoice_lines.append((0, False, line))

        #Append setelah Part dan Job have been collected
        dict_customer_invoice.update({
            'invoice_line_ids' : new_invoice_lines
        })


        #PPN SACI
        dict_customer_invoice.update({
            'amount_tax_saci' : payload_api.get('ppn_10', 0)
        })



        #Creating Customer Invoice
        customer_invoice_doc = self.sudo(1).with_context(company_id=company_id.id, force_company=company_id.id).create(dict_customer_invoice)
        customer_invoice_doc_id = customer_invoice_doc.id


        #Customer Invoice to Open
        self.env.cr.commit()
        self.env.cr.execute('SAVEPOINT draft_invoice')
        try:
            # with self.env.cr.savepoint():
            #     customer_invoice_doc.with_context(company_id=company_id.id, force_company=company_id.id).action_invoice_open()
            customer_invoice_doc.with_context(company_id=company_id.id, force_company=company_id.id).action_invoice_open()
            customer_invoice_doc.with_context(company_id=company_id.id, force_company=company_id.id).cetak_faktur_pajak()
        except Exception as E:
            if hasattr(E, 'name'):
                payload_log = E.name
            else:
                payload_log = str(E)
            self.env.cr.execute('ROLLBACK TO SAVEPOINT draft_invoice')
            self.sudo().browse(customer_invoice_doc_id).payload_log = payload_log
            raise E
        return {
            'customer_invoice_id' : customer_invoice_doc.id,
            'customer_invoice_number' : customer_invoice_doc.number,
        }

    @api.multi
    def action_invoice_open(self):
        result = super(AccountInvoice, self).action_invoice_open()
        for invoice_doc in self:
            if invoice_doc.no_invoice:
                try:
                    self.change_journal_and_invoice_name(invoice_doc, invoice_doc.no_invoice)
                except Exception as E:
                    if str(E).find('violates unique constraint'):
                        raise UserError("Nomor Invoice %s sudah ada di Odoo. Nomor Invoice harus unik dan tidak boleh sama."  % (invoice_doc.no_invoice))
        return result
