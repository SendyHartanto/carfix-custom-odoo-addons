from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError




class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def _prepare_tax_line_vals(self, line, tax):
        vals = super(AccountInvoice, self)._prepare_tax_line_vals(line, tax)
        force_pph_amount = self._context.get('force_pph_amount')
        pph_tax = 'pph' in tax['name'].lower()
        if force_pph_amount and pph_tax:
            vals['name'] = "PPH 22 %s %% " % force_pph_amount
        return vals


class AccountTax(models.Model):
    _inherit = 'account.tax'

    def _compute_amount(self, base_amount, price_unit, quantity=1.0, product=None, partner=None):
        force_pph_amount = self._context.get('force_pph_amount')
        pph_tax = 'pph' in self.name.lower()
        if force_pph_amount and pph_tax:
            if (self.amount_type == 'percent' and not self.price_include) or (self.amount_type == 'division' and self.price_include):
                return base_amount * force_pph_amount * 1 / 100
            else:
                return super(AccountTax, self)._compute_amount(base_amount, price_unit, quantity, product, partner)
        else:
            return super(AccountTax, self)._compute_amount(base_amount, price_unit, quantity, product, partner)
