from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        self.ensure_one()
        if self.type == 'in_invoice': # Khusus Vendor Bill
            company_id = self.company_id
            sum_of_diff_amount = 0
            product_id_vs_diff_amount = {}

            # Mencari Price Diff di setiap baris Invoice
            for inv_line in self.invoice_line_ids:
                price_diff = inv_line.price_diff * inv_line.quantity
                if price_diff != 0:
                    product_id_vs_diff_amount[inv_line.id] = price_diff
                    sum_of_diff_amount += price_diff
                    acc = inv_line.product_id.with_context(company_id=company_id.id, force_company=company_id.id).property_account_creditor_price_difference
                    if not acc:
                        # if not found on the product get the price difference account at the category
                        acc = inv_line.with_context(company_id=company_id.id, force_company=company_id.id).product_id.categ_id.property_account_creditor_price_difference_categ
                    if not acc:
                        raise UserError("Tidak ada konfigurasi 'Price Difference Account' pada Product %s." % inv_line.product_id.name)

            if product_id_vs_diff_amount.keys():
                for move_line in move_lines:
                    move_line_content = move_line[2]
                    if move_line_content.get('product_id', False): # Artinya account.move.line yang punya product
                        #Tambahkan SIA nya
                        invl_id = move_line_content.get('invl_id', False)
                        value_diff_dari_dict = product_id_vs_diff_amount.get(invl_id, 0)
                        move_line_content['debit'] += value_diff_dari_dict

                price_diff_summary_move = (0, 0, {
                    'name': 'Gain/Loss PO vs Inv',
                    'debit': -sum_of_diff_amount if sum_of_diff_amount < 0 else 0.0,
                    'credit': sum_of_diff_amount if sum_of_diff_amount > 0 else 0.0,
                    'partner_id': self.partner_id.id,
                    'account_id': acc.id,
                })
                #2. Masukkan ke Move Lines yg sudah ada
                move_lines.insert(0, price_diff_summary_move)

        return super(AccountInvoice, self).finalize_invoice_move_lines(move_lines)
