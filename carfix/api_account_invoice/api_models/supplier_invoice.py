import json
import logging
from odoo import models, fields, api, _
from odoo.addons.rest_api.http import DetailException
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def validate_payload_supplier(self, payload_api):
        missing_key = []
        required_key = [
            'supplier_id',
            'no_invoice',
            'company_code',
            'nama',
            'detail',
        ]
        for key in required_key:
            if not payload_api.get(key, False):
                missing_key.append(key)
        if missing_key:
            raise DetailException(400, "Key %s tidak ada pada struktur JSON" % (", ".join(missing_key)))

    def process_supplier(self, payload):
        supplier_id = payload.get('supplier_id', False)
        if not supplier_id:
            raise DetailException(400, "Key supplier_id tidak ada pada struktur JSON")
        search_query = [('php_id', '=', supplier_id)]
        res_partner_obj = self.sudo().env['res.partner']
        partner_id = res_partner_obj.search(search_query, limit=1)
        if not partner_id:
            partner_id = self.create_new_partner(payload, is_supplier=True)
        #Process New Data
        partner_id.write(
            {
                "street" : payload.get('alamat', ''),
                "ref" : payload.get('kode_supplier', ''),
                "npwp" : payload.get('npwp', ''),
            }
        )
        return partner_id

    def get_tax_pph_id(self, payload_api):
        invoice_name = payload_api.get('no_invoice')
        pph_total = payload_api.get('pph_total')
        tax_obj = self.env['account.tax']
        force_company = self._context.get('force_company')
        if not force_company:
            force_company = self.env.user.company_id.id
        search_query = [("company_id", "=", force_company), ("name", "ilike", "pph"), ("type_tax_use", "=", "purchase")]
        tax_doc = tax_obj.search(search_query, limit=1)
        company_name = self.env['res.company'].browse(force_company).name
        if not tax_doc:
            raise UserError('Invoice %s memiliki PPH sebesar %s tetapi Konfigurasi Tax PPH belum di set untuk Company %s' % (invoice_name, pph_total, company_name))
        return tax_doc


    def new_supplier_invoice(self, body, **kwargs):

        payload_api = body.get('data', False)
        self.validate_payload_supplier(payload_api)

        company_id = self.process_company(payload_api)
        self = self.sudo(1).with_context(company_id=company_id.id, force_company=company_id.id)

        #Minimum Payload adalah account_id dan partner_id dan tipe invoice in_invoice
        # dict_supplier_invoice={'account_id': 7, 'partner_id': 1}
        # partner_id = self.process_supplier(payload_api)
        partner_id = self.with_context(company_id=company_id.id, force_company=company_id.id).process_supplier(payload_api)
        account_ap = partner_id.property_account_payable_id.filtered(lambda x: x.company_id.id == company_id.id)
        if not account_ap:
            raise DetailException(400, "Akun AP belum ada. Segera Konfigurasi Akun AP untuk supplier %s untuk Company %s !." % (partner_id.name, company_id.name, ))

        dict_supplier_invoice = {
            'payload': json.dumps(payload_api),
            'create_date_api' : fields.Datetime.now(),
            'account_id': account_ap.id,
            'partner_id': partner_id.id,
            'company_id': company_id.id,
            'type': 'in_invoice',
        }
        dict_supplier_invoice.update(payload_api)
        dict_supplier_invoice.update({
            'date_invoice' : payload_api['tanggal_invoice'],
            'date_due' : payload_api['tanggal_jatuh_tempo_inv'],
        })
        #Minimum Payload untuk Invoice Line
        #{"product_id" : 34, "name": "Barang A", "price_unit": 120000, 'account_id' : 17,}

        #Process PPh
        pph_total = payload_api.get('pph_total', 0)
        try:
            if pph_total > 0:
                tax_pph_id = self.get_tax_pph_id(payload_api)
        except Exception as E:
            if hasattr(E, 'name'):
                payload_log = E.name
            else:
                payload_log = str(E)
            inv_creation_ctx = {
                'default_type': 'in_invoice',
                'type': 'in_invoice',
                'journal_type': 'purchase',
                'company_id' : company_id.id,
                'force_company' : company_id.id
            }
            #Creating Supplier Invoice
            supplier_invoice_doc = self.sudo(1).with_context(inv_creation_ctx).create(dict_supplier_invoice)
            supplier_invoice_doc_id = supplier_invoice_doc.id

            self.sudo().browse(supplier_invoice_doc_id).payload_log = payload_log
            raise E

        new_invoice_lines = []

        #A. Simplified. Product aja disini
        if 'detail' in payload_api.keys():
            for line in payload_api['detail']:
                product_id = self.process_product(line)
                # taxes = self.product_id.taxes_id or self.account_id.tax_ids
                account_expense_id = product_id.property_account_expense_id or product_id.categ_id.property_account_expense_categ_id
                if not account_expense_id:
                    raise DetailException(400, "Akun Expense untuk produk belum di-setting. Cek Konfigurasi Account Expense : Produk '%s' atau Kategori Produk '%s' untuk cabang %s" % (product_id.name, product_id.categ_id.name, company_id.name))
                merged_taxes = self._mergeDefaultAndProductTaxes(product_id, supplier_invoice=True)
                invoice_line_tax_ids = [(4, tax) for tax in merged_taxes]
                if pph_total:
                    invoice_line_tax_ids += [(4, tax_pph_id.id)]
                line.update({
                    'product_id' : product_id.id,
                    'account_id' : account_expense_id.id,
                    'invoice_line_tax_ids' : invoice_line_tax_ids,
                })
                new_invoice_lines.append((0, False, line))

        #Append Invoice Line
        dict_supplier_invoice.update({
            'invoice_line_ids' : new_invoice_lines
        })

        #PPN SACI
        dict_supplier_invoice.update({
            'amount_tax_saci' : payload_api.get('ppn_10', 0)
        })


        inv_creation_ctx = {
            'default_type': 'in_invoice',
            'type': 'in_invoice',
            'journal_type': 'purchase',
            'company_id' : company_id.id,
            'force_company' : company_id.id,
            'force_pph_amount' : pph_total,
        }

        #Creating Supplier Invoice
        supplier_invoice_doc = self.sudo(1).with_context(inv_creation_ctx).create(dict_supplier_invoice)
        supplier_invoice_doc.distribute_discount()
        supplier_invoice_doc_id = supplier_invoice_doc.id

        self.env.cr.commit()
        self.env.cr.execute('SAVEPOINT draft_invoice')
        try:
            supplier_invoice_doc.with_context(company_id=company_id.id, force_company=company_id.id).action_invoice_open()
        except Exception as E:
            if hasattr(E, 'name'):
                payload_log = E.name
            else:
                payload_log = str(E)
            self.env.cr.execute('ROLLBACK TO SAVEPOINT draft_invoice')
            self.sudo().browse(supplier_invoice_doc_id).payload_log = payload_log
            raise E
        # saci_no_invoice = payload_api.get('no_invoice')
        # if saci_no_invoice:
        #     try:
        #         self.change_journal_and_invoice_name(supplier_invoice_doc, saci_no_invoice)
        #     except Exception as E:
        #         # E.pgerror
        #         raise DetailException(400, "Nomor Invoice %s sudah ada di Odoo. Nomor Invoice harus unik dan tidak boleh sama."  % (payload_api.get('no_invoice')))
        return {
            'supplier_invoice_id' : supplier_invoice_doc.id,
            'supplier_invoice_number' : supplier_invoice_doc.number,
        }
