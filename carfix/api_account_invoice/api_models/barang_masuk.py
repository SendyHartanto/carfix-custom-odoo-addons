import json

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.addons.rest_api.http import DetailException

class BarangMasuk(models.Model):
    _inherit = 'barang.masuk'

    def validate_payload(self, payload_api):
        missing_key = []
        required_key = [
            'name',
            'debit_account',
            'credit_account',
            'move_date',
            'code_journal',
            'supplier_id',
            'product',
        ]
        for key in required_key:
            if not payload_api.get(key, False):
                missing_key.append(key)
        if missing_key:
            raise DetailException(400, "Key %s tidak ada pada struktur JSON" % (", ".join(missing_key)))

    def handle_line_from_api(self, payload):
        api_line_ids = payload.get('product', False)
        move_amount = 0
        for api_line_id in api_line_ids:
            create_value = api_line_id.copy()
            create_value.update({
                'bm_id' : self.id,
                'barang_masuk_id' : self.id,
            })
            self.env['barang.masuk.line'].create(create_value)
            product_name = api_line_id.get('product_name', False)
            quantity = api_line_id.get('quantity', 0)
            if quantity < 0:
                raise UserError("Qty untuk produk '%s' harus diatas 0. Cek key quantity" % (product_name))
            price = api_line_id.get('price', 0)
            if price < 0:
                raise UserError("Price untuk produk '%s' harus diatas 0. Cek key price" % (product_name))
            move_amount += price * quantity
        return move_amount

    def find_cogs_account(self, tipe_account):
        force_company = self._context.get('force_company')
        if not force_company:
            force_company = self.env.user.company_id.id
        coa_obj = self.env['account.account']
        company_id = self.env['res.company']
        if tipe_account == 'debit':
            account_code = self.debit_account
            search_query = [('company_id', '=', force_company), ("code", "=", account_code)]
            account = coa_obj.search(search_query, limit=1)
        else:
            account_code = self.credit_account
            search_query = [('company_id', '=', force_company), ("code", "=", account_code)]
            account = coa_obj.search(search_query, limit=1)
        if not account:
            raise DetailException(400, "Akun dengan Kode %s tidak ada pada Master COA pada company %s" % (account_code, company_id.browse(force_company).name))
        return account

    def create_new_journal(self, code_journal):
        force_company = self._context.get('force_company')
        if not force_company:
            force_company = self.env.user.company_id.id
        dict_new_journal = {
            'name': 'Stock - '+ code_journal,
            'type': 'general',
            'code': code_journal,
            'api_ref': code_journal,
            'company_id': force_company,
        }
        journal_obj = self.sudo().env['account.journal']
        journal_id = journal_obj.create(dict_new_journal)
        return journal_id

    def get_journal_id(self, code_journal):
        force_company = self._context.get('force_company')
        if not force_company:
            force_company = self.env.user.company_id.id
        journal_obj = self.env['account.journal']
        search_query = [('company_id', '=', force_company), ("api_ref", "=", code_journal)]
        journal_doc = journal_obj.search(search_query, limit=1)
        if not journal_doc:
            journal_doc = self.create_new_journal(code_journal)
        return journal_doc

    def _get_move_vals(self):
        code_journal = self.code_journal
        journal_doc = self.get_journal_id(code_journal)
        journal_name = journal_doc.sequence_id._next()
        return {
            'name': journal_name,
            'date': self.move_date,
            'ref': self.name,
            'company_id': self.env.user.company_id.id,
            'journal_id': journal_doc.id,
        }

    def validate_payload_supplier_id(self, supplier_id, payload_api):
        rpartner = self.env['res.partner']
        search_query = [("php_id", "=", supplier_id)]
        partner_doc = rpartner.search(search_query, limit=1)
        if not partner_doc:
            partner_doc = self.create_new_partner(supplier_id, payload_api)
        return partner_doc.id

    def create_new_partner(self, supplier_id, payload_api):
        dict_new_partner = {
            'name': payload_api['supplier_name'],
            'php_id': supplier_id,
            'customer': False,
            'supplier': True,
        }

        res_partner_obj = self.sudo().env['res.partner']
        partner_id = res_partner_obj.sudo().create(dict_new_partner)
        return partner_id

    def process_company(self, payload):
        company_code = payload.get('company_code')
        search_query = [('ref', '=', company_code)]
        res_company_obj = self.sudo().env['res.company']
        company_id = res_company_obj.search(search_query, limit=1)
        if not company_id:
            search_query = [('ref', '=', False)]
            res_company_obj = self.sudo().env['res.company']
            company_id = res_company_obj.search(search_query, limit=1)
            return company_id
        return company_id

    def new_barang_masuk(self, body, **kwargs):
        payload_api = body.get('data', False)
        bm_doc_name = payload_api.get('name', 'Unnamed Dokumen')
        self.validate_payload(payload_api)

        #handle Company
        company_id = self.process_company(payload_api)
        self = self.with_context(company_id=company_id.id, force_company=company_id.id)

        bm_obj = self.env['barang.masuk']
        #handle Header
        payload_api.update({
            'payload': json.dumps(payload_api),
            'company_id': company_id.id,
            'state': 'draft',
        })
        bm_doc = bm_obj.create(payload_api)

        #Creating Barang Masuk
        bm_doc_id = bm_doc.id

        self.env.cr.commit()

        move_amount = bm_doc.handle_line_from_api(payload_api)

        self.env.cr.execute('SAVEPOINT draft_lbm')
        try:
            move_doc = bm_doc.validate_barang_masuk(payload_api, move_amount)
        except Exception as E:
            if hasattr(E, 'name'):
                payload_log = E.name
            else:
                payload_log = str(E)
            self.env.cr.execute('ROLLBACK TO SAVEPOINT draft_lbm')
            self.sudo().browse(bm_doc_id).payload_log = payload_log
            raise E

        return {
            'barang_masuk_id' : bm_doc.id,
            'barang_masuk_name' : bm_doc.name,
            'journal_id' : move_doc.id,
            'journal_id_name' : move_doc.name,
        }

    def validate_barang_masuk(self, payload_api, move_amount):
        existing_bm_doc = self.search([('name', '=', self.name), ('state', '=', 'validate')])
        if existing_bm_doc:
            message = '''Sudah ada LBM dengan nomor %s''' % (self.name)
            raise UserError(message)
        #handle Account
        bm_doc = self
        debit_account_id = bm_doc.find_cogs_account('debit').id
        credit_account_id = bm_doc.find_cogs_account('credit').id

        #Handle Product
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        # if not self.destination_journal_id.default_credit_account_id:
            # raise UserError(_('The journal %s does not have a Default Credit Account, please specify one.') % self.destination_journal_id.name)
        move_doc = self.env['account.move'].create(bm_doc._get_move_vals())
        # dst_move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))

        #Move Line untuk Payment Method
        supplier_id = payload_api.get('supplier_id', False)
        supplier_id = self.validate_payload_supplier_id(supplier_id, payload_api)

        debit_aml_vals = {
            'name': 'Barang Masuk',
            'debit': move_amount,
            'credit': 0,
            'move_id' : move_doc.id,
            'account_id': debit_account_id,
        }
        #Move Line untuk Advance
        credit_aml_vals = {
            'name': 'Barang Masuk',
            'debit': 0,
            'credit': move_amount,
            'move_id' : move_doc.id,
            'partner_id': supplier_id,
            'account_id': credit_account_id,
        }

        #Pembentukkan Account Move Line
        #Di sortir terbalik
        aml_obj.create(credit_aml_vals)
        aml_obj.create(debit_aml_vals)
        move_doc.post()

        bm_doc.write({
            'move_id': move_doc.id,
            'state': 'validate',
        })
        return move_doc
