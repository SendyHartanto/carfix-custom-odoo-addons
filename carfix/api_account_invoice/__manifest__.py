{

    'name': 'Account Invoice AR dan AP',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Backend',
    'website': 'http://visi.co.id/',
    'summary': 'Restful Api Service',
    'description': '''
    Referensi Pph : http://www.academia.edu/4398765/Cara_Dan_Contoh_Perhitungan_Pajak_PPh_Pasal_23_Atas_Jasa_Service_AC

    Requirement :

    1. Field Alamat, NPWP, No Polisi, Mobil, Km, Keterangan, No Seri Pajak, Kode Customer, TOP, Rekening Bank
    2.
    3.


    ''',
    'depends': [
        'account',
        'vs_account_discount_value',
        'vs_efaktur',
    ],
    'data': [
            'views/account_view.xml',
            'views/barang_masuk.xml',
            'views/barang_masuk_line.xml',
            'views/supplier_invoice_view.xml',
            'views/res_company.xml',
            'views/account_move_line.xml',
            'views/account_journal.xml',

            'wizard/bm_tools.xml',

            'report/_menu_item.xml',
            'report/list_lbm.xml',
            'report/barang_masuk_line.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
