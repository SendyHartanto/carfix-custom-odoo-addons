from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class BarangMasukLine(models.Model):
    _inherit = 'barang.masuk.line'

    bm_id_name = fields.Char(related='bm_id.name', string='LBM', store=True)
    bm_id_move_date = fields.Date(related='bm_id.move_date', string='Barang Masuk Date', store=True)


    price_po_qty = fields.Float(compute='_compute_price_po_qty', string='Subtotal', store=True)

    @api.multi
    def _compute_price_po_qty(self):
        for session_doc in self:
            session_doc.price_po_qty = session_doc.price * session_doc.quantity
