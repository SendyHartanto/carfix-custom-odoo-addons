from odoo import models, fields, api, _
from odoo.exceptions import UserError

class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    def find_cogs_account(self, tipe_account):
        coa_obj = self.env['account.account']
        force_company = self._context.get('force_company')
        if not force_company:
            force_company = self.env.user.company_id.id
        search_query = [("company_id", "=", force_company)]
        if tipe_account == 'debit':
            search_query.append(("code", "=", self.coa_cogs_debit))
            account = coa_obj.search(search_query, limit=1)
            # if not account:
            #     raise UserError("Tidak ada akun '%s' pada Master Data COA" % self.coa_cogs_debit)
            return account
        else:
            search_query.append(("code", "=", self.coa_cogs_credit))
            account = coa_obj.search(search_query, limit=1)
            # if not account:
            #     raise UserError("Tidak ada akun '%s' pada Master Data COA" % self.coa_cogs_credit)
            return account

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        self.ensure_one()
        sum_of_disc_amount = 0
        product_id_vs_disc_amount = {}
        coa_cogs_debit = self.find_cogs_account('debit')
        coa_cogs_credit = self.find_cogs_account('credit')

        for line in self.invoice_line_ids:
            # Jika ada COGS amount
            if line.price_cogs > 0:
                if not coa_cogs_debit:
                    raise UserError("COGS Debit Error. Tidak ada akun '%s' pada Master Data COA" % self.coa_cogs_debit)
                if not coa_cogs_credit:
                    raise UserError("COGS Credit Error. Tidak ada akun '%s' pada Master Data COA" % self.coa_cogs_credit)
                #1. Move untuk Debit COGS
                debit_moves = (0, 0, {
                    'name': 'COGS',
                    'debit': line.price_cogs * line.quantity,
                    'credit': 0,
                    'partner_id': self.partner_id.id,
                    'account_id': coa_cogs_debit.id,
                })
                #2. Masukkan ke Move Lines yg sudah ada
                move_lines.insert(0, debit_moves)

                credit_moves = (0, 0, {
                    'name': 'COGS',
                    'debit': 0,
                    'credit': line.price_cogs * line.quantity,
                    'partner_id': self.partner_id.id,
                    'account_id': coa_cogs_credit.id,
                })
                #2. Masukkan ke Move Lines yg sudah ada
                move_lines.insert(0, credit_moves)

        return super(AccountInvoice, self).finalize_invoice_move_lines(move_lines)
