from odoo import models, fields, api, _
from odoo.exceptions import UserError

class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'
    account_company_id = fields.Many2one(related='account_id.company_id', string='Company')

class AccountMove(models.Model):

    _inherit = 'account.move'

    @api.multi
    def _check_lock_date(self):
        if self._context.get('by_pass_lock_date', False):
            return True
        else:
            result = super(AccountMove, self)._check_lock_date()
        return result

    @api.multi
    def _post_validate(self):
        for move in self:
            if move.line_ids:
                if not all([x.company_id.id == move.company_id.id for x in move.line_ids]):
                    name = move.name
                    journal_name = move.journal_id.name
                    company_id = move.journal_id.company_id
                    company_name = move.journal_id.company_id.name
                    message = """
                    
                        Berikut detail Perjurnalan yang gagal

                        ---- INFO : Account Move -----

                        Nama Transaksi Jurnal Entries : %s,
                        - Kategori Jurnal: %s,
                        - Company ID: %s,
                        - Nama Company : %s,
                    """ % (name, journal_name, company_id, company_name)
                    line_index = 0
                    for line in move.line_ids:
                        line_index += 1
                        name = line.name
                        debit = line.debit
                        credit = line.credit
                        account_id = line.account_id
                        account_id_code = line.account_id.code
                        account_id_name = line.account_id.name
                        account_id_company_name = line.account_id.company_id.name
                        company_id = line.company_id
                        company_name = line.company_id.name
                        data = """
                            ---- INFO : Account Move Line (Baris ke-%s) -----

                            Deskripsi Journal  : %s,
                            - Nilai Debit  : %s,
                            - Nilai Credit  : %s,

                            - Account ID  : %s,
                            - Account Name : (%s) %s,
                            - Account Company Name  : %s,

                            - Company ID  : %s,
                            - Company Name  : %s,
                        """ % (line_index, name, debit, credit, account_id, account_id_code, account_id_name, account_id_company_name, company_id, company_name)
                        message += data
                    raise UserError(_("Cannot create moves for different companies. %s" % message))
        result = super(AccountMove, self)._post_validate()
        return result
