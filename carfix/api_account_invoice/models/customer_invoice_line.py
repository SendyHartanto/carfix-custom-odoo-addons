from odoo import models, fields, api, _

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    tipe_produk = fields.Char('Tipe')
    price_cogs = fields.Float('COGS Value')
