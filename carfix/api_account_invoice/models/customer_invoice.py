from odoo import models, fields, api, _


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    payload = fields.Text('Payload')
    payload_log = fields.Text('Payload Log')
    create_date_api = fields.Datetime('Sent Date via API')

    no_invoice = fields.Char('Force Invoice Name')
    alamat = fields.Char('Alamat')
    npwp = fields.Char('NPWP')
    no_polisi = fields.Char('No Polisi')
    mobil = fields.Char('Mobil')
    km_mobil = fields.Char('Km')
    keterangan = fields.Char('Keterangan')
    no_seri_pajak = fields.Char('No Seri Pajak', help="Nomor Seri Pajak ini dikirim dari SACI. Odoo hanya perlu membuat laporan pajak")
    kode_customer = fields.Char('Kode Customer')
    top = fields.Char('TOP')
    rekening_bank = fields.Char('Rekening Bank')


    #COGS Part
    coa_cogs_debit = fields.Char('COA Debit untuk COGS', help="Akun ini didapatkan via API, (misal COGS - Related Parties)")
    coa_cogs_credit = fields.Char('COA Credit untuk COGS', help="Akun ini didapatkan via API, (misal Inventory)")

    coa_cogs_debit_id = fields.Many2one('account.account',
        string="COA Debit untuk COGS",
        help="Akun ini didapatkan via API"
    )
    coa_cogs_credit_id = fields.Many2one('account.account',
        string="COA Credit untuk COGS",
        help="Akun ini didapatkan via API"
    )
