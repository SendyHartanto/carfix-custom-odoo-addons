from odoo import models, fields, api, _


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    tanggal_buat = fields.Date('Tanggal Buat', default=fields.Date.context_today)
    tanggal_jatuh_tempo_ppn = fields.Date('Tgl Jatuh Tempo PPn')

    jenis_pembayaran = fields.Char('Jenis Pembayaran')
    kode_supplier = fields.Char('Kode Supplier')
    # partner_id_ref = fields.Char(related='partner_id.ref', string='Kode Supplier')

    #Sudah ada di Customer Invoice
    # no_seri_pajak = fields.Char('No Seri Pajak', help="Nomor Seri Pajak ini dikirim dari SACI. Odoo hanya perlu membuat laporan pajak")
    # keterangan = fields.Char('Keterangan')

    tanggal_no_seri_pajak = fields.Date('Tgl No Seri Pajak')

    status = fields.Char('Status')

    invoice_line_supplier_ids = fields.One2many('account.invoice.line', compute='_supplier_invoice_line', string='Part')

    @api.depends()
    def _supplier_invoice_line(self):
        for inv in self:
            for inv_doc in inv:
                inv_doc.invoice_line_supplier_ids = inv.invoice_line_ids


    summary_amount_ppn = fields.Monetary(compute='_supplier_summary', string='PPN')
    summary_amount_pph = fields.Monetary(compute='_supplier_summary', string='PPh')

    # @api.depends()
    def _supplier_summary(self):
        for inv_doc in self:
            #Hitung Summary Amount Discount
            summary_amount_ppn = 0
            summary_amount_pph = 0
            for tax_line in inv_doc.tax_line_ids:
                if tax_line.tax_id.name:
                    if 'ppn' in tax_line.tax_id.name.lower():
                        summary_amount_ppn += tax_line.amount
                    if 'pph' in tax_line.tax_id.name.lower():
                        summary_amount_pph += tax_line.amount

            inv_doc.summary_amount_ppn = summary_amount_ppn
            inv_doc.summary_amount_pph = summary_amount_pph
