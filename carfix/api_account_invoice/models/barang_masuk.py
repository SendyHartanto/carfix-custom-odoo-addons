from odoo import models, fields, api, _


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    api_ref = fields.Char('Api Reference')


class BarangMasuk(models.Model):
    _name = 'barang.masuk'
    _inherit = ['mail.thread']
    _order = 'api_date desc'

    name = fields.Char('Name', track_visibility='onchange')
    code_journal = fields.Char('Code Journal', track_visibility='onchange')
    supplier_id = fields.Integer('ID Supplier', track_visibility='onchange')
    move_date = fields.Date('Tanggal Transaksi', track_visibility='onchange')

    move_id = fields.Many2one('account.move', 'Journal Name')
    move_id_amount = fields.Monetary(related='move_id.amount', string='Journal Amount', store=True)
    move_id_amount_float = fields.Float(compute='_compute_move_amount', string='Journal Amount', store=True)

    @api.multi
    @api.depends('move_id.amount')
    def _compute_move_amount(self):
        for doc_id in self:
            doc_id.move_id_amount_float = doc_id.move_id_amount

    currency_id = fields.Many2one('res.currency', string="Currency", related='move_id.currency_id', store=True)
    company_id = fields.Many2one('res.company', string="Company")

    move_id_error_note = fields.Text('Move Error Note', track_visibility='onchange')
    api_error_note = fields.Text('Api Error Note', track_visibility='onchange')

    # coa_cogs_debit
    debit_account = fields.Char('Debit Account', track_visibility='onchange')
    credit_account = fields.Char('Credit Account', track_visibility='onchange')

    line_ids = fields.One2many('barang.masuk.line', 'barang_masuk_id', 'Line', track_visibility='onchange')
    bm_line_ids = fields.One2many('barang.masuk.line', 'bm_id', 'Line', track_visibility='onchange')

    payload = fields.Text('Payload', track_visibility='onchange')
    payload_log = fields.Text('Payload Log')
    api_date = fields.Datetime('Sent Date via API', default=fields.Datetime.now(), track_visibility='onchange')


    total_price = fields.Float(compute='_compute_amount', string='Price Amount', store=True, track_visibility='onchange')
    total_cogs = fields.Float(compute='_compute_amount', string='COGS Amount', store=True, track_visibility='onchange')
    selisih_amount_price = fields.Float(compute='_compute_amount', string='Selisih vs Price', store=True, track_visibility='onchange')
    selisih_amount_cogs = fields.Float(compute='_compute_amount', string='Selisih vs COGS', store=True, track_visibility='onchange')

    state = fields.Selection([
            ('draft', 'Draft'),
            ('validate', 'Validate'),
        ], string='Status', default='validate')

    @api.multi
    def recalc_credit_debit_from_price_po(self):
        """
            Perhitungan Journal bukan berdasarkan Price COGS
        """
        self = self.with_context(check_move_validity=False)
        for doc_id in self:
            move_amount = 0
            for bm_line_id in doc_id.bm_line_ids:
                quantity = bm_line_id.quantity
                price = bm_line_id.price
                move_amount += price * quantity
            move_doc = doc_id.move_id
            if move_doc:
                move_doc.state = 'draft'
                for move_line_id in move_doc.line_ids.with_context(check_move_validity=False):
                    move_line_id = move_line_id.with_context(by_pass_lock_date=True)
                    if move_line_id.debit:
                        move_line_id.debit = move_amount
                    if move_line_id.credit:
                        move_line_id.credit = move_amount
                move_doc.state = 'posted'
            else:
                doc_id.manual_journal()

    @api.multi
    def get_company_id_from_name(self):
        res_company_obj = self.env['res.company']
        for doc_id in self.sudo():
            bm_name = doc_id.name
            if bm_name:
                search_query = [('ref', '=', bm_name[:5])]
                company_doc = res_company_obj.search(search_query, limit=1)
                doc_id.write({
                    'company_id': company_doc.id,
                })

    @api.multi
    def manual_journal(self):
        for doc_id in self:
            if doc_id.company_id:
                doc_id = doc_id.with_context(company_id=doc_id.company_id.id, force_company=doc_id.company_id.id)
                if doc_id.name:
                    #handle Account
                    debit_account_id = doc_id.find_cogs_account('debit').id
                    credit_account_id = doc_id.find_cogs_account('credit').id
                    move_amount = 0
                    #Handle Amount Total untuk di journal
                    for doc_line_id in doc_id.bm_line_ids:
                        move_amount += doc_line_id.price * doc_line_id.quantity


                    aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
                    # if not self.destination_journal_id.default_credit_account_id:
                        # raise UserError(_('The journal %s does not have a Default Credit Account, please specify one.') % self.destination_journal_id.name)
                    move_doc = self.env['account.move'].create(doc_id._get_move_vals())
                    # dst_move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))

                    #Move Line untuk Payment Method
                    # supplier_id = self.validate_payload_supplier_id(supplier_id)
                    supplier_id = 1

                    debit_aml_vals = {
                        'name': 'Barang Masuk',
                        'debit': move_amount,
                        'credit': 0,
                        'move_id' : move_doc.id,
                        'account_id': debit_account_id,
                    }
                    #Move Line untuk Advance
                    credit_aml_vals = {
                        'name': 'Barang Masuk',
                        'debit': 0,
                        'credit': move_amount,
                        'move_id' : move_doc.id,
                        'partner_id': supplier_id,
                        'account_id': credit_account_id,
                    }

                    #Pembentukkan Account Move Line
                    #Di sortir terbalik
                    aml_obj.create(credit_aml_vals)
                    aml_obj.create(debit_aml_vals)
                    move_doc.post()

                    self._cr.commit()

                    doc_id.write({
                        'move_id': move_doc.id,
                    })



    @api.multi
    @api.depends('move_id_amount', 'line_ids', 'line_ids.quantity_price', 'line_ids.quantity_price_cogs')
    def _compute_amount(self):
        for bm_doc in self:
            total_price = 0
            total_cogs = 0
            for line in bm_doc.line_ids:
                total_cogs += line.price_cogs * line.quantity
                total_price += line.price * line.quantity
            bm_doc.total_price = total_price
            bm_doc.selisih_amount_price = total_price - bm_doc.move_id_amount_float
            bm_doc.selisih_amount_cogs = total_cogs - bm_doc.move_id_amount_float
            bm_doc.total_cogs = total_cogs

    _sql_constraints = [
        ('barang_masuk_name_uniq', 'Check(1=1)', 'Nomor Dokumen Barang Masuk harus unik!'),
    ]


class BarangMasukLine(models.Model):
    _name = 'barang.masuk.line'
    _inherit = ['mail.thread']

    barang_masuk_id = fields.Many2one('Barang Masuk v0', ondelete='cascade', track_visibility='onchange')

    bm_id = fields.Many2one('barang.masuk', 'Barang Masuk New', ondelete='cascade', track_visibility='onchange')

    name = fields.Char('Name', track_visibility='onchange')

    product_id = fields.Integer('Product ID', track_visibility='onchange')
    product_code = fields.Char('Product Code', track_visibility='onchange')
    product_name = fields.Char('Product Name', track_visibility='onchange')

    quantity = fields.Float('Quantity', track_visibility='onchange')
    price = fields.Float('Price by PO', track_visibility='onchange')
    price_cogs = fields.Float('Price COGS', track_visibility='onchange')

    quantity_price = fields.Float(compute='_compute_subtotal', string='Qty x Price', store=True)
    quantity_price_cogs = fields.Float(compute='_compute_subtotal', string='Qty x Price COGS', store=True)

    @api.multi
    @api.depends('price', 'price_cogs', 'quantity')
    def _compute_subtotal(self):
        for doc_id in self:
            doc_id.quantity_price = doc_id.quantity * doc_id.price
            doc_id.quantity_price_cogs = doc_id.quantity * doc_id.price_cogs
