from odoo import models, fields, api, _

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    lbm = fields.Char('LBM')
    product_code = fields.Char(related='product_id.default_code', string='Product Number')
    freight = fields.Float('Freight')
    price_diff = fields.Float('Price Diff from API')
