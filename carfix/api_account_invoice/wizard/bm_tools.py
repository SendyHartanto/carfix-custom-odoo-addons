import logging

from datetime import datetime, date
from odoo import fields, models, api, _



class BmToolsWizard(models.TransientModel):

    _name = 'bm.tools.wizard'

    name = fields.Char('Name')

    def manual_journal(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        total_count = len(doc_ids)
        index_count = 0
        _logger = logging.getLogger("Manual Journal")
        for doc_id in doc_ids:
            index_count += 1
            doc_id.manual_journal()
            message = "Processing %s of %s, %s" % (
                    index_count,
                    total_count,
                    round((index_count/total_count * 100), 2)
                )
            _logger.info(message)

    def company_name(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        total_count = len(doc_ids)
        index_count = 0
        _logger = logging.getLogger("Manual Journal")
        for doc_id in doc_ids:
            index_count += 1
            doc_id.get_company_id_from_name()
            message = "Processing %s of %s, %s" % (
                    index_count,
                    total_count,
                    round((index_count/total_count * 100), 2)
                )
            _logger.info(message)

    def company_name_2(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        total_count = len(doc_ids)
        index_count = 0
        _logger = logging.getLogger("Manual Journal")
        for doc_id in doc_ids:
            index_count += 1
            message = "Processing %s of %s, %s" % (
                    index_count,
                    total_count,
                    round((index_count/total_count * 100), 2)
                )
            _logger.info(message)

    def remap_bm_line(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        total_count = len(doc_ids)
        index_count = 0
        _logger = logging.getLogger("Remap Barang Masuk")
        for doc_id in doc_ids:
            index_count += 1
            message = "Processing %s of %s, %s" % (
                    index_count,
                    total_count,
                    round((index_count/total_count * 100), 2)
                )
            _logger.info(message)
            for line_id in doc_id.line_ids:
                # value = {
                #     'bm_id': line_id.barang_masuk_id.id,
                #     'name': line_id.name,
                #     'product_id': line_id.product_id,
                #     'product_code': line_id.product_code,
                #     'product_name': line_id.product_name,
                #     'quantity': line_id.quantity,
                #     'price': line_id.price,
                #     'price_cogs': line_id.price_cogs,
                # }
                # barang_masuk_obj.create(value)
                line_id.bm_id = line_id.barang_masuk_id.id
            self._cr.commit()

    def recalc_moves(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        account_move_obj = self.env['account.move']
        total_count = len(doc_ids)
        index_count = 0
        _logger = logging.getLogger("Relink Move")
        for doc_id in doc_ids:
            index_count += 1
            message = "Processing %s of %s, %s" % (
                    index_count,
                    total_count,
                    round((index_count/total_count * 100), 2)
                )
            _logger.info(message)
            doc_id.recalc_credit_debit_from_price_po()
            self._cr.commit()

    def relink_move(self):
        ids_to_change = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        doc_ids = self.env[active_model].browse(ids_to_change)
        account_move_obj = self.env['account.move']
        total_count = len(doc_ids)
        index_count = 0
        _logger = logging.getLogger("Relink Move")
        for doc_id in doc_ids:
            index_count += 1
            message = "Processing %s of %s, %s" % (
                    index_count,
                    total_count,
                    round((index_count/total_count * 100), 2)
                )
            _logger.info(message)
            if not doc_id.name:
                doc_id.write({
                    'move_id_error_note': 'Dokumen LBM tidak memiliki nomor yg valid',
                })
                continue
            search_query = [('ref', 'ilike', doc_id.name)]
            account_move_doc = account_move_obj.search(search_query)
            if len(account_move_doc) > 1:
                message = '''
                    Journal ditemukan lebih dari satu

                    %s
                ''' % (', '.join(account_move_doc.mapped('name')))
                doc_id.write({
                    'move_id_error_note': message,
                })
            elif account_move_doc:
                doc_id.write({
                    'move_id': account_move_doc.id,
                    'move_id_error_note': False,
                })
            else:
                doc_id.write({
                    'move_id_error_note': 'Tidak Ada Journal',
                })
            self._cr.commit()
