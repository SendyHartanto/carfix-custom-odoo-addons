from odoo import models, _
import operator
from odoo.exceptions import UserError


class ifrsx_indonesia_xlsx(models.AbstractModel):
    _name = 'report.ifrsx_indonesia.ifrsx_indonesia.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        style_hdr_blue = workbook.add_format({'border': 1, 'bg_color': 'cyan', 'num_format': '#,##0'})
        style_hdr_bold_blue = workbook.add_format({'border': 1, 'bg_color': 'cyan', 'num_format': '#,##0', 'bold': True})
        style_hdr_center_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan', 'num_format': '#,##0'})
        style_hdr_yellow = workbook.add_format({'border': 1, 'bg_color': 'yellow', 'num_format': '#,##0'})
        style_hdr_grey = workbook.add_format({'border': 1, 'bg_color': '#808080', 'num_format': '#,##0'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_ifrs_xlsx_datas()
            report_name = datas['report_name']
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(6, 1)
            
            sheet.merge_range('A1:D1', str(datas['report_name']) + ' - ' + datas['companies'], style_normal_bold)
        
            sheet.write('A3:A3', 'Companies', style_hdr_center_blue)
            sheet.write('B3:B3', 'Period', style_hdr_center_blue)
            sheet.write('C3:C3', 'Target Moves', style_hdr_center_blue)
            sheet.write('D3:D3', 'Mode', style_hdr_center_blue)
            
            sheet.write('A4:A4', datas['companies'], style_normal_center)
            sheet.write('B4:B4', datas['period_name'], style_normal_center)
            sheet.write('C4:C4', datas['target_move'], style_normal_center)
            sheet.write('D4:D4', datas['mode'], style_normal_center)

            row_count = 5
            col_count = 0
            columns = []
            headers = datas['period']
            columns.append(['Account Name', 40])
            for header in headers:
                i = [header, 20]
                columns.append(i)
            len_columns = len(columns)
            for column in columns:
                sheet.set_column(col_count, col_count, column[1]) 
                sheet.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
            
            row_count += 2    
            col_count = 0 
            for line in datas['csv']:
                if line.get('type') == 'abstract':
                    if line.get('style') == 'title':
                        sheet.merge_range(row_count, 0, row_count, len_columns - 1, line['name'], style_hdr_blue)
                    else:
                        sheet.merge_range(row_count, 0, row_count, len_columns - 1, line['name'], style_hdr_yellow)
                    row_count += 1
                    col_count = 0
                    continue
                
                if line.get('type') == 'detail':
                    sheet.write(row_count, col_count, '   ' + str(line['name']), style_normal_bold)
                    col_count += 1
                    for value in line['value']:
                        sheet.write(row_count, col_count, value, style_normal_bold)
                        col_count += 1
                    col_count = 0
                    row_count += 1
                    
                if line.get('type') == 'account':
                    sheet.write(row_count, col_count, '      ' + str(line['name']) , style_normal)
                    col_count += 1
                    for value in line['value']:
                        sheet.write(row_count, col_count, value, style_normal)
                        col_count += 1
                    col_count = 0
                    row_count += 1
                    
                if line.get('type') == 'total':
                    if line.get('style') == 'title':
                        sheet.write(row_count, col_count, line['name'] , style_hdr_blue)
                    else:
                        sheet.write(row_count, col_count, '   ' + str(line['name']) , style_hdr_grey)
                        
                    col_count += 1
                    for value in line['value']:
                        if line.get('style') == 'title':
                            sheet.write(row_count, col_count, value, style_hdr_blue)
                        else:
                            sheet.write(row_count, col_count, value, style_hdr_grey)
                            
                        col_count += 1
                    col_count = 0
                    row_count += 1
                 
                col_count = 0
