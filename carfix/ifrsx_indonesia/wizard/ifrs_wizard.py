from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import time


class IfrsWizard(models.TransientModel):
    _name = "ifrs.wizard"
    _description = 'IFRS Report Wizard'
                                        
    company_ids = fields.Many2many('res.company', string='Company',
                                  required=True,
                                  help='List of Company on the Report', default=lambda self:self.env.user.company_id)
    target_move = fields.Selection([('posted', 'All Posted Entries'),
                                     ('all', 'All Entries'),
                                     ], 'Target Moves', default='all', required=True,
                                   help=('Print All\
                                          Accounting\
                                          Entries or\
                                          just Posted\
                                          Accounting\
                                          Entries'))
    name = fields.Many2one('ifrs.ifrs', string='Report Name', required=True)
    period_id = fields.Many2one('account.period', string='Period')
    expand_mode = fields.Selection([
                        ('summary', 'Summary'),
                        ('details', 'Details'), ],
                        string='Mode', default='summary', required=True)
    
    @api.onchange('name')
    def name_change(self):
        return {'domain' : {'period_id' : [('id', 'in', self.name.account_period_ids.ids)]}}
    
    @api.multi
    def view_ifrs_xlsx_report(self):
        return self.env.ref('ifrsx_indonesia.ifrsx_indonesia_xlsx').report_action(self.ids, config=False)
    
    @api.multi
    def _get_ifrs_xlsx_datas(self):
        context = self._context or {}
        datas = {'active_ids': context.get('active_ids', [])}
        if len(self.company_ids.ids) > 1:
            datas['company_name'] = 'Consolidation' + ' - ' + self.company_ids[0].currency_id.name
        else:
            datas['company_name'] = self.company_ids[0].name + ' - ' + self.company_ids[0].currency_id.name
        datas['companies'] = ', '.join(map(str, [x.name for x in self['company_ids']]))
        datas['mode'] = self.expand_mode == 'summary' and 'Summary' or 'Details'
        datas['report_name'] = self.name.name
        datas['target_move'] = self.target_move == 'all' and 'All Entries' or 'All Posted Entries'
        datas['period'] = self.period_id.period_line.mapped('name')
        datas['period_name'] = self.period_id.name
        
        datas['csv'] = self.name.get_report_data(self.company_ids, self.period_id, self.target_move, self.expand_mode)
        return datas
        
