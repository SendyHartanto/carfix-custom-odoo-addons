{
    "name": "IFRSX INDONESIA",
    "version": "0.6",
    "author": "PT VISI",
    "category": "Accounting & Finance",
    "description": "Accounting & Finance IFRS",
    "website": "http://www.vauxoo.com",
    "license": "",
    "depends": [
        'account',
        'accounting_report_xlsx',
        'report_xlsx'
    ],
    "demo": [
    ],
    "data": [
        "views/ifrs_view.xml",
        "wizard/ifrs_wizard_view.xml",
        "views/account_period_view.xml",
        'security/ir.model.access.csv',
    ],
    "test": [],
    "js": [],
    "css": [],
    "qweb": [],
    "installable": True,
    "auto_install": False,
    "active": False,
    "application": True,
}
