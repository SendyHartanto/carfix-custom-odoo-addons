# -*- coding: utf-8 -*-

from odoo import api, fields, models

class AccountPeriod(models.Model):
    _name = "account.period"
    _description = "Report's Period"

    name = fields.Char('Name', required = True)
    active = fields.Boolean('Active', default=True)
    period_line = fields.One2many('account.period.line', 'period_id', string='Period of report', copy=True)

class AccountPeriodLine(models.Model):
    _name = "account.period.line"
    _description = "Report's Period Line"
    _order = 'date_from, id'

    name = fields.Char('Name', requied=True)
    date_from = fields.Date('Date From', required = True)
    date_to = fields.Date('Date To', required = True)
    period_id = fields.Many2one('account.period', 'Report Period')

