from odoo import models, api, fields, _

class IfrsIfrs(models.Model):
    _name = 'ifrs.ifrs'
    
    name = fields.Char('Name', required=True, help='Report name', translate=True)
    description = fields.Text('Description')
    ifrs_lines = fields.One2many('ifrs.lines', 'ifrs_id', 'IFRS lines', copy=True)
    state = fields.Selection(
        [('inactive', 'Inactive'),
         ('active', 'Active'), ],
        'State', required=True, default='inactive')
    account_period_ids = fields.Many2many('account.period', 'ifrs_period_rel', \
                                          'ifrs_id', 'account_period_id', string="Periods")
    
    @api.multi
    def get_report_data(self, company_ids, period_id, target_move, expand_mode):
        self.ensure_one()
        lines = self.ifrs_lines.get_detail_lines()
        
        if target_move == 'all':
            state = ('draft', 'posted')
        else:
            state = ('posted', 'done')
            
        query = """
            SELECT 
                il.id AS line, 
                aa.id AS account, 
                apl.id AS period, 
                SUM(CASE 
                    WHEN il.acc_val='balance' THEN
                        COALESCE(aml.debit,0.0)-COALESCE(aml.credit,0.0)
                    WHEN il.acc_val='debit' THEN
                        COALESCE(aml.debit,0.0)
                    WHEN il.acc_val='credit' THEN
                        COALESCE(aml.credit,0.0)
                    END) AS value
            FROM
                ifrs_lines il
                LEFT JOIN ifrs_account_rel iar ON iar.ifrs_lines_id=il.id
                LEFT JOIN account_account aa ON iar.account_id=aa.id AND aa.company_id IN %s
                FULL OUTER JOIN account_period_line apl ON apl.period_id = '%s' 
                LEFT JOIN (SELECT s_aml.* FROM account_move_line s_aml 
                            LEFT JOIN account_move s_am ON s_am.id=s_aml.move_id WHERE s_am.state IN %s) aml 
                                                 ON aa.id=aml.account_id AND
                                                    aml.date <= CASE WHEN il.acc_span in ('ending', 'mutation') 
                                                                THEN apl.date_to
                                                                ELSE apl.date_from END AND
                                                    aml.date >= CASE WHEN il.acc_span='mutation' 
                                                                THEN apl.date_from
                                                                ELSE '0001-01-01'::DATE END
            WHERE
                il.id IN %s
            GROUP BY 
                il.id, aa.id, apl.id
        """ % (company_ids.ids, period_id.id, state, list(set(lines.ids)))
        self.env.cr.execute(query.replace('[', '(').replace(']', ')'))
        out_list = self.env.cr.dictfetchall()
        compiled_data = {}
        for out_line in out_list:
            if not compiled_data.get(out_line['line']):
                compiled_data[out_line['line']] = {}
            if not compiled_data[out_line['line']].get(out_line['period']):
                compiled_data[out_line['line']][out_line['period']] = {'value': 0, 'account' : {}}
            if not compiled_data[out_line['line']][out_line['period']]['account'].get(out_line['account']):
                compiled_data[out_line['line']][out_line['period']]['account'][out_line['account']] = out_line['value']
            compiled_data[out_line['line']][out_line['period']]['value'] += out_line['value']
        
        result = []
        for line in self.ifrs_lines:
            line_dict = {'name' : line.name, 'type' : line.type, 'style':line.style, 'value' : []}
            if line.invisible:
                continue
            elif line.type == 'detail':
                for period_line in period_id.period_line:
                    if line.sign_change:
                        line_dict['value'].append(-compiled_data[line.id][period_line.id]['value'])
                    else:
                        line_dict['value'].append(compiled_data[line.id][period_line.id]['value'])
            elif line.type == 'total':
                for period_line in period_id.period_line:
                    if line.sign_change:
                        line_dict['value'].append(-line.get_total_value(compiled_data, period_line))
                    else:
                        line_dict['value'].append(line.get_total_value(compiled_data, period_line))
            result.append(line_dict)
            
            if line.type == 'detail' and expand_mode == 'details':
                for account_id in line.account_ids.filtered(lambda c: c.company_id in company_ids):
                    line_dict = {'name' : account_id.code + ' - ' + account_id.name, \
                                 'type' : 'account', 'value' : []}
                    for period_line in period_id.period_line:
                        if line.sign_change:
                            line_dict['value'].append(-compiled_data[line.id][period_line.id]['account'][account_id.id])
                        else:
                            line_dict['value'].append(compiled_data[line.id][period_line.id]['account'][account_id.id])
                    result.append(line_dict)
                    
        return result
        
class IfrsLines(models.Model):

    _name = 'ifrs.lines'
    _order = 'ifrs_id, sequence'
    
    sequence = fields.Integer('Sequence', required=True,
                       help=('Indicates the order of the line in the \
                             report. The sequence must be unique and \
                             unrepeatable'))
    name = fields.Char('Name', required=True, translate=True,
                    help=('Line name in the report. This name can be \
                          translatable, if there are multiple languages \
                          loaded it can be translated'))
    type = fields.Selection(
            [('abstract', 'Header'),
             ('detail', 'Detail'),
             ('total', 'Total')],
            string='Type',
            required=True, default='abstract',
            help='Line type of report:'
            " -Abstract(A),-Detail(D),-Total(T)")
    
    style = fields.Selection(
            [('normal', 'Normal'),
             ('title', 'Title'),
             ('subtitle', 'Subtitle')],
            string='Style', required=True, default='normal')
    
    ifrs_id = fields.Many2one('ifrs.ifrs', 'IFRS', required=True)
    account_ids = fields.Many2many('account.account', 'ifrs_account_rel',
                                'ifrs_lines_id', 'account_id',
                                string='Consolidated Accounts')
    total_ids = fields.Many2many('ifrs.lines', 'ifrs_lines_rel',
                                  'parent_id', 'child_id',
                                  string='First Operand', domain=[('type', '!=', 'abstract')])
    comment = fields.Text('Comments/Question',
                           help=('Comments or questions about this ifrs \
                                 line'))
    invisible = fields.Boolean('Invisible')
    sign_change = fields.Boolean('Change Sign to Amount')
    acc_span = fields.Selection(
            [('init', 'Initial Values'),
             ('mutation', 'Mutation in Periods'),
             ('ending', 'Ending Values')],
            string='Accounting Span', required=True, default='ending',
            help="Choose amount values in selected period")
    acc_val = fields.Selection(
            [
             ('balance', 'Balance'),
             ('debit', 'Debit'),
             ('credit', 'Credit')],
            string='Accounting Span', required=True, default='balance',
            help="Choose amount values in selected period")
    
    @api.multi
    def get_detail_lines(self):
        total_lines = self.env['ifrs.lines']
        for line in self:
            if line.type == 'detail':
                total_lines = total_lines and total_lines + line or line
            elif line.type == 'total':
                total_lines = total_lines and total_lines + line.total_ids.get_detail_lines() \
                                or line.total_ids.get_detail_lines()
        return total_lines
    
    @api.multi
    def get_total_value(self, compiled_data, period_line):
        self.ensure_one()
        result = 0
        for ifrs_line in self.total_ids:
            if ifrs_line.type == 'detail':
                result += compiled_data[ifrs_line.id][period_line.id]['value']
            else:
                result += ifrs_line.get_total_value(compiled_data, period_line)
        return result
        
    
    
    
    
