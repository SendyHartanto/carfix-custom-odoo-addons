{

    'name': 'Currency Setup',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Accounting Tweak',
    'website': 'http://visi.co.id/',
    'summary': 'Menyesuaikan perhitungan Tax dibelakang Koma',
    'description': '''

    Requirement :

    1. 2945.46 -> 2945.45

    ''',
    'depends': [
        'base', #untuk Currency USD
        'web',
    ],
    'data': [
        'views/currency.xml',
        'views/webclient_templates.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
