odoo.define("vs_cu.field_utils", function(require) {
    "use strict";

    var session = require("web.session");
    var weContext = require("web.field_utils");
    var oldFormat = weContext.format;

    function formatMonetaryNew(value, field, options) {
        if (value === false) {
            return "";
        }
        options = options || {};

        var currency = options.currency;
        if (!currency) {
            var currency_id = options.currency_id;
            if (!currency_id && options.data) {
                var currency_field =
                    options.currency_field ||
                    field.currency_field ||
                    "currency_id";
                currency_id =
                    options.data[currency_field] &&
                    options.data[currency_field].res_id;
            }
            currency = session.get_currency(currency_id);
        }

        var digits = (currency && currency.digits) || options.digits;
        if (options.field_digits === true) {
            digits = field.digits || digits;
        }
        var formatted_value = oldFormat.float(value, field, {
            digits: [63, 2]
        });

        if (!currency || options.noSymbol) {
            return formatted_value;
        }
        if (currency.position === "after") {
            return (formatted_value += "&nbsp;" + currency.symbol);
        } else {
            return currency.symbol + "&nbsp;" + formatted_value;
        }
    }

    // weContext.parse= weContext.parse; //bisa perlu bisa gak
    weContext.format = _.extend(oldFormat, {
        monetary: formatMonetaryNew
    });
});
