{
    'name': 'Collector AR',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http://visi.co.id/',
    'summary': 'Custom-built Odoo',
    'description': """

    Fitur :
    Schedule Collection AR

    """,
    'depends': [
        'account',
        'account_invoicing',
        'account_cancel',
        'payment',
        'hr',
    ],
    'data': [
        'views/collector_ar.xml',
        'views/res_partner.xml',
        'views/ir_sequence.xml',
        'views/hr_employee.xml',
        'views/payment_action_button.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
