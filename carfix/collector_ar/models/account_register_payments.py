from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class AccountRegisterPayments(models.TransientModel):
    _inherit = "account.register.payments"

    @api.model
    def default_get(self, fields):
        rec = super(AccountRegisterPayments, self).default_get(fields)
        button_context = self._context.get('button_context', False)

        if button_context:
            collector_ar_id = button_context.get('collector_ar_id', False)
            default_communication = button_context.get('default_communication', 'Collector AR')

            rec.update({
                'communication': default_communication,
            })
        return rec
