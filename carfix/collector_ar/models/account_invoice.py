from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    running_collector_ar_id = fields.Many2one('collector.ar', 'Collector AR')
