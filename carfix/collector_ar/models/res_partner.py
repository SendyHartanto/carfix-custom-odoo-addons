from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class ResPartner(models.Model):
    _inherit = 'res.partner'

    dth_senin = fields.Boolean(string='Monday', help="Penagihan hari Senin")
    dth_selasa = fields.Boolean(string='Tuesday', help="Penagihan hari Selasa")
    dth_rabu = fields.Boolean(string='Wednesday', help="Penagihan hari Rabu")
    dth_kamis = fields.Boolean(string='Thursday', help="Penagihan hari Kamis")
    dth_jumat = fields.Boolean(string='Friday', help="Penagihan hari Jumat")
    dth_sabtu = fields.Boolean(string='Saturday', help="Penagihan hari Sabtu")
    dth_ahad = fields.Boolean(string='Sunday', help="Penagihan hari Ahad")

    dth_code = fields.Char(compute='_generate_dth_code', string='Code', help="Slot Jadwal DTH dirangkum menjadi Code terkait optimisasi penggunaan Database", store=True)

    @api.multi
    @api.depends('dth_senin',
            'dth_selasa',
            'dth_rabu',
            'dth_kamis',
            'dth_jumat',
            'dth_sabtu',
            'dth_ahad',
        )
    def _generate_dth_code(self):
        for partner_doc in self:
            code = ""
            if partner_doc.dth_senin:
                code+= "1"
            if partner_doc.dth_selasa:
                code+= "2"
            if partner_doc.dth_rabu:
                code+= "3"
            if partner_doc.dth_kamis:
                code+= "4"
            if partner_doc.dth_jumat:
                code+= "5"
            if partner_doc.dth_sabtu:
                code+= "6"
            if partner_doc.dth_ahad:
                code+= "0"
            partner_doc.dth_code = code
