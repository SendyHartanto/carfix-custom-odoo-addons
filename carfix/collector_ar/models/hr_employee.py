from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    customer_handle_ids = fields.Many2many(
        comodel_name='res.partner',
        relation='customer_handle_rel',
        column1='employee_id',
        column2='partner_id',
        string='Customer Handle',
        copy=False,
        domain="[('customer', '=', True)]",
        )
    