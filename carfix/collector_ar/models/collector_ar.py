from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime

class CollectorAr(models.Model):
    _name = 'collector.ar'
    _inherit = ['mail.thread']

    READONLY_STATES = {
        'draft': [('readonly', False)],
    }

    name = fields.Char(string='Name', default='/', copy=False, readonly=True, states=READONLY_STATES)
    date_planned = fields.Date(string='Date Planned', readonly=True, states=READONLY_STATES)
    employee_id = fields.Many2one('hr.employee', string='Employee', readonly=True, states=READONLY_STATES)

    state = fields.Selection([
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('reconciled', 'Reconciled'),
            ('close', 'Closed'),
        ], string='Status',
        required=True, default='draft',
        help="""
            Draft - ketika Staf DTH membuat dokumen pertama kali
            Open - ketika Staf DTH sudah konfirmasi dan Tim Collector sudah progress dengan Collecting AR
            Reconciled - ketika Staf DTH sudah serah terima dengan Kasir. Kasir sudah selesai konfirmasi Payment
            Close - ketika Staf DTH sudah menganggap Collection AR selesai
        """,
        track_visibility='onchange', copy=False)

    partner_ids = fields.Many2many(
        comodel_name='res.partner',
        relation='collector_partner_rel',
        column1='collector_id',
        column2='partner_id',
        string='Partner',
        copy=False,
        readonly=True,
        states=READONLY_STATES,
        )

    invoice_ids = fields.Many2many(
        comodel_name='account.invoice',
        relation='collector_invoice_rel',
        column1='collector_id',
        column2='invoice_id',
        string='Invoice',
        copy=False,
        readonly=True,
        states=READONLY_STATES,
        )

    def update_partner(self):
        self.ensure_one()
        if not self.employee_id:
            raise UserError("Kolom Employee Harus terisi")
        if not self.date_planned:
            raise UserError("Kolom Date Planned Harus terisi")
        date_planned = self.date_planned
        date_planned_obj = datetime.strptime(date_planned, "%Y-%m-%d")
        hari_string = date_planned_obj.strftime("%w")
        cust_ids = self.employee_id.customer_handle_ids.filtered(lambda x: hari_string in x.dth_code)
        self.partner_ids = cust_ids

    @api.onchange('employee_id', 'date_planned')
    def onchange_partner_ids(self):
        if self.employee_id and self.date_planned:
            self.update_partner()

    @api.one
    def action_open(self):
        self.name = self.env['ir.sequence'].next_by_code('collector.ar')
        self.state = "open"
        # Kasih Flag Invoice agar tidak bisa di masukkan ke Another DTH
        for inv_doc in self.invoice_ids:
            inv_doc.running_collector_ar_id = self.id
        return

    @api.one
    def action_reconciled(self):
        self.state = "reconciled"
        return

    @api.one
    def action_close(self):
        self.state = "close"
        # Kasih Flag False agar bisa di masukkan ke Another DTH
        for inv_doc in self.invoice_ids:
            inv_doc.running_collector_ar_id = False
        return

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        rec = super(CollectorAr, self).copy(default)
        rec.update_partner()
        return rec

    def collector_get_invoices(self):
        domain = [('id','in', self.invoice_ids.ids)]
        return {
            'name': 'Setoran Kasir ' + self.name,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain' : domain,
            'context': {
                'button_context': {
                    'collector_ar_id': self.id,
                    'default_communication': self.name,
                },
            },
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window',
        }

    def collector_get_journal(self):
        domain = [('ref','ilike', self.name)]
        if self.state == 'draft':
            raise UserError("Status Collector harus Open")
        return {
            'name': 'Related Journal Entry',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain' : domain,
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
        }
    