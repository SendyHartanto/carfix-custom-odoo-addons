from odoo import models, fields, api, _


class Company(models.Model):
    _inherit = 'res.company'
    
    image_header = fields.Binary("Image Header", attachment=True,
        help="This field holds the image used as avatar for this contact, limited to 1024x1024px",)
    
class Paymentcarfix(models.Model):
    _inherit = 'payment.carfix'
      
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
        readonly=True, states={'draft': [('readonly', False)]},
        default=lambda self: self.env.user.company_id.id)
    street = fields.Char('Address', related='company_id.parent_id.street')  
    city = fields.Char('City', related='company_id.parent_id.city')  
    zip = fields.Char('ZIP', related='company_id.parent_id.zip')  
    phone = fields.Char('ZIP', related='company_id.parent_id.phone')  
    vat = fields.Char('ZIP', related='company_id.parent_id.vat')  
