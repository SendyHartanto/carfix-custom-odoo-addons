# -*- coding: utf-8 -*-
# Copyright 2012-2016 Akretion (http://www.akretion.com/)
# @author: Benoît GUILLOT <benoit.guillot@akretion.com>
# @author: Chafique DELLI <chafique.delli@akretion.com>
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# Copyright 2018 Tecnativa - Pedro M. Baeza
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Pdf Print Out',
    'version': '11.0.1.0.0',
    'category': 'Custom',
    'license': 'AGPL-3',
    'summary': 'Manage pdf printout',
    'author': 'PT. VISI',
    'website': 'http://visi.co.id',
    'depends': [
        'payment_carfix',
    ],
    'data': [
        'report/reports.xml',
        'report/payment_templates.xml',
        'views/company_view.xml'
    ],
    'installable': True,
}
