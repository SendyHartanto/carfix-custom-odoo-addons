{
    'name': 'VS Journal Approval',
    'version': '11.1.0',
    'author': 'PT VISI',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http: //www.visi.co.id/',
    'summary': 'Custom-built Odoo',
    'description': '''
        References :
        - https://docs.oracle.com/cd/A60725_05/html/comnls/us/gl/jeappr03.htm
        - https://www.intellitecsolutions.com/blog/view/submit-a-journal-entry-for-approval-in-dynamics-gp/
        - https://docs.microsoft.com/en-us/dynamicsax-2012/appuser-itpro/set-up-financial-journal-approvals

        
    ''',
    'depends': [
        'account', # python
        # 'sale', # python
    ],
    'data': [
        # 'views/_menu_item.xml',
        'views/account_move.xml',
        'views/account_journal.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}