from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountMove(models.Model):
    _inherit = 'account.move'

    state = fields.Selection(selection=[
            ('draft', 'Unposted'),
            ('waiting', 'Waiting Approval'),
            ('reject', 'Rejected'),
            ('posted', 'Posted'),
        ])

    @api.multi
    def post(self):
        for doc in self:
            if doc.journal_id.journal_approval and doc.state == 'draft':
                doc._post_validate()
                doc.state = 'waiting'
                return True
            elif doc.journal_id.journal_approval and doc.state == 'waiting':
                return super(AccountMove, self).post()
            else:
                return super(AccountMove, self).post()

    @api.multi
    def waiting_to_post(self):
        if self._context.get('waiting_to_post_button', False):
            return super(AccountMove, self).post()
        else:
            return True
