from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountJournal(models.Model):
    _inherit = 'account.journal'

    journal_approval = fields.Boolean(string='Journal Approval', help="""
        Ketika dicentang, maka setiap Journal Entries yang akan di post, akan melalui 
        Approval Workflow terlebih dahulu.
    """)
