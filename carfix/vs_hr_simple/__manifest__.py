{
    'name': 'VS HR Simplified',
    'version': '11.1.0',
    'author': 'PT VISI',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http: //www.visi.co.id/',
    'summary': 'Custom-built Odoo',
    'description': '''
    ''',
    'depends': [
        'hr', # python
    ],
    'data': [
        'views/menu_item.xml',
        'views/hr_employee.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}