# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import date, datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF

class AssetWizard(models.TransientModel):
    _name = 'asset.wizard'
    _description = 'wizard to add asset value based on created entry'
    
    @api.depends('asset_id')
    def _get_account_id(self):
        self.account_id = self.asset_id.category_id.suspend_account_id.id
        
    asset_id = fields.Many2one('account.asset.asset', string="Asset Ref")
    asset_state = fields.Selection(string="Asset State", related='asset_id.state')
    
    account_id = fields.Many2one('account.account', string="Account", compute='_get_account_id')
    move_line_ids = fields.Many2many('account.move.line', string="Costs")
    
    date = fields.Date('Sell Date')
    partner_id = fields.Many2one('res.partner', 'Customer')
    
    @api.one
    def add_move_line(self):
        self.ensure_one()
        if any(credit or 0 > 0 for credit in self.move_line_ids.mapped('credit')):
            raise UserError("Unable to add negative amount")
        self.asset_id.move_line_ids |= self.move_line_ids
        self.asset_id.value = sum(self.asset_id.move_line_ids.mapped('debit')) - \
                                sum(self.asset_id.move_line_ids.mapped('credit'))
        if self.asset_state == 'open':
            self.asset_id.reclass(self.date)
        else:
            partner_ids = self.move_line_ids.mapped('partner_id')
            self.asset_id.partner_id = partner_ids and partner_ids[0].id
        return {'type': 'ir.actions.act_window_close'}
    
    @api.one
    def create_invoice(self):
        asset_lines = self.env['account.asset.depreciation.line']
        
        for asset in self.asset_id:
            unposted_depreciation_line_ids = asset.depreciation_line_ids.filtered(lambda x: not x.move_check)
            if unposted_depreciation_line_ids:
                # Remove all unposted depr. lines
                commands = [(2, line_id.id, False) for line_id in unposted_depreciation_line_ids]

                # Create a new depr. line with the residual amount and post it
                sequence = len(asset.depreciation_line_ids) - len(unposted_depreciation_line_ids) + 1
                vals = {
                    'amount': asset.value_residual,
                    'asset_id': asset.id,
                    'sequence': sequence,
                    'name': (asset.code or '') + '/' + str(sequence),
                    'remaining_value': 0,
                    'depreciated_value': asset.value - asset.salvage_value,  # the asset is completely depreciated
                    'depreciation_date': self.date,
                }
                commands.append((0, False, vals))
                asset.write({'depreciation_line_ids': commands,
                             'method_end': self.date,
                             'method_number': sequence})
                asset_lines |= asset.depreciation_line_ids[-1]
                
        invoice = self.env['account.invoice'].create({
            'partner_id' : self.partner_id.id,
            'date_invoice' : self.date,
            'type': 'out_invoice',
            'account_id': self.partner_id.property_account_receivable_id.id,
            'currency_id': self.env.user.company_id.currency_id.id,
            'origin' : 'Disposal of ' + self.asset_id.name
        })
        for asset_line in asset_lines:
            invoice.invoice_line_ids.create({
                'name' : asset_line.asset_id.name,
                'origin': asset_line.asset_id.name,
                'account_id': asset_line.asset_id.category_id.account_id.id,
                'quantity': 1.0,
                'discount': 0.0,
                'price_unit': 0.0,
                'asset_id' : asset_line.asset_id.id,
                'invoice_id' : invoice.id
            })
        self.asset_id.invoice_disposal_id = invoice.id
        return {
            'name': _('Asset Selling Invoice'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'res_id' : invoice.id,
            'target': 'new',
        }
        
    @api.one
    def reclass(self):
        self.asset_id.validate()
        self.asset_id.date = self.date
        self.asset_id.reclass(self.date)
        return {'type' : 'ir.actions.act_window_close'}
        
 
    
