# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _

class PaymentRequestWizard(models.TransientModel):
    _name = 'payment.request.wizard'
    _description = 'Payment Request Wizard'
    
    date = fields.Date('Payment Date Plan')
    
    @api.multi
    def assign_date(self):
        invoices = self.env['account.invoice'].browse(self._context.get('active_ids')).filtered(lambda i: i.request_id and not i.date_payment_plan)
        return invoices.write({'date_payment_plan':self.date})
