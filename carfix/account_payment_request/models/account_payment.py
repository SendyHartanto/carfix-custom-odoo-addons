# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class AccountPayment(models.Model):
    _inherit = 'account.payment'
    
    @api.one
    @api.depends('move_line_ids', 'move_line_ids.reconciled')
    def _reconciled(self):
        column = self.payment_type == 'outbound' and 'debit' or 'credit'
        line = self.move_line_ids.filtered(lambda ml: ml.account_id == self.destination_account_id and ml[column] > 0)
        self.reconciled = (not line and True) or (line and all(l.reconciled for l in line))
        self.amount_residual = abs(sum(line.mapped('amount_residual')))
        
    @api.model
    def _get_advance_type(self):
        if self._context.get('default_partner_type') == 'employee':
            return [('advance', 'Advance')]
        return [('normal', 'Invoice'),
                ('advance', 'Advance')]
        
    method_type = fields.Selection([('liquid', 'Liquidity'),
                                    ('postdated', 'Giro/Check')],
                                   string='Method Type', required=True, default='liquid')
    advance_type = fields.Selection(_get_advance_type,
                                   string='Payment For', required=True, default='normal')
    partner_type = fields.Selection([('customer', 'Customer'),
                                     ('supplier', 'Vendor'),
                                     ('employee', 'Employee')])
    tax_id = fields.Many2one('account.tax', 'Applied Tax')
    amount_residual = fields.Monetary(string='Unallocated Amount',
        store=True, readonly=True, compute='_reconciled')
    reconciled = fields.Boolean('Allocated', compute="_reconciled", store=True)
    bank_account = fields.Char('Bank Account')    
    bank_id = fields.Many2one('res.bank', 'Bank')
    user_id = fields.Many2one('res.users', 'Salesperson', default=lambda self: self.env.user)
    force_dest_account_id = fields.Many2one('account.account', 'Force Counterpart Account', domain=[('reconcile', '=', True)])
    
    
    @api.onchange('partner_type', 'journal_id', 'user_id')
    def _onchange_partner_type(self):
        res = super(AccountPayment, self)._onchange_partner_type()
        if not res:
            res = {}
        if not res.get('domain'):
            res['domain'] = {}
        if self.partner_type == 'employee':
            res['domain']['partner_id'] = [('employee', '=', True)]
        return res
    
    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:

            if rec.state != 'draft':
                raise UserError(_("Only a draft payment can be posted."))

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if rec.partner_type == 'customer':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if rec.partner_type == 'supplier':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
                if rec.partner_type == 'employee':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.employee.return'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.employee.payment'
            rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(sequence_code)
            if not rec.name and rec.payment_type != 'transfer':
                raise UserError(_("You have to define a sequence for %s in your company.") % (sequence_code,))

            # Create the journal entry
            amount = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            move = rec._create_payment_entry(amount)

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()

            rec.write({'state': 'posted', 'move_name': move.name})
    
    @api.one
    @api.depends('invoice_ids', 'payment_type', 'advance_type', 'partner_type', 'partner_id', 'force_dest_account_id', 'company_id')
    def _compute_destination_account_id(self):
        if self.force_dest_account_id:
            self.destination_account_id = self.force_dest_account_id
        elif self.advance_type == 'advance' and self.partner_id:
            if self.partner_type == 'customer':
                self.destination_account_id = self.partner_id.property_acc_deposit_cust_id.id
            else:
                self.destination_account_id = self.partner_id.property_acc_deposit_vend_id.id
        elif self.payment_type == 'transfer':
            if self.company_id and not self.company_id.transfer_account_id.id:
                raise UserError(_('Transfer account not defined on the company.'))
            self.destination_account_id = self.company_id.transfer_account_id.id
        else:
            super(AccountPayment, self)._compute_destination_account_id()
    
