# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as dateformat

    
class AccountPaymentRequest(models.Model):
    _name = 'account.payment.request'
    _description = 'Payment Request'
    _inherit = ['mail.thread']
    _order = "date_request desc, id desc"
    
    READONLY_STATES = {
        'confirm': [('readonly', True)],
        'request': [('readonly', True)],
        'paid': [('readonly', True)],
        'cancel': [('readonly', True)],
    }
    
    type = fields.Selection([
        ('saleable', 'Saleable'),
        ('other', 'Other'),
        ], string='Type', index=True, default='saleable')
    name = fields.Char('Reference', index=True, copy=False, readonly=True, default='New')
    date_request = fields.Date('Date', required=True, states=READONLY_STATES)
    user_id = fields.Many2one('res.users', 'Requested By', states=READONLY_STATES, default=lambda self: self.env.user)
    approve_user_id = fields.Many2one('res.users', 'Approved By', readonly=True)
    company_id = fields.Many2one('res.company', 'Company', states=READONLY_STATES, required=True, default=lambda self: self.env.user.company_id)
    
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirm', 'Waiting Approval'),
            ('request', 'Requested'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False)
    
    invoice_ids = fields.One2many('account.invoice', 'request_id', 'Invoices', states=READONLY_STATES, 
                                  domain=[state == 'draft' and (
                                         ('request_id', '=', False),
                                         ('tax_validity', '=', 1),
                                         ('state', '=', 'open'),
                                         ('type', '=', 'in_invoice'),
                                         '|', ('invoice_line_ids.product_id', '=', False),
                                         ('invoice_line_ids.product_id.sale_ok', '=', (type == 'saleable' and True or False))) or (
                                         ('type', '=', 'in_invoice'))])
    
    invoice_draft_ids = fields.Many2many('account.invoice', 'account_invoice_draft_request_rel', 'request_id', 'invoice_id', 'Budget to Prepare', states=READONLY_STATES,
                                         domain=[state == 'draft' and (
                                                 ('request_id', '=', False),
                                                 ('tax_validity', '=', False),
                                                 ('state', '!=', 'paid'),
                                                 ('type', '=', 'in_invoice'),
                                                 '|', ('invoice_line_ids.product_id', '=', False),
                                                 ('invoice_line_ids.product_id.sale_ok', '=', (type == 'saleable' and True or False))) or (
                                                 ('type', '=', 'in_invoice'))])
    
    voucher_ids = fields.One2many('account.voucher', 'request_id', 'Vouchers', states=READONLY_STATES)
    voucher_draft_ids = fields.Many2many('account.voucher', 'account_voucher_draft_request_rel', 'request_id', 'voucher_id', 'Budget to Prepare', states=READONLY_STATES)
    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            if vals.get('type', 'saleable') == 'saleable':
                vals['name'] = self.env['ir.sequence'].next_by_code('payment.request.saleable') or '/'
            else:
                vals['name'] = self.env['ir.sequence'].next_by_code('payment.request.other') or '/'
        return super(AccountPaymentRequest, self).create(vals)
            
    @api.onchange('type', 'invoice_ids', 'invoice_draft_ids', 'state')
    def type_change(self):
        res = {'domain' : {}}
        if self.type == 'saleable' :
            res['domain']['invoice_ids'] = [('request_id', '=', False),
                                            ('tax_validity', '=', 1),
                                            ('state', '=', 'open'),
                                            ('type', '=', 'in_invoice'),
                                            '|', ('invoice_line_ids.product_id', '=', False),
                                            ('invoice_line_ids.product_id.sale_ok', '=', True)]
            res['domain']['invoice_draft_ids'] = [('request_id', '=', False),
                                                  ('tax_validity', '=', False),
                                                  ('state', '!=', 'paid'),
                                                  ('type', '=', 'in_invoice'),
                                                  '|', ('invoice_line_ids.product_id', '=', False),
                                                  ('invoice_line_ids.product_id.sale_ok', '=', True)]
        else:
            res['domain']['invoice_ids'] = [('request_id', '=', False),
                                            ('tax_validity', '=', 1),
                                            ('state', '=', 'open'),
                                            ('type', '=', 'in_invoice'),
                                            '|', ('invoice_line_ids.product_id', '=', False),
                                            ('invoice_line_ids.product_id.sale_ok', '=', False)]
            res['domain']['invoice_draft_ids'] = [('request_id', '=', False),
                                                  ('tax_validity', '=', False),
                                                  ('state', '!=', 'paid'),
                                                  ('type', '=', 'in_invoice'),
                                                  '|', ('invoice_line_ids.product_id', '=', False),
                                                  ('invoice_line_ids.product_id.sale_ok', '=', False)]
        return res
            
    @api.multi
    def test_paid(self):
        for req in self:
            all_invoice_paid = all(state == 'paid' for state in req.invoice_ids.mapped('state'))
            all_voucher_paid = all(state == 'posted' for state in req.voucher_ids.mapped('state'))
            if req.state == 'paid' and (not all_invoice_paid or not all_voucher_paid):
                req.state = 'request'
            elif req.state == 'request' and all_invoice_paid and all_voucher_paid:
                req.state = 'paid'
        
    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
    
    @api.multi
    def action_approve(self):
        self.state = 'request'
        self.approve_user_id = self.env.user.id
        
    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        
    @api.multi
    def action_draft(self):
        self.state = 'draft'
        
    @api.multi
    def unlink(self):
        for request in self:
            if not request.state == 'cancel':
                raise UserError(_('In order to delete a payment request, you must cancel it first.'))
        return super(AccountPaymentRequest, self).unlink()
    
    @api.multi
    def action_print(self):

        def convert_date(date):
            return date and datetime.strptime(date, dateformat).strftime('%d %B %Y') or ''
        
        def convert_date2(date):
            return date and datetime.strptime(date, dateformat).strftime('%d/%m/%Y') or ''
        
        state = dict([
            ('draft', 'Draft'),
            ('confirm', 'Waiting Approval'),
            ('request', 'Requested'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ])
        
        self.ensure_one()
        data = {
            'company' : self.company_id.name,
            'requestor' : self.user_id.name,
            'approval' : self.approve_user_id.name or '',
            'reference' : self.name,
            'state' : state[self.state],
            'date' : convert_date(self.date_request),
            'line_request' : {},
            'line_budget' : {}
        }
        for bill in self.invoice_ids:
            partner = bill.partner_id.parent_id.name or bill.partner_id.name
            if not data['line_request'].get(partner):
                data['line_request'][partner] = {}
            if not data['line_request'][partner].get(convert_date2(bill.date_invoice)):
                data['line_request'][partner][convert_date2(bill.date_invoice)] = {bill.company_id.name: []}
            amount = float(bill.amount_total - sum(bill.share_ids.mapped('value')))
            residual = amount / bill.amount_total * bill.residual
            data['line_request'][partner][convert_date2(bill.date_invoice)][bill.company_id.name].append(
                [bill.reference or '', amount, residual,
                 convert_date2(bill.date_due), bill.number])
            
            for share in bill.share_ids:
                if not data['line_request'][partner][convert_date2(bill.date_invoice)].get(share.name.name):
                    data['line_request'][partner][convert_date2(bill.date_invoice)][share.name.name] = []
                residual = float(share.value) / bill.amount_total * bill.residual
                data['line_request'][partner][convert_date2(bill.date_invoice)][share.name.name].append(
                    [share.reference or '', share.value, residual,
                     convert_date2(bill.date_due), bill.number])
        # anif
        for bill in self.voucher_ids:
            partner = bill.partner_id.parent_id.name or bill.partner_id.name
            if not data['line_request'].get(partner):
                data['line_request'][partner] = {}
            if not data['line_request'][partner].get(convert_date2(bill.date)):
                data['line_request'][partner][convert_date2(bill.date)] = {bill.company_id.name: []}
            amount = float(bill.amount)
            residual = 0.0 if bill.state == 'posted' else amount
            data['line_request'][partner][convert_date2(bill.date)][bill.company_id.name].append(
                [bill.reference or '', amount, residual,
                 convert_date2(bill.date), bill.number])
        # end anif
              
        for bill in self.invoice_draft_ids:
            partner = bill.partner_id.parent_id.name or bill.partner_id.name
            if not data['line_budget'].get(partner):
                data['line_budget'][partner] = {}
            if not data['line_budget'][partner].get(convert_date2(bill.date_invoice)):
                data['line_budget'][partner][convert_date2(bill.date_invoice)] = {bill.company_id.name: []}
            amount = float(bill.amount_total - sum(bill.share_ids.mapped('value')))
            residual = amount / bill.amount_total * bill.residual
            data['line_budget'][partner][convert_date2(bill.date_invoice)][bill.company_id.name].append(
                [bill.reference or '', amount, residual,
                 convert_date2(bill.date_due), bill.number])
            
            for share in bill.share_ids:
                if not data['line_budget'][partner][convert_date2(bill.date_invoice)].get(share.name.name):
                    data['line_budget'][partner][convert_date2(bill.date_invoice)][share.name.name] = []
                residual = float(share.value) / bill.amount_total * bill.residual
                data['line_budget'][partner][convert_date2(bill.date_invoice)][share.name.name].append(
                    [share.reference or '', share.value, residual,
                     convert_date2(bill.date_due), bill.number])
        # anif
        for bill in self.voucher_draft_ids:
            partner = bill.partner_id.parent_id.name or bill.partner_id.name
            if not data['line_budget'].get(partner):
                data['line_budget'][partner] = {}
            if not data['line_budget'][partner].get(convert_date2(bill.date)):
                data['line_budget'][partner][convert_date2(bill.date)] = {bill.company_id.name: []}
            amount = float(bill.amount)
            residual = 0.0 if bill.state == 'posted' else amount
            data['line_budget'][partner][convert_date2(bill.date)][bill.company_id.name].append(
                [bill.reference or '', amount, residual,
                 convert_date2(bill.date), bill.number])
        # end anif
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'report.account_payment_request.payment_request_xlsx',
            'datas': data
        }

