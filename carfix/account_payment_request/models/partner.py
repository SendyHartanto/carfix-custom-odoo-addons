# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.tools import email_split
from odoo.exceptions import UserError


def extract_email(email):
    """ extract the email address from a user-friendly email address """
    addresses = email_split(email)
    return addresses[0] if addresses else ''


class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    employee = fields.Boolean('Is an Employee')
    property_acc_deposit_cust_id = fields.Many2one('account.account', company_dependent=True,
        string="Account Deposit Customer",
        domain="[('reconcile', '=', True), ('deprecated', '=', False)]",
        help="This account will be used instead of the default one as the customer deposit account for the current partner",
        required=True)
    
    property_acc_deposit_vend_id = fields.Many2one('account.account', company_dependent=True,
        string="Account Deposit Vendor",
        domain="[('reconcile', '=', True), ('deprecated', '=', False)]",
        help="This account will be used instead of the default one as the supplier deposit account for the current partner",
        required=True)
    
    @api.one
    def create_related_user(self):
        if not self.email:
            raise UserError(_('Please fill the email first'))
        if not extract_email(self.email):
            raise UserError(_('Invalid for email format'))
        res_users = self.env['res.users']
        create_context = dict(self._context or {}, noshortcut=True, no_reset_password=True)  # to prevent shortcut creation
        values = {
            'company_id': self.company_id.id,
            'company_ids': [(6, 0, [self.company_id.id])],
            'email': extract_email(self.email),
            'login': extract_email(self.email),
            'partner_id': self.id,
            'groups_id': [(6, 0, [])],
        }
        return res_users.with_context(create_context).create(values)
    
