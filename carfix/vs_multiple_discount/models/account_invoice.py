from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError



class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    discount_1 = fields.Float('Disc (%) 1')
    discount_nominal_1 = fields.Float('Disc (Rp) 1')

    discount_2 = fields.Float('Disc (%) 2')
    discount_nominal_2 = fields.Float('Disc (Rp) 2')

    discount_3 = fields.Float('Disc (%) 3')
    discount_nominal_3 = fields.Float('Disc (Rp) 3')

    @api.onchange('price_unit', 'quantity', 'discount_1', 'discount_nominal_1',
    'discount_2', 'discount_nominal_2', 'discount_3', 'discount_nominal_3')
    def onchange_compute_diskon(self):
        # old_price_reduce = super(AccountInvoiceLine, self)._compute_price_reduce()[0] # Hasil @api.one

        #Di Custom Berdasarkan Bisnis Proses Customer
        diskon_persentase = self.discount_1 + self.discount_2
        price_reduce = self.price_unit * ((diskon_persentase or 0.0) / 100.0)

        self.discount_nominal = price_reduce + self.discount_nominal_3


    def line_compute_diskon(self):
        # old_price_reduce = super(AccountInvoiceLine, self)._compute_price_reduce()[0] # Hasil @api.one

        #Di Custom Berdasarkan Bisnis Proses Customer
        diskon_persentase = self.discount_1 + self.discount_2
        price_reduce = self.price_unit * ((diskon_persentase or 0.0) / 100.0)
        # import ipdb; ipdb.set_trace()
        self.discount_nominal = price_reduce + self.discount_nominal_3

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    discount_total = fields.Float('Discount (%) Total')
    discount_nominal_total = fields.Float('Disc (Rp) Total')
    amount_before_disc = fields.Float('Amount Before Disc')


    # @api.onchange('discount_total', 'discount_nominal_total')
    # def _compute_diskon_total(self):
    #     # old_price_reduce = super(AccountInvoiceLine, self)._compute_price_reduce()[0] # Hasil @api.one

    #     #Di Custom Berdasarkan Bisnis Proses Customer
    #     diskon_persentase = self.discount_1 + self.discount_2
    #     price_reduce = self.price_unit * ((diskon_persentase or 0.0) / 100.0)

    #     self.discount_nominal = price_reduce + self.discount_nominal_3


    # @api.depends('amount_discount_total')
    # @api.onchange('invoice_line_ids', 'invoice_line_ids.price_base_subtotal', 'discount_total', 'discount_nominal_total')

    # def _compute_base_subtotal(self):
    #     super(AccountInvoice, self)._compute_base_subtotal()
    #     self._amount_all_discount()


    def amount_all_discount(self):
        for inv_doc in self:
            amount_before_disc = 0.0
            #Merangkum Semua Baris
            for line in inv_doc.invoice_line_ids:
                amount_before_disc += line.price_base_subtotal #Nilai Total Sebelum Diskon untuk menghitung Porsi
            #Mencari porsi diskon total per line
            for line in inv_doc.invoice_line_ids:
                if amount_before_disc > 0:
                    porsi = (line.price_base_subtotal / amount_before_disc)
                else:
                    porsi = 0
                line.update({
                    'discount_nominal_3' : porsi * inv_doc.discount_nominal_total
                })
                line.line_compute_diskon()
                line._compute_price()
            for line in inv_doc.tax_line_ids:
                line._compute_base_amount()
            inv_doc.update({
                'amount_before_disc' : amount_before_disc,
            })
