{

    'name': '(Deprecated) VS - Multiple Discount in Invoice',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Backend',
    'website': 'http://visi.co.id/',
    'summary': 'Invoice multiple Discount',
    'description': '''


    Referensi : http://ortax.org/ortax/?mod=forum&page=show&idtopik=45207
    Requirement :

    1. ada beberapa Diskon pada Invoice


    ''',
    'depends': [
        'account',
        'vs_account_discount_value',
    ],
    'data': [
            'views/account_invoice.xml',
            # 'views/konfigurasi_diskon.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
