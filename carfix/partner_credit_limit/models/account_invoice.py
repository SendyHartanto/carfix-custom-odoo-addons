# See LICENSE file for full copyright and licensing details.

from datetime import datetime

from odoo import api, models, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def check_limit(self):
        self.ensure_one()
        partner = self.partner_id
        moveline_obj = self.env['account.move.line']
        movelines = moveline_obj.search(
            [('partner_id', '=', partner.id),
             ('account_id.user_type_id.name', 'in', ['Receivable', 'Payable']),
             ('full_reconcile_id', '=', False)]
        )
        debit, credit = 0.0, 0.0
        today_dt = datetime.strftime(datetime.now().date(), DF)
        for line in movelines:
            if line.date_maturity < today_dt:
                credit += line.debit
                debit += line.credit
        if partner.credit_limit > 0:
            if (credit - debit + self.amount_total) > partner.credit_limit:
                if not partner.over_credit:
                    msg = 'Customer Invoice gagal dibuat. \n' \
                        'Sebelum tanggal %s sudah ada hutang sebesar Rp %s !\n' \
                        'Jumlah Invoice sekarang : %s  \n' \
                        'Limit Customer %s : Rp %s ' % (today_dt, '{:,.2f}'.format(credit - debit), '{:,.2f}'.format(self.amount_total) , partner.name, '{:,.2f}'.format(partner.credit_limit))
                    raise UserError(_('Credit Over Limits !\n' + msg))
                partner.write({'credit_limit': credit - debit + self.amount_total})
        return True

    @api.multi
    def action_invoice_open(self):
        for invoice in self:
            invoice.check_limit()
        res = super(AccountInvoice, self).action_invoice_open()
        return res
