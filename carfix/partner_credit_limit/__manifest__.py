
{

    'name': 'Partner Credit Limit',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Accounting',
    'website': 'http://visi.co.id/',
    'summary': 'Restful Api Service',
    'depends': [
        'account',
    ],
    'data': [
        'views/partner_view.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
