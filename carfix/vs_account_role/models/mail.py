from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

#TAMBAHAN AKUN UNTUK DEBIT, CREDIT CARD, E MONEY

class Message(models.Model):
    _inherit = 'mail.message'

    _message_read_limit = 30

    @api.model
    def _get_default_from(self):
        return "admin@gmail.com"
        # if self.env.user.email:
        #     return formataddr((self.env.user.name, self.env.user.email))
        # raise UserError(_("Unable to send email, please configure the sender's email address."))
