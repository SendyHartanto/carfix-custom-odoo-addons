from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    account_id = fields.Many2one(implied_group="vs_account_role.account_base_user")
