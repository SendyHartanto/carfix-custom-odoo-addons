{

    'name': 'Account Module Role',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'AR Process',
    'website': 'http://visi.co.id/',
    'summary': 'Accounting Groups and User Access',
    'description': '''
    References
    1. https://hiring.monster.com/hr/hr-best-practices/recruiting-hiring-advice/job-descriptions/accounts-receivable-clerk-job-description-sample.aspx
    ''',
    'depends': [
        'account',
    ],
    'data': [
        'security/user_role.xml',
        'security/ir.model.access.csv',
        'views/account_menuitem.xml',
        'views/customer_invoice.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
