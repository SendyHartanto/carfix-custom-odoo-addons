from odoo import fields, models, api, _
from datetime import datetime, date
from odoo.exceptions import UserError


class SaleReturnWizard(models.TransientModel):
    _name = 'sale_return.report.wizard'
    
    @api.multi
    def get_first_date(self):
        current_date = date.today()
        return date(current_date.year, current_date.month, 1)
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Start Date', default=get_first_date)
    date_to = fields.Date('End Date', default=fields.date.today())
    partner_ids = fields.Many2many('res.partner', string='Customer(s)')
    invoice_state = fields.Selection([('all', 'All Invoices'), ('open', 'Not Paid'), ('paid', 'Paid')], string="Invoice Status", default='open')
    
    @api.multi
    def view_sale_return_report(self):
        return self.env.ref('accounting_report_xlsx.sale_return_xlsx').report_action(self.ids, config=False)
    
    @api.multi
    def _get_sale_return_datas(self):
        datas = {}
        datas['ids'] = [self['id']]
        datas['type'] = 'Sale Return'
        datas['company_name'] = self.company_id.name + ' - ' + self.company_id.currency_id.name
        datas['model'] = self._name
        datas['form'] = self.read()[0]
        datas['date_from'] = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%d %B %Y')
        datas['date_to'] = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%d %B %Y')
        datas['partner_ids'] = ', '.join(map(str, [x.ref for x in self['partner_ids']]))
        
        compiled_data = {}
        where_query = ''
        
        if self.invoice_state == 'all':
            state = ('open', 'paid')
        elif self.invoice_state == 'open':
            state = ('open', 'only')
        else:
            state = ('paid', 'only')
        
        if self.partner_ids:
            datas['partner_ids'] = ', '.join(map(str, [x.ref for x in self['partner_ids']]))
        else:
            datas['partner_ids'] = 'All'
            
        if len(self.partner_ids) > 1:
            where_query += " and ap.partner_id in %s" % (str(tuple(self.partner_ids.ids)))
        elif len(self.partner_ids) == 1:
            where_query += " and ap.partner_id = %s" % (tuple(self.partner_ids.ids))

        query = """
                select ap.name as giro_no, ap.payment_date as rec_date, ap.payment_date as date_due,
                        rp.ref as ref, rp.name as partner, ap.communication as bank, ap.communication as dth, ap.amount as amount
                from account_payment ap
                    left join 
                        res_partner rp on (rp.id = ap.partner_id)
                where 
                    ap.company_id = %s and
                    ap.partner_type = 'customer' and
                    ap.payment_type = 'inbound' and
                    ap.payment_date >= '%s' and
                    ap.payment_date <= '%s' and
                    ap.state in %s %s
                order by ap.payment_date, rp.ref;
                """ % (self.company_id.id, self.date_from, self.date_to, state, where_query)
         
        self.sudo().env.cr.execute(query)
        data = self.sudo().env.cr.fetchall()
        return data
    
