from odoo import fields, models, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, date, timedelta
from odoo.exceptions import UserError, ValidationError


class TrialBalanceReportWizard(models.TransientModel):
    _name = 'trial.balance.report.wizard'
    
    @api.multi
    def get_first_date(self):
        current_date = date.today()
        return date(current_date.year, current_date.month, 1)
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Start Date', default=get_first_date)
    date_to = fields.Date('End Date', default=date.today())
    account_ids = fields.Many2many('account.account', string='Account(s)')
    target_move = fields.Selection([('posted', 'All Posted Entries'), ('all', 'All Entries')], string="Target Moves", default='all')
    
    @api.multi
    def view_trial_balance_report(self):
        return self.env.ref('accounting_report_xlsx.trial_balance_xlsx').report_action(self.ids, config=False)
    
    @api.multi
    def _get_trial_balance_datas(self):
        datas = {}
        datas['ids'] = [self['id']]
        datas['type'] = 'Trial Balance'
        datas['company_name'] = self.company_id.name + ' - ' + self.company_id.currency_id.name
        datas['model'] = self._name
        datas['form'] = self.read()[0]
        datas['date_from'] = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%d %B %Y')
        datas['date_to'] = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%d %B %Y')
        datas['account_ids'] = ', '.join(map(str, [x.code for x in self['account_ids']]))
        
        where_query = ''
        if self.target_move == 'all':
            state = ('draft', 'posted')
            datas['target_move'] = 'All Entries'
        else:
            state = ('posted', 'done')
            datas['target_move'] = 'All Posted Entries'
        
        if self.account_ids:
            datas['account_ids'] = ', '.join(map(str, [x.code for x in self['account_ids']]))
        else:
            datas['account_ids'] = 'All'
        if len(self.account_ids) > 1:
            where_query += " and aa.id in %s" % (str(tuple(self.account_ids.ids)))
        elif len(self.account_ids) == 1:
            where_query += " and aa.id = %s" % (tuple(self.account_ids.ids))
            
        cutoff_date = self.date_from[:4] + '-01-01'
        
        query = """
                select code, account, type, coalesce(sum(init),0), coalesce(sum(debit),0), coalesce(sum(credit),0)
                from 
                    ((select aa.code, aa.name as account, 0 as init, coalesce(sum(aml.debit), 0) as debit, coalesce(sum(aml.credit), 0) as credit, aat.name as type
                        from account_account aa
                           left join
                              account_move_line aml on (aml.account_id=aa.id and aml.date >= '%s' and aml.date <= '%s')
                           left join
                              account_account_type aat on (aat.id = aa.user_type_id)
                           left join
                              account_move am on (am.id = aml.move_id and am.state in %s)
                        where
                            aa.company_id = %s %s
                        group by aa.code, aa.name, aat.name)
                    union
                    (select aa.code, aa.name as account, coalesce(sum(aml.debit),0)-coalesce(sum(aml.credit),0) as init, 0 as debit, 0 as credit, aat.name as type
                        from account_account aa
                           left join
                              account_account_type aat on (aat.id = aa.user_type_id)
                           left join
                              account_move_line aml on (
                                  aml.account_id=aa.id and aml.date < '%s' and 
                                  aml.date >= CASE WHEN aat.include_initial_balance THEN '1970-01-01'::date ELSE '%s' END)
                           left join
                              account_move am on (am.id = aml.move_id and am.state in %s)
                        where
                            aa.company_id = %s %s
                        group by aa.code, aa.name, aat.name)) as a
                group by code, account, type
                order by code;
                """ % (self.date_from, self.date_to, state, self.company_id.id, where_query, self.date_from, cutoff_date, state, self.company_id.id, where_query)
        self.sudo().env.cr.execute(query)
        data = self.sudo().env.cr.fetchall()
        datas['csv'] = data
        return datas
    
