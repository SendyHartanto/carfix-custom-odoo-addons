from . import general_ledger_wizard, supplier_invoice_wizard, list_ap_wizard, giro_payment_wizard
from . import aged_partner_balance_wizard, list_journal_wizard, sale_return_wizard, trial_balance_wizard
