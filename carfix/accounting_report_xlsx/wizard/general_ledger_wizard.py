from odoo import fields, models, api, _
from datetime import datetime, date
from odoo.exceptions import UserError


class GeneralLedgerReportWizard(models.TransientModel):
    _name = 'general.ledger.report.wizard'
    
    @api.multi
    def get_first_date(self):
        current_date = date.today()
        return date(current_date.year, current_date.month, 1)
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Start Date', default=get_first_date)
    date_to = fields.Date('End Date', default=fields.date.today())
    account_ids = fields.Many2many('account.account', string='Account(s)')
    target_move = fields.Selection([('posted', 'All Posted Entries'), ('all', 'All Entries')], string="Target Moves", default='all')
    
    @api.multi
    def view_general_ledger_report(self):
        return self.env.ref('accounting_report_xlsx.general_ledger_xlsx').report_action(self.ids, config=False)
    
    @api.multi
    def _get_general_ledger_datas(self):
        datas = {}
        datas['ids'] = [self['id']]
        datas['type'] = 'General Ledger'
        datas['company_name'] = self.company_id.name + ' - ' + self.company_id.currency_id.name
        datas['model'] = self._name
        datas['form'] = self.read()[0]
        datas['date_from'] = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%d %B %Y')
        datas['date_to'] = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%d %B %Y')
        datas['account_ids'] = ', '.join(map(str, [x.code for x in self['account_ids']]))
        
        compiled_data = {}
        where_query = ''
        
        if self.target_move == 'all':
            state = ('draft', 'posted')
            datas['target_move'] = 'All Entries'
        else:
            state = ('posted', 'done')
            datas['target_move'] = 'All Posted Entries'
        
        if self.account_ids:
            datas['account_ids'] = ', '.join(map(str, [x.code for x in self['account_ids']]))
        else:
            datas['account_ids'] = 'All'
            
        if len(self.account_ids) > 1:
            where_query += " and aml.account_id in %s" % (str(tuple(self.account_ids.ids)))
        elif len(self.account_ids) == 1:
            where_query += " and aml.account_id = %s" % (tuple(self.account_ids.ids))

        ####### INITIAL BALANCE
        query = """
                select 
                    aa.code as account, 
                    aa.name as acc_name,
                    sum(aml.debit) as debit, 
                    sum(aml.credit) as credit
                from 
                    account_move_line aml
                    left join
                        account_account aa on (aa.id = aml.account_id)
                    left join
                        account_move am on (am.id = aml.move_id)
                where
                    aml.company_id = %s and
                    aml.date < '%s' %s
                group by 
                    aa.code, aa.name;
                     
        """ %  (self.company_id.id, self.date_from, where_query)
        self.sudo().env.cr.execute(query)
        data = self.sudo().env.cr.dictfetchall()
        for move_line in data:
            account = move_line['account'] + ' - ' + move_line['acc_name']
            if not compiled_data.get(account):
                compiled_data[account] = {'init_debit' : 0, 'init_credit' : 0, 'move_line' : []}
            compiled_data[account]['init_debit'] += move_line['debit']
            compiled_data[account]['init_credit'] += move_line['credit']
             
        query = """
                select aml.date, am.name as entries, aj.code as journal, aa.code as account, aa.name as acc_name, rp.name as partner, 
                    am.ref as ref, aml.name as label, string_agg(aa1.code, ', ') as counterpart, aml.debit, aml.credit
                from account_move_line aml
                    left join 
                        account_move_line aml1 on (aml1.move_id=aml.move_id and aml1.id !=aml.id ) 
                    left join 
                        account_account aa1 on (aa1.id=aml1.account_id) 
                    left join
                        account_move am on (am.id = aml.move_id)
                    left join
                        account_journal aj on (aj.id = aml.journal_id)
                    left join
                        account_account aa on (aa.id = aml.account_id)
                    left join 
                        res_partner rp on (rp.id = aml.partner_id)
                where 
                    aml.company_id = %s and
                    aml.date >= '%s' and
                    aml.date <= '%s' and
                    am.state in %s %s
                group by
                    aml.date, aml.id, am.name, aj.code, aa.code, aa.name , rp.name, aml.name, aml.debit, aml.credit, am.ref
                order by aml.date, aml.id;
                """ %  (self.company_id.id, self.date_from, self.date_to, state, where_query)
         
        self.sudo().env.cr.execute(query)
        data = self.sudo().env.cr.dictfetchall()
         
        for move_line in data:
            account = move_line['account'] + ' - ' + move_line['acc_name']
            if not compiled_data.get(account):
                compiled_data[account] = {'init_debit' : 0, 'init_credit' : 0, 'move_line' : []}
            #TODO: jika cash in transit, maka tampilkan nomor session pada label
            compiled_data[account]['move_line'].append([
                                                        move_line['entries'], 
                                                        move_line['date'], 
                                                        move_line['ref'], 
                                                        move_line['label'], 
                                                        move_line['debit'], 
                                                        move_line['credit']])
        return compiled_data
    
