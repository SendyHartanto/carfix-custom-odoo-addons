from odoo import fields, models, api, _
from datetime import datetime, date
from odoo.exceptions import UserError


class ListJournalReportWizard(models.TransientModel):
    _name = 'list.journal.report.wizard'
    
    @api.multi
    def get_first_date(self):
        current_date = date.today()
        return date(current_date.year, current_date.month, 1)
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Start Date', default=get_first_date)
    date_to = fields.Date('End Date', default=fields.date.today())
    journal_ids = fields.Many2many('account.journal', string='Journal(s)')
    target_move = fields.Selection([('posted', 'All Posted Entries'), ('all', 'All Entries')], string="Target Moves", default='all')
    
    @api.multi
    def view_list_journal_report(self):
        return self.env.ref('accounting_report_xlsx.list_journal_xlsx').report_action(self.ids, config=False)
    
    @api.multi
    def _get_list_journal_datas(self):
        datas = {}
        compiled_data = {}
        datas['ids'] = [self['id']]
        datas['company_name'] = self.company_id.name + ' - ' + self.company_id.currency_id.name
        datas['model'] = self._name
        datas['form'] = self.read()[0]
        datas['date_from'] = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%d %B %Y')
        datas['date_to'] = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%d %B %Y')
        datas['journal_ids'] = ', '.join(map(str, [x.code for x in self['journal_ids']]))
        
        domain = [
            ('date', '>=', self.date_from),
            ('date', '<=', self.date_to),
            ('company_id', '=', self.company_id.id)
        ]
        
        if self.target_move == 'all':
            state = ['draft', 'posted']
            domain.append(('state', 'in', state))
            datas['target_move'] = 'All Entries'
        else:
            state = ['posted', 'done']
            domain.append(('state', 'in', state))
            datas['target_move'] = 'All Posted Entries'
            
        if self.journal_ids:
            domain.append(('journal_id', 'in', self.journal_ids.ids))
            datas['journal_ids'] = ', '.join(map(str, [x.code for x in self['journal_ids']]))
        else:
            datas['journal_ids'] = 'All'
        
        move_ids = self.env['account.move'].search(domain)
        
        for move_id in move_ids:
            journal = '%s - %s' % (move_id.journal_id.code, move_id.journal_id.name)
            if not compiled_data.get(journal):
                compiled_data[journal] = {
                    'code': move_id.journal_id.code,
                    'name': move_id.journal_id.name,
                    'moves': {}
                }
            
            if not compiled_data[journal]['moves'].get(move_id.name):
                compiled_data[journal]['moves'][move_id.name] = {
                    'date': move_id.date,
                    'name': move_id.name,
                    'ref': move_id.ref or '',
                    'move_line': []
                }
            
            for move_line_id in move_id.line_ids.sorted(key=lambda l: -l.debit):
                compiled_data[journal]['moves'][move_id.name]['move_line'].append([
                    '%s %s' % (move_line_id.account_id.code, move_line_id.account_id.name),
                    move_line_id.name or '',
                    move_line_id.debit,
                    move_line_id.credit                                    
                ])
        datas['csv'] = compiled_data
        return datas
    
