from odoo import fields, models, api, _
from datetime import datetime, date
from odoo.exceptions import UserError


class ListAPReportWizard(models.TransientModel):
    _name = 'list_ap.report.wizard'
    
    @api.multi
    def get_first_date(self):
        current_date = date.today()
        return date(current_date.year, current_date.month, 1)
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Start Date', default=get_first_date)
    date_to = fields.Date('End Date', default=fields.date.today())
    partner_ids = fields.Many2many('res.partner', string='Supplier(s)')
    invoice_state = fields.Selection([('all', 'All Invoices'), ('open', 'Not Paid'), ('paid', 'Paid')], string="Invoice Status", default='open')
    
    @api.multi
    def view_list_ap_report(self):
        return self.env.ref('accounting_report_xlsx.list_ap_xlsx').report_action(self.ids, config=False)
    
    @api.multi
    def _get_list_ap_datas(self):
        datas = {}
        datas['ids'] = [self['id']]
        datas['type'] = 'List AP'
        datas['company_name'] = self.company_id.name + ' - ' + self.company_id.currency_id.name
        datas['model'] = self._name
        datas['form'] = self.read()[0]
        datas['date_from'] = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%d %B %Y')
        datas['date_to'] = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%d %B %Y')
        datas['partner_ids'] = ', '.join(map(str, [x.ref for x in self['partner_ids']]))
        
        compiled_data = {}
        where_query = ''
        
        if self.invoice_state == 'all':
            state = ('draft', 'open', 'paid')
            datas['invoice_state'] = 'All Invoices'
        elif self.invoice_state == 'open':
            state = ('open', 'all')
            datas['invoice_state'] = 'Not Paid Invoices'
        else:
            state = ('paid', 'all')
            datas['invoice_state'] = 'Paid Invoices'            
        
        if self.partner_ids:
            datas['partner_ids'] = ', '.join(map(str, [x.ref for x in self['partner_ids']]))
        else:
            datas['partner_ids'] = 'All'
            
        if len(self.partner_ids) > 1:
            where_query += " and ai.partner_id in %s" % (str(tuple(self.partner_ids.ids)))
        elif len(self.partner_ids) == 1:
            where_query += " and ai.partner_id = %s" % (tuple(self.partner_ids.ids))

        query = """
                select rp.ref as ref, rp.name as partner, ai.number as invoice, ai.date_invoice as date_invoice, 
                    ai.date_due as date_due, ai.date_due as date_ppn, ai.amount_total as amount_total,
                    ai.residual as amount_due, ai.amount_tax as amount_tax, ai.residual as payment_due
                from account_invoice ai
                    left join 
                        res_partner rp on (rp.id = ai.partner_id)
                where 
                    ai.company_id = %s and
                    ai.type = 'in_invoice' and
                    ai.date_invoice >= '%s' and
                    ai.date_invoice <= '%s' and
                    ai.state in %s %s
                group by
                    rp.ref, rp.name, ai.number, ai.date_invoice, ai.date_due, ai.amount_total, ai.residual, ai.amount_tax
                order by rp.ref, ai.date_invoice;
                """ % (self.company_id.id, self.date_from, self.date_to, state, where_query)
         
        self.sudo().env.cr.execute(query)
        data = self.sudo().env.cr.dictfetchall()
         
        for move_line in data:
            partner = '%s - %s' % (move_line['ref'], move_line['partner'])
            if not compiled_data.get(partner):
                compiled_data[partner] = {'ref': move_line['ref'] , 'name': move_line['partner'], 'invoices': []}
            compiled_data[partner]['invoices'].append([
                                                    move_line['invoice'],
                                                    move_line['date_invoice'],
                                                    move_line['date_due'],
                                                    move_line['date_ppn'],
                                                    move_line['amount_total'],
                                                    move_line['amount_due'],
                                                    move_line['amount_tax'],
                                                    move_line['amount_total'] - move_line['amount_due'],
                                                    move_line['amount_due'],
                                                    0, 0, 0                                                    
                                                ])
        return compiled_data
    
