from odoo import models, _
import operator
from datetime import datetime
from odoo.exceptions import UserError
from xlsxwriter.utility import xl_rowcol_to_cell


class aged_partner_balance_xlsx(models.AbstractModel):
    _name = 'report.accounting_report_xlsx.aged_partner_balance.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
              ['   Date', 12],
              ['Entry', 15],
              ['Label', 20],
              ['Journal', 8]
            ]
    
        style_hdr_blue = workbook.add_format({'border': 1, 'bg_color': 'cyan', 'num_format': '#,##0'})
        style_hdr_bold_blue = workbook.add_format({'border': 1, 'bg_color': 'cyan', 'num_format': '#,##0', 'bold': True})
        style_hdr_center_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan', 'num_format': '#,##0'})
        style_hdr_yellow = workbook.add_format({'border': 1, 'bg_color': 'yellow', 'num_format': '#,##0'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_aged_partner_balance_datas()
            report_name = 'Aged Partner Balance'
            ws = workbook.add_worksheet(report_name[:31])
            ws.merge_range('A1:H1', 'AGED PARTNER BALANCE - ' + obj.company_id.name, style_normal_bold)   
            ws.freeze_panes(5, 5)
            
            ws.write('A3:A3', 'Start Date', style_hdr_center_blue)
            ws.merge_range('B3:C3', 'Account Filter', style_hdr_center_blue)
            ws.merge_range('D3:E3', 'Partner Filter', style_hdr_center_blue)
            ws.write('F3:F3', 'Target Moves', style_hdr_center_blue)
    
            ws.write('A4:A4', datetime.strptime(obj.start_date, '%Y-%m-%d').strftime('%d %B %Y'), style_normal_center)
            ws.merge_range('B4:C4', ', '.join(map(str, [x.code for x in obj.account_ids])) if obj.account_ids else 'All', style_normal_center)
            ws.merge_range('D4:E4', ', '.join(map(str, [x.ref for x in obj.partner_ids])) if obj.partner_ids else 'All', style_normal_center)
            ws.write('F4:F4', 'All Entries' if obj.target_move == 'all' else 'All Posted Entries', style_normal_center)
            
            list_overdues = []
            list_overdues.append(['Balance', 15])
            list_overdues.append(['Not Due', 15])
            od = 'Overdue <'
            periods = obj.period_length.split(',')
            a = 0
            for period in periods:
                overdue = [od + ' ' + str(int(period) + int(a)), 15]
                a += int(period)
                list_overdues.append(overdue)
            list_overdues.append(['Older', 15])
            len_overdues = len(list_overdues)
    
            row_count = 6
            col_count = 0
            for account in sorted(datas.items(), key=operator.itemgetter(0)):
                ws.merge_range('A%s:F%s' % (row_count, row_count), account[0], style_hdr_blue)
                for partner in sorted(account[1]['partner'].items(), key=operator.itemgetter(0)):
                    row_count += 1
                    ws.merge_range('A%s:F%s' % (row_count, row_count), partner[0], style_normal)
                    for column in columns:
                        ws.set_column(col_count, col_count, column[1]) 
                        ws.write(row_count, col_count, column[0], style_hdr_yellow)
                        col_count += 1
                    for list_overdue in list_overdues:
                        ws.set_column(col_count, col_count, list_overdue[1]) 
                        ws.write(row_count, col_count, list_overdue[0], style_hdr_yellow)
                        col_count += 1
                        
                    row_count += 1
                    col_count = 0
                    row_overdue = row_count
                    for lines in partner[1]:
                        for line in lines:
                            if col_count == 0:
                                ws.write(row_count, col_count, '   ' + str(line), style_normal)
                            else:
                                ws.write(row_count, col_count, line, style_normal)
                            col_count += 1
                        col_count = 0
                        row_count += 1
                    row_count += 1
                    ws.merge_range('A%s:D%s' % (row_count, row_count), partner[0], style_normal_bold)
                    col_count = 4
                    len_overdue = 0
                    while len_overdue < len_overdues:
                        cell_overdue_start = xl_rowcol_to_cell(row_overdue, col_count)
                        cell_overdue_end = xl_rowcol_to_cell(row_count - 2, col_count)
                        ws.write(row_count - 1, col_count, '=sum(%s:%s)' % (cell_overdue_start, cell_overdue_end), style_normal_bold)
                        col_count += 1
                        len_overdue += 1
                    col_count = 0
                    row_count += 1
                ws.merge_range('A%s:D%s' % (row_count, row_count), account[0], style_hdr_bold_blue)
                col_count = 4
                for ending in account[1]['ending']:
                    ws.write(row_count - 1, col_count, ending, style_hdr_bold_blue)
                    col_count += 1
                row_count += 2
                col_count = 0
            
