from odoo import models, _
import operator
from odoo.exceptions import UserError


class trial_balance_xlsx(models.AbstractModel):
    _name = 'report.accounting_report_xlsx.trial_balance.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
              ['Code', 10],
              ['Account', 40],
              ['Type', 20],
              ['Initial Balance', 15],
              ['Debit', 15],
              ['Credit', 15],
              ['Ending Balance', 20]
        ]
    
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_trial_balance_datas()
            report_name = 'Trial Balance'
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(6, 0)
            
            sheet.merge_range('A1:H1', 'TRIAL BALANCE - ' + obj.company_id.name, style_normal_bold)
        
            sheet.merge_range('A2:B2', 'Date', style_hdr_blue)
            sheet.merge_range('C2:E2', 'Account Filters', style_hdr_blue)
            sheet.merge_range('F2:G2', 'Target Moves', style_hdr_blue)
            
            account_filters = ', '.join(map(str, [x.code for x in obj['account_ids']])) if obj.account_ids else 'All'
            if obj.target_move == 'all':
                target_move = 'All Entries'
            else:
                target_move = 'All Posted Entries'
                
            sheet.merge_range('A3:B3', obj.date_from + ' - ' + obj.date_to, style_normal_center)
            sheet.merge_range('C3:E3', account_filters, style_normal_center)
            sheet.merge_range('F3:G3', target_move, style_normal_center)
            
            row_count = 5
            col_count = 0
            for column in columns:
                sheet.set_column(col_count, col_count, column[1]) 
                sheet.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
                    
            row_count += 1
            col_count = 0
            for lines in datas['csv']:
                for line in lines:
                    sheet.write(row_count, col_count, line, style_normal)
                    col_count += 1  
                sheet.write(row_count, col_count, '=D%s+E%s-F%s' % (row_count + 1, row_count + 1, row_count + 1), style_normal_bold)
                col_count = 0
                row_count += 1
                
