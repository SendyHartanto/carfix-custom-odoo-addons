from odoo import models, _
import operator
from odoo.exceptions import UserError


class list_ap_xlsx(models.AbstractModel):
    _name = 'report.accounting_report_xlsx.list_ap.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
                      ['CODE', 10],
                      ['NAME', 20],
                      ['INVOICE', 13],
                      ['DATE', 10],
                      ['DUE DATE', 10],
                      ['PPN DATE', 10],
                      ['AMOUNT', 13],
                      ['DUE AMOUNT', 13],
                      ['PPN', 13],
                      ['PAYMENT', 13],
                      ['DUE PAYMENT', 13],
                      ['DN', 10],
                      ['CN', 10],
                      ['PPH', 10],
                ]
    
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'border': 1, 'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_list_ap_datas()
            report_name = 'List AP Trade'
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(5, 0)
            
            sheet.merge_range('A1:K1', 'List AP Trade - ' + obj.company_id.name, style_normal_bold)
        
            sheet.merge_range('A3:C3', 'Date', style_hdr_blue)
            sheet.merge_range('D3:I3', 'Supplier Filters', style_hdr_blue)
            sheet.merge_range('J3:K3', 'Invoice Status', style_hdr_blue)
            
            vendor_filters = ', '.join(map(str, [x.name or '/' for x in obj['partner_ids']])) if obj.partner_ids else 'All Suppliers'
            if obj.invoice_state == 'all':
                invoice_state = 'All Invoices'
            elif obj.invoice_state == 'open':
                invoice_state = 'Not Paid invoices'
            else:
                invoice_state = 'Paid invoices'
                
            sheet.merge_range('A4:C4', obj.date_from + ' - ' + obj.date_to, style_normal_center)
            sheet.merge_range('D4:I4', vendor_filters, style_normal_center)
            sheet.merge_range('J4:K4', invoice_state, style_normal_center)
            
            row_count = 5
            col_count = 0
            for column in columns:
                sheet.set_column(col_count, col_count, column[1]) 
                sheet.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
                
            col_count = 0
            for partner in sorted(datas.items(), key=operator.itemgetter(0)):
                row_count += 1
                sheet.write(row_count, col_count, partner[1]['ref'], style_normal)
                col_count += 1
                sheet.write(row_count, col_count, partner[1]['name'], style_normal)
                col_count += 1
                row_start = row_count + 1
                for invoice in partner[1]['invoices']:
                    for line in invoice:
                        sheet.write(row_count, col_count, line, style_normal)
                        col_count += 1  
                    col_count = 2
                    row_count += 1
                sheet.merge_range('C%s:F%s' % (row_count + 1, row_count + 1), 'SUB TOTAL', style_bold_center)
                sheet.write('G%s:G%s' % (row_count + 1, row_count + 1), '=SUM(G%s:G%s)' % (row_start, row_count), style_normal_bold)
                sheet.write('H%s:H%s' % (row_count + 1, row_count + 1), '=SUM(H%s:H%s)' % (row_start, row_count), style_normal_bold)
                sheet.write('I%s:I%s' % (row_count + 1, row_count + 1), '=SUM(I%s:I%s)' % (row_start, row_count), style_normal_bold)
                sheet.write('J%s:J%s' % (row_count + 1, row_count + 1), '=SUM(J%s:J%s)' % (row_start, row_count), style_normal_bold)
                sheet.write('K%s:K%s' % (row_count + 1, row_count + 1), '=SUM(K%s:K%s)' % (row_start, row_count), style_normal_bold)
                sheet.write('L%s:L%s' % (row_count + 1, row_count + 1), '=SUM(L%s:L%s)' % (row_start, row_count), style_normal_bold)
                sheet.write('M%s:M%s' % (row_count + 1, row_count + 1), '=SUM(M%s:M%s)' % (row_start, row_count), style_normal_bold)
                sheet.write('N%s:N%s' % (row_count + 1, row_count + 1), '=SUM(N%s:N%s)' % (row_start, row_count), style_normal_bold)
                
                col_count = 0
                row_count += 1
            
#             row_count += 1
#             sheet.merge_range('C%s:F%s' % (row_count + 1, row_count + 1), 'GRAND TOTAL', style_bold_center)
#             sheet.write('G%s:G%s' % (row_count + 1, row_count + 1), '=SUM(G%s:G%s)' % (row_start, row_count), style_normal_bold)
#             sheet.write('H%s:H%s' % (row_count + 1, row_count + 1), '=SUM(H%s:H%s)' % (row_start, row_count), style_normal_bold)
#             sheet.write('I%s:I%s' % (row_count + 1, row_count + 1), '=SUM(I%s:I%s)' % (row_start, row_count), style_normal_bold)
#             sheet.write('J%s:J%s' % (row_count + 1, row_count + 1), '=SUM(J%s:J%s)' % (row_start, row_count), style_normal_bold)
#             sheet.write('K%s:K%s' % (row_count + 1, row_count + 1), '=SUM(K%s:K%s)' % (row_start, row_count), style_normal_bold)
#             sheet.write('L%s:L%s' % (row_count + 1, row_count + 1), '=SUM(L%s:L%s)' % (row_start, row_count), style_normal_bold)
#             sheet.write('M%s:M%s' % (row_count + 1, row_count + 1), '=SUM(M%s:M%s)' % (row_start, row_count), style_normal_bold)
#             sheet.write('N%s:N%s' % (row_count + 1, row_count + 1), '=SUM(N%s:N%s)' % (row_start, row_count), style_normal_bold)
            
