from odoo import models, _
import operator
from odoo.exceptions import UserError


class supplier_invoice_xlsx(models.AbstractModel):
    _name = 'report.accounting_report_xlsx.supplier_invoice.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
                      ['SUPPLIER', 20],
                      ['INVOICE', 15],
                      ['DATE', 13],
                      ['PART CODE', 15],
                      ['DESCRIPTION', 20],
                      ['QTY', 5],
                      ['PRICE', 13],
                      ['FREE DISC', 13],
                      ['ADD DISC', 13],
                      ['AMOUNT', 15],
                      ['FLAG', 5]
                ]
    
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'border': 1, 'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_supplier_invoice_datas()
            report_name = 'List Supplier Invoices'
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(5, 0)
            
            sheet.merge_range('A1:K1', 'List Supplier Invoices - ' + obj.company_id.name, style_normal_bold)
        
            sheet.merge_range('A3:C3', 'Date', style_hdr_blue)
            sheet.merge_range('D3:I3', 'Supplier Filters', style_hdr_blue)
            sheet.merge_range('J3:K3', 'Invoice Status', style_hdr_blue)
            
            vendor_filters = ', '.join(map(str, [x.name or '/' for x in obj['partner_ids']])) if obj.partner_ids else 'All Suppliers'
            if obj.invoice_state == 'all':
                invoice_state = 'All Invoices'
            elif obj.invoice_state == 'open':
                invoice_state = 'Not Paid invoices'
            else:
                invoice_state = 'Paid invoices'
                
            sheet.merge_range('A4:C4', obj.date_from + ' - ' + obj.date_to, style_normal_center)
            sheet.merge_range('D4:I4', vendor_filters, style_normal_center)
            sheet.merge_range('J4:K4', invoice_state, style_normal_center)
            
            row_count = 5
            col_count = 0
            for column in columns:
                sheet.set_column(col_count, col_count, column[1]) 
                sheet.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
                
            col_count = 0
            for partner in sorted(datas.items(), key=operator.itemgetter(0)):
                row_count += 1
                sheet.write(row_count, col_count, partner[0], style_normal)
                col_count += 1
                for invoice in sorted(partner[1]['invoices'].items(), key=operator.itemgetter(0)):
                    sheet.write(row_count, col_count, invoice[0], style_normal)
                    col_count += 1  
                    sheet.write(row_count, col_count, invoice[1]['date'], style_normal_center)
                    col_count += 1  
                    for lines in invoice[1]['invoice_line']:
                        for line in lines:
                            sheet.write(row_count, col_count, line, style_normal)
                            col_count += 1  
                        sheet.write(row_count, col_count, '=(F%s*G%s)-(H%s+I%s)' % (row_count + 1, row_count + 1, row_count + 1, row_count + 1), style_normal)
                        col_count += 1
                        sheet.write(row_count, col_count, '', style_normal)
                        col_count = 3
                        row_count += 1
                    col_count = 1
                sheet.merge_range('A%s:B%s' % (row_count + 1, row_count + 1), 'Freight Disc :       0', style_bold_center)
                sheet.merge_range('C%s:D%s' % (row_count + 1, row_count + 1), 'Add Disc :       0', style_bold_center)
                sheet.merge_range('E%s:G%s' % (row_count + 1, row_count + 1), 'Net Amount :       0', style_bold_center)
                sheet.write('H%s:H%s' % (row_count + 1, row_count + 1), 'PPN :        0', style_bold_center)
                sheet.write('I%s:I%s' % (row_count + 1, row_count + 1), 'PPH :        0', style_bold_center)
                sheet.merge_range('J%s:K%s' % (row_count + 1, row_count + 1), 'AP :        0', style_bold_center)
                row_count += 1
                col_count = 0
            
