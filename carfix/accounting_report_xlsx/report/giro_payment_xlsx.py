from odoo import models, _
import operator
from odoo.exceptions import UserError


class giro_payment_xlsx(models.AbstractModel):
    _name = 'report.accounting_report_xlsx.giro_payment.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
                      ['GIRO NO', 10],
                      ['REC DATE', 10],
                      ['DATE DUE', 10],
                      ['CODE', 5],
                      ['CUSTOMER', 15],
                      ['BANK', 10],
                      ['DTH NO', 13],
                      ['AMOUNT', 13],
                ]
    
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'border': 1, 'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_giro_payment_datas()
            report_name = 'Giro Over Due'
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(5, 0)
            
            sheet.merge_range('A1:H1', 'GIRO OVER DUE - ' + obj.company_id.name, style_normal_bold)
        
            sheet.merge_range('A3:C3', 'Date', style_hdr_blue)
            sheet.merge_range('D3:H3', 'Customer Filters', style_hdr_blue)
            
            customer_filters = ', '.join(map(str, [x.name or '/' for x in obj['partner_ids']])) if obj.partner_ids else 'All Customer'
                
            sheet.merge_range('A4:C4', obj.date_from + ' - ' + obj.date_to, style_normal_center)
            sheet.merge_range('D4:H4', customer_filters, style_normal_center)
            
            row_count = 5
            col_count = 0
            for column in columns:
                sheet.set_column(col_count, col_count, column[1]) 
                sheet.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
                
            col_count = 0
            row_count += 1
            row_start = row_count
            for payment in datas:
                row_start = row_count + 1
                for line in payment:
                    sheet.write(row_count, col_count, line, style_normal)
                    col_count += 1  
                col_count = 0
                row_count += 1
            sheet.write('G%s:G%s' % (row_count + 1, row_count + 1), 'TOTAL', style_bold_center)
            sheet.write('H%s:H%s' % (row_count + 1, row_count + 1), '=SUM(H%s:H%s)' % (row_start, row_count), style_normal_bold)
                
