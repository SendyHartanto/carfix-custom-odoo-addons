from odoo import models, _
import operator
from odoo.exceptions import UserError


class list_journal_xlsx(models.AbstractModel):
    _name = 'report.accounting_report_xlsx.list_journal.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
                      ['No', 5],
                      ['Date', 10],
                      ['GL No', 20],
                      ['Document No', 20],
                      ['Post Account', 20],
                      ['Description', 20],
                      ['Debit', 15],
                      ['Credit', 15]
                ]
    
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_hdr_yellow = workbook.add_format({'border': 1, 'bg_color': 'yellow'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_list_journal_datas()
            report_name = 'Journal List'
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(6, 0)
            
            sheet.merge_range('A1:H1', 'JOURNAL LIST - ' + obj.company_id.name, style_normal_bold)
        
            sheet.merge_range('A3:C3', 'Date', style_hdr_blue)
            sheet.merge_range('D3:F3', 'Journal Filters', style_hdr_blue)
            sheet.merge_range('G3:H3', 'Target Moves', style_hdr_blue)
            
            journal_filters = ', '.join(map(str, [x.code or '/' for x in obj['journal_ids']])) if obj.journal_ids else 'All'
            if obj.target_move == 'all':
                target_move = 'All Entries'
            else:
                target_move = 'All Posted Entries'
                
            sheet.merge_range('A4:C4', obj.date_from + ' - ' + obj.date_to, style_normal_center)
            sheet.merge_range('D4:F4', journal_filters, style_normal_center)
            sheet.merge_range('G4:H4', target_move, style_normal_center)
            
            row_count = 5
            col_count = 0
            for column in columns:
                sheet.set_column(col_count, col_count, column[1]) 
                sheet.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
            
            row_count += 2
            col_count = 0
            for journal in sorted(datas['csv'].items(), key=operator.itemgetter(0)):
                sheet.merge_range('A%s:H%s' % (row_count, row_count), journal[0], style_hdr_yellow)
                no = 1
                for moves in sorted(journal[1]['moves'].items(), key=operator.itemgetter(0)):
                    col_count = 0
                    sheet.write(row_count, col_count, no, style_normal_center)
                    col_count += 1 
                    sheet.write(row_count, col_count, moves[1]['date'], style_normal)
                    col_count += 1 
                    sheet.write(row_count, col_count, moves[1]['name'], style_normal)
                    col_count += 1 
                    sheet.write(row_count, col_count, moves[1]['ref'], style_normal)
                    col_count += 1  
                    for lines in moves[1]['move_line']:
                        col_count = 4
                        for line in lines:
                            sheet.write(row_count, col_count, line, style_normal)
                            col_count += 1  
                        row_count += 1
                    col_count = 0
                    row_count += 1
                    no += 1
                row_count += 1
            
