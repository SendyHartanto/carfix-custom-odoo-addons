from odoo import models, _
import operator
from odoo.exceptions import UserError


class general_ledger_xlsx(models.AbstractModel):
    _name = 'report.accounting_report_xlsx.general_ledger.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
                      ['No', 5],
                      ['Trans No', 20],
                      ['Date', 13],
                      ['Reff', 20],
                      ['Description', 20],
                      ['Debit', 15],
                      ['Credit', 15],
                      ['Balance', 20]
                ]
    
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_general_ledger_datas()
            report_name = 'General Ledger'
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(4, 0)
            
            sheet.merge_range('A1:H1', 'GENERAL LEDGER - ' + obj.company_id.name, style_normal_bold)
        
            sheet.merge_range('A2:C2', 'Date', style_hdr_blue)
            sheet.merge_range('D2:F2', 'Account Filters', style_hdr_blue)
            sheet.merge_range('G2:H2', 'Target Moves', style_hdr_blue)
            
            account_filters = ', '.join(map(str, [x.code for x in obj['account_ids']])) if obj.account_ids else 'All'
            if obj.target_move == 'all':
                target_move = 'All Entries'
            else:
                target_move = 'All Posted Entries'
                
            sheet.merge_range('A3:C3', obj.date_from + ' - ' + obj.date_to, style_normal_center)
            sheet.merge_range('D3:F3', account_filters, style_normal_center)
            sheet.merge_range('G3:H3', target_move, style_normal_center)
            
            row_count = 5
            col_count = 0
            for account in sorted(datas.items(), key=operator.itemgetter(0)):
                sheet.merge_range('A%s:H%s' % (row_count, row_count), account[0], style_normal_bold)
                for column in columns:
                    sheet.set_column(col_count, col_count, column[1]) 
                    sheet.write(row_count, col_count, column[0], style_hdr_blue)
                    col_count += 1
                
                row_count += 2
                sheet.merge_range('D%s:E%s' % (row_count, row_count), 'Starting Balance', style_normal_bold)
                sheet.write('F%s:F%s' % (row_count, row_count), account[1]['init_debit'] or 0.0, style_normal_bold)
                sheet.write('G%s:G%s' % (row_count, row_count), account[1]['init_credit'] or 0.0, style_normal_bold)
                sheet.write('H%s:H%s' % (row_count, row_count), '=F%s-G%s' % (row_count, row_count), style_normal_bold)

                col_count = 0
                no = 1
                row_start = row_count
                for move_line in account[1]['move_line']:
                    sheet.write(row_count, col_count, no, style_normal_center)
                    col_count += 1  
                    for line in move_line:
                        sheet.write(row_count, col_count, line, style_normal)
                        col_count += 1  
                    sheet.write(row_count, col_count, '=H%s+F%s-G%s' % (row_count, row_count + 1, row_count + 1), style_normal)
                    no += 1
                    col_count = 0
                    row_count += 1
                
                row_count += 1
                sheet.merge_range('D%s:E%s' % (row_count, row_count), 'Ending Balance', style_normal_bold)
                sheet.write('F%s:F%s' % (row_count, row_count), '=sum(F%s:F%s)' % (row_start, row_count - 1), style_normal_bold)
                sheet.write('G%s:G%s' % (row_count, row_count), '=sum(G%s:G%s)' % (row_start, row_count - 1), style_normal_bold)
                sheet.write('H%s:H%s' % (row_count, row_count), '=F%s-G%s' % (row_count, row_count), style_normal_bold)
                
                col_count = 0
                row_count += 2
            
