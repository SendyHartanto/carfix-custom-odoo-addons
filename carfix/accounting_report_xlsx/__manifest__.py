{
    'name': 'Accounting Report Xlsx',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'sequence': 0,
    'category': 'Custom',
    'website': 'http://visi.co.id/',
    'summary': 'Accounting Report Xlsx',
    'description': '''
        Accounting Report Xlsx
    ''',
    'depends': [
        'report_xlsx',
        'account',
        'account_invoicing',
        'account_payment',
    ],
    'data': [
        'wizard/general_ledger_wizard_view.xml',
        'wizard/supplier_invoice_wizard_view.xml',
        'wizard/list_ap_wizard_view.xml',
        'wizard/giro_payment_wizard_view.xml',
        'wizard/aged_partner_balance_wizard_view.xml',
        'wizard/sale_return_wizard_view.xml',
        'wizard/list_journal_wizard_view.xml',
        'wizard/trial_balance_wizard_view.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
