from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class PaymentAdvance(models.Model):
    _name = "payment.advance"

    # name = fields.Char(string='Name', readonly=True, states={'draft': [('readonly', False)]})
    name = fields.Char(string='Name', related='move_id.name', readonly=True, states={'draft': [('readonly', False)]})

    company_id = fields.Many2one('res.company', string='Company',
        readonly=True, states={'draft': [('readonly', False)]},
        default=lambda self: self.env.user.company_id.id)

    partner_id = fields.Many2one('res.partner', 'Customer', required=True, readonly=True, states={'draft': [('readonly', False)]})
    journal_id = fields.Many2one('account.journal', 'Payment Method', domain=[('type', 'in', ('bank', 'cash'))], required=True, readonly=True, states={'draft': [('readonly', False)]})
    destination_journal_id = fields.Many2one('account.journal', 'Jurnal Advance', required=True, readonly=True, states={'draft': [('readonly', False)]})

    amount = fields.Monetary(string='Payment Amount', required=True, readonly=True, states={'draft': [('readonly', False)]})
    payment_date = fields.Date(string='Payment Date', default=fields.Date.context_today, required=True, copy=False, readonly=True, states={'draft': [('readonly', False)]})

    account_advance_id = fields.Many2one('account.account', 'Advance Account')

    state = fields.Selection([
        ('draft', 'Draft'),
        ('posted', 'Posted'),
        ('sent', 'Sent'),
        ('reconciled', 'Reconciled'),
        ('cancelled', 'Cancelled')], readonly=True, default='draft', copy=False, string="Status")

    move_name = fields.Char(string='Journal Entry Name', readonly=True,
        default=False, copy=False,
        help="Technical field holding the number given to the journal entry, automatically set when the statement line is reconciled then stored to set the same number again if the line is cancelled, set to draft and re-processed again.")

    move_id = fields.Many2one('account.move', string="Journal Entries", readonly=True, )
    move_settlement_id = fields.Many2one('account.move', string="Settlement Entries", readonly=True, )
    invoice_id = fields.Many2one('account.invoice', string="Related Invoice", readonly=True, )

    communication = fields.Char(string='Memo', readonly=True, states={'draft': [('readonly', False)]})

    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id, readonly=True, states={'draft': [('readonly', False)]})


    def _get_move_vals(self, journal=None):
        """ Return dict to create the payment move
        """
        journal = journal or self.journal_id
        if not journal.sequence_id:
            raise UserError(_('Configuration Error !'), _('The journal %s does not have a sequence, please specify one.') % journal.name)
        if not journal.sequence_id.active:
            raise UserError(_('Configuration Error !'), _('The sequence of journal %s is deactivated.') % journal.name)
        name = self.move_name or journal.with_context(ir_sequence_date=self.payment_date).sequence_id.next_by_id()
        return {
            'name': name,
            'date': self.payment_date,
            'ref': self.communication or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
        }

    @api.multi
    def post(self):

        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        if not self.destination_journal_id.default_credit_account_id:
            raise UserError(_('The journal %s does not have a Default Credit Account, please specify one.') % self.destination_journal_id.name)

        move = self.env['account.move'].create(self._get_move_vals())
        # dst_move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))
        #Move Line untuk Payment Method
        debit_aml_vals = {
            'name': self.communication or 'Uang Muka',
            'debit': self.amount,
            'credit': 0,
            'partner_id': self.partner_id.id,
            'move_id' : move.id,
            # 'account_id': self.payment_type in ('outbound','transfer') and self.journal_id.default_debit_account_id.id or self.journal_id.default_credit_account_id.id,
            'account_id': self.journal_id.default_debit_account_id.id
        }
        #Move Line untuk Advance
        credit_aml_vals = {
            'name': 'Uang Muka',
            'debit': 0,
            'credit': self.amount,
            'move_id' : move.id,
            'partner_id': self.partner_id.id,
            'account_id': self.destination_journal_id.default_credit_account_id.id
        }

        #Pembentukkan Account Move Line
        #Di sortir terbalik
        aml_obj.create(credit_aml_vals)
        aml_obj.create(debit_aml_vals)
        move.post()

        self.write({
                'state': 'posted',
                'move_id': move.id,
            }
        )





    @api.multi
    def set_to_draft(self):
        self.write({'state': 'draft'})


    @api.multi
    def cancel(self):
        for rec in self:
            if rec.move_id:
                rec.move_id.line_ids.remove_move_reconcile()
                rec.move_id.button_cancel()
                rec.move_id.unlink()
            if rec.move_settlement_id:
                rec.move_settlement_id.line_ids.remove_move_reconcile()
                rec.move_settlement_id.button_cancel()
                rec.move_settlement_id.unlink()
            rec.state = 'cancelled'

    @api.multi
    def unlink(self):
        if any(bool(rec.move_id) for rec in self):
            raise UserError(_("You can not delete a payment that is already posted"))
        return super(PaymentAdvance, self).unlink()

    @api.multi
    def link_invoice(self):
        inv_doc = self.invoice_id
        if inv_doc:
            aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
            if not self.destination_journal_id.default_debit_account_id:
                raise UserError(_('The journal %s does not have a Default Debit Account, please specify one.') % self.destination_journal_id.name)

            move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))
            # dst_move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))
            #Move Line untuk Advance
            debit_aml_vals = {
                'name': 'Settlement : Advance payment',
                'debit': self.amount,
                'credit': 0,
                'partner_id': self.partner_id.id,
                'move_id' : move.id,
                # 'account_id': self.payment_type in ('outbound','transfer') and self.journal_id.default_debit_account_id.id or self.journal_id.default_credit_account_id.id,
                'account_id': self.destination_journal_id.default_debit_account_id.id
            }
            #Move Line untuk Payment Method
            credit_aml_vals = {
                'name': 'Settlement : Advance payment',
                'debit': 0,
                'credit': self.amount,
                'move_id' : move.id,
                'partner_id': self.partner_id.id,
                'account_id': self.partner_id.property_account_receivable_id.id,
            }

            #Pembentukkan Account Move Line
            #Di sortir terbalik
            credit_aml = aml_obj.create(credit_aml_vals)
            debit_aml = aml_obj.create(debit_aml_vals)

            accountInvoice = self.env['account.invoice']
            inv_doc.register_payment(credit_aml)
            move.post()

            self.write({
                    'state': 'reconciled',
                    'move_settlement_id': move.id,
                }
            )

        else:
            raise UserError(_("Link an Invoice first !"))



    #Buat Layout
    @api.constrains('amount')
    def _constrain_amount(self):
        for rec in self:
            if rec.amount < 0.0:
                raise ValidationError(_(
                    'Payment amount must always be positive.'))

    @api.onchange('partner_type')
    def _onchange_partner_type(self):
        # Set partner_id domain
        res = {}
        if self.partner_type:
            res = {'domain': {'partner_id': [(self.partner_type, '=', True)]}}
        return res

    @api.onchange('destination_journal_id')
    def _ganti_jurnal_advance(self):

        if self.destination_journal_id:
            account_id = self.destination_journal_id.default_credit_account_id
            if account_id:
                self.account_advance_id = account_id
            else:
                raise UserError(_("Field of Default Debit Credit Account in Journal named : %s , is not Configured. Go to Menu Invoicing > Configuration > Management > Journal") % self.destination_journal_id.name)



# action_rule = self.env['base.automation'].browse(action_rule_id)
# result = {}
# server_action = action_rule.action_server_id.with_context(active_model=self._name, onchange_self=self)
# res = server_action.run()
# if res:
#     if 'value' in res:
#         res['value'].pop('id', None)
#         self.update({key: val for key, val in res['value'].items() if key in self._fields})
#     if 'domain' in res:
#         result.setdefault('domain', {}).update(res['domain'])
#     if 'warning' in res:
#         result['warning'] = res['warning']
