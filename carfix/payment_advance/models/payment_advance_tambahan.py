from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


MAP_INVOICE_TYPE_PARTNER_TYPE = {
    'out_invoice': 'customer',
    'out_refund': 'customer',
    'in_invoice': 'supplier',
    'in_refund': 'supplier',
}
# Since invoice amounts are unsigned, this is how we know if money comes in or goes out
MAP_INVOICE_TYPE_PAYMENT_SIGN = {
    'out_invoice': 1,
    'in_refund': -1,
    'in_invoice': -1,
    'out_refund': 1,
}

class PaymentAdvanceTambahan(models.Model):
    _inherit = "payment.advance"


    payment_type = fields.Selection([('outbound', 'Send Money'), ('inbound', 'Receive Money')], string='Payment Type', required=True)
    # payment_method_id = fields.Many2one('account.payment.method', string='Payment Method Type', required=False, oldname="payment_method",
    #     help="Manual: Get paid by cash, check or any other method outside of Odoo.\n"\
    #     "Electronic: Get paid automatically through a payment acquirer by requesting a transaction on a card saved by the customer when buying or subscribing online (payment token).\n"\
    #     "Check: Pay bill by check and print it from Odoo.\n"\
    #     "Batch Deposit: Encase several customer checks at once by generating a batch deposit to submit to your bank. When encoding the bank statement in Odoo, you are suggested to reconcile the transaction with the batch deposit.To enable batch deposit,module account_batch_deposit must be installed.\n"\
    #     "SEPA Credit Transfer: Pay bill from a SEPA Credit Transfer file you submit to your bank. To enable sepa credit transfer, module account_sepa must be installed ")
    # payment_method_code = fields.Char(related='payment_method_id.code',
    #     help="Technical field used to adapt the interface to the payment type selected.", readonly=True)

    partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor')])


    @api.model
    def default_get(self, fields):
        rec = super(PaymentAdvanceTambahan, self).default_get(fields)


        rec.update({
            'payment_type': 'inbound',
            'partner_type': 'customer',
        })
        return rec
