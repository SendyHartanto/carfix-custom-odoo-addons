{

    'name': 'Payment Voucher',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'AR Process',
    'website': 'http://visi.co.id/',
    'summary': 'AR Process',
    'description': '''
    ''',
    'depends': [
        'vs_account_role',
        'account',
        'web',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/payment_voucher.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
