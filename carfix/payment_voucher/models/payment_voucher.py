from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class PaymentVoucher(models.Model):
    _name = "payment.voucher"

    name = fields.Char(string='Name', required=True, help="Nomor Voucer yang diserahkan oleh Customer", readonly=True, states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one('res.partner', 'Customer', readonly=True, states={'draft': [('readonly', False)]})
    invoice_id = fields.Many2one('account.invoice', 'Invoice', readonly=True, states={'draft': [('readonly', False)]})
    journal_id = fields.Many2one('account.journal', 'Payment Method', domain=[('type', 'in', ('bank', 'cash'))], readonly=True, states={'draft': [('readonly', False)]})
    # destination_journal_id = fields.Many2one('account.journal', 'Jurnal Advance')

    amount = fields.Monetary(string='Payment Amount', readonly=True, states={'draft': [('readonly', False)]})
    payment_date = fields.Date(string='Payment Date', default=fields.Date.context_today, readonly=True, states={'draft': [('readonly', False)]})

    company_id = fields.Many2one('res.company', string='Company',
        readonly=True, states={'draft': [('readonly', False)]},
        default=lambda self: self.env.user.company_id.id)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('posted', 'Posted'),
        ('sent', 'Sent'),
        ('reconciled', 'Reconciled'),
        ('cancelled', 'Cancelled')], readonly=True, default='draft', copy=False, string="Status")

    move_name = fields.Char(string='Journal Entry Name', readonly=True,
        default=False, copy=False,
        help="Technical field holding the number given to the journal entry, automatically set when the statement line is reconciled then stored to set the same number again if the line is cancelled, set to draft and re-processed again.")

    communication = fields.Char(string='Memo')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id, readonly=True, states={'draft': [('readonly', False)]})

    def _get_move_vals(self, journal=None):
        """ Return dict to create the payment move
        """
        journal = journal or self.journal_id
        if not journal.sequence_id:
            raise UserError(_('Configuration Error !'), _('The journal %s does not have a sequence, please specify one.') % journal.name)
        if not journal.sequence_id.active:
            raise UserError(_('Configuration Error !'), _('The sequence of journal %s is deactivated.') % journal.name)
        name = self.move_name or journal.with_context(ir_sequence_date=self.payment_date).sequence_id.next_by_id()
        return {
            'name': name,
            'date': self.payment_date,
            'ref': 'Voucher : ' + self.name or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
        }

    @api.multi
    def post(self):
        self.link_invoice()
        self.write({'state': 'posted'})


    @api.multi
    def set_to_draft(self):
        self.write({'state': 'draft'})


    @api.multi
    def link_invoice(self):

        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        if not self.journal_id.default_debit_account_id:
            raise UserError(_('The journal %s does not have a Default Debit Account, please specify one.') % self.journal_id.name)
        move = self.env['account.move'].create(self._get_move_vals())
        # dst_move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))
        #Move Line untuk Advance
        inv_doc = self.invoice_id or False
        if not inv_doc:
            raise UserError(_('Voucher %s does not have linked Invoice') % self.name)

        debit_aml_vals = {
            'name': move.ref or '',
            'debit': self.amount,
            'credit': 0,
            'partner_id': self.partner_id.id,
            'move_id' : move.id,
            # 'account_id': self.payment_type in ('outbound','transfer') and self.journal_id.default_debit_account_id.id or self.journal_id.default_credit_account_id.id,
            'account_id': self.journal_id.default_debit_account_id.id
        }
        #Move Line untuk Payment Method
        credit_aml_vals = {
            'name': move.ref or '',
            'debit': 0,
            'credit': self.amount,
            'move_id' : move.id,
            'partner_id': self.partner_id.id,
            'account_id': self.invoice_id.account_id.id,
        }

        #Pembentukkan Account Move Line
        #Di sortir terbalik
        credit_aml = aml_obj.create(credit_aml_vals)
        debit_aml = aml_obj.create(debit_aml_vals)

        inv_doc.register_payment(credit_aml)

        move.post()


    @api.constrains('amount')
    def _constrain_amount(self):
        for rec in self:
            if rec.amount <= 0.0:
                raise ValidationError(_(
                    'The amount must always be positive.'))

    @api.constrains('name')
    def _constrain_amount(self):
        for rec in self:
            payment_voucher_doc = self.env['payment.voucher'].search(
                [('name', '=', rec.name)], limit=1)
            if payment_voucher_doc != rec:
                raise ValidationError(
                    _('Voucher %s sudah digunakan'
                      ' pada tanggal %s'
                      ' oleh %s') %
                    (payment_voucher_doc.name,
                    payment_voucher_doc.payment_date,
                    payment_voucher_doc.partner_id.name)
                    )
