from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AccountJournal(models.Model):
    _inherit = "account.journal"

    type = fields.Selection(selection_add=[('voucher', 'Voucher')])
