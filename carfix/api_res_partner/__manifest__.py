{

    'name': 'API Res Partner',
    'version': '11.1.0',
    'author': 'PT Visi',
    'license': 'OPL-1',
    'category': 'Backend',
    'website': 'http://visi.co.id/',
    'summary': 'Restful Api Service',
    'description': '''

    Requirement :

    1. Field Php Id,
    2. Field NPWP

    ''',
    'depends': [
        'base',
    ],
    'data': [
            'views/res_partner.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
