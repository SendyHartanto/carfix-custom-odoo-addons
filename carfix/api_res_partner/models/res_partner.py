from odoo import models, fields, api, _

class ResPartner(models.Model):
    _inherit = "res.partner"

    php_id = fields.Integer(string='SACI-ID', help="Data ini tersinkron hanya ketika koneksi Customer Invoice dari Saci")
    npwp = fields.Char(string='NPWP', help="Data ini tersinkron hanya ketika koneksi Customer Invoice dari Saci")
    kategori = fields.Char(string='Kategori')
